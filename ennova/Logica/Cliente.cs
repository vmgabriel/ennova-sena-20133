﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace ennova.Logica
{
    public class Cliente
    {
        public Int64 id { get; set; }
        public String Nombre { get; set; }
        public String Apellido1 { get; set; }
        public String Apellido2 { get; set; }
        public String Direccion { get; set; }
        public String Genero { get; set; }
        public String Correo { get; set; }
        public String Telefono { get; set; }
        public Int64 CC { get; set; }

        public Cliente() { }

        public static int registrar(Cliente nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec crear_cliente '" + nep.Nombre + "','" + nep.Apellido1 + "','" + nep.Apellido2 + "','"+nep.Correo+"','"+nep.Genero+"',"+nep.CC+",'"+nep.Direccion+"','"+nep.Telefono+"'"), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Cliente> pedir()
        {
            List<Cliente> vend = new List<Cliente>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_cliente"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Cliente a = new Cliente();
                    a.id = read.GetInt64(0);
                    a.Nombre = read.GetString(1);
                    a.Apellido1 = read.GetString(2);
                    a.Apellido2 = read.GetString(3);
                    a.Correo = read.GetString(4);
                    a.Genero = read.GetString(5);
                    a.CC = read.GetInt64(6);
                    a.Direccion = read.GetString(7);
                    a.Telefono = read.GetString(8);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }

        public static Cliente llamar(Int64 pid)
        {
            Cliente a = new Cliente();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_cliente " + pid, Conn);
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                a.id = read.GetInt64(0);
                a.Nombre = read.GetString(1);
                a.Apellido1 = read.GetString(2);
                a.Apellido2 = read.GetString(3);
                a.Correo = read.GetString(4);
                a.Genero = read.GetString(5);
                a.CC = read.GetInt64(6);
                a.Direccion = read.GetString(7);
                a.Telefono = read.GetString(8);
                Conn.Close();
                return a;
            }
        }

        public static int modificar(Cliente nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_cliente "+nep.id+",'" + nep.Nombre + "','" + nep.Apellido1 + "','" + nep.Apellido2 + "','" + nep.Correo + "','" + nep.Genero + "'," + nep.CC + ",'" + nep.Direccion + "','" + nep.Telefono + "'"), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Cliente> buscar(string code)
        {
            List<Cliente> vend = new List<Cliente>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec Buscar_cliente '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Cliente a = new Cliente();
                    a.id = read.GetInt64(0);
                    a.Nombre = read.GetString(1);
                    a.Apellido1 = read.GetString(2);
                    a.Apellido2 = read.GetString(3);
                    a.Correo = read.GetString(4);
                    a.Genero = read.GetString(5);
                    a.CC = read.GetInt64(6);
                    a.Direccion = read.GetString(7);
                    a.Telefono = read.GetString(8);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_cliente " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }
    }
}
