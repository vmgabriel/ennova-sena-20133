﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ennova.Conexion;

namespace ennova.Logica
{
    public class Detalle_factura_venta
    {
        public Int64 id_detallefv {get; set;}
        public Int64 cantidad_productos_detalle {get; set;}
        public Int64 valor_total_productos_detalle {get; set;}
        public Int64 Fk_prod_detallefv {get; set;}
        public Int64 Fk_fv_detallefv {get; set;}

        public Detalle_factura_venta() { }

        public static int registrar(Detalle_factura_venta nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec crear_detfactura " + nep.cantidad_productos_detalle + "," + nep.valor_total_productos_detalle + "," + nep.Fk_prod_detallefv + "," + nep.Fk_fv_detallefv), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Detalle_factura_venta> pedir()
        {
            List<Detalle_factura_venta> vend = new List<Detalle_factura_venta>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_detfactura"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Detalle_factura_venta a = new Detalle_factura_venta();
                    a.id_detallefv = read.GetInt64(0);
                    a.cantidad_productos_detalle = read.GetInt64(1);
                    a.valor_total_productos_detalle = read.GetInt64(2);
                    a.Fk_prod_detallefv = read.GetInt64(3);
                    a.Fk_fv_detallefv = read.GetInt64(4);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }
        public static List<Factura_Propiedad> pedir_factura(Int64 pid)
        {
            List<Factura_Propiedad> vend = new List<Factura_Propiedad>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec Factura_venta_sacar " + pid), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Factura_Propiedad a = new Factura_Propiedad();
                    a.id_Cod = read.GetInt64(0);
                    a.id_producto = read.GetInt64(1);
                    a.Nombre_producto = read.GetString(2);
                    a.cantidad_productos = read.GetInt64(3);
                    a.costo_producto = read.GetInt64(4);
                    a.valor_total_producto = read.GetInt64(5);
                    a.Factura_venta = read.GetInt64(6);
                    Global.costo_total = Global.costo_total + read.GetInt64(5);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }

        public static Detalle_factura_venta llamar(Int64 pid)
        {
            Detalle_factura_venta a = new Detalle_factura_venta();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_detfactura " + pid, Conn);
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                a.id_detallefv = read.GetInt64(0);
                a.cantidad_productos_detalle = read.GetInt64(1);
                a.valor_total_productos_detalle = read.GetInt64(2);
                a.Fk_prod_detallefv = read.GetInt64(3);
                a.Fk_fv_detallefv = read.GetInt64(4);
                Conn.Close();
                return a;
            }
        }

        public static int modificar(Detalle_factura_venta nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_detfactura "+nep.id_detallefv+"," + nep.cantidad_productos_detalle + "," + nep.valor_total_productos_detalle + "," + nep.Fk_prod_detallefv + "," + nep.Fk_fv_detallefv), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Detalle_factura_venta> buscar(string code)
        {
            List<Detalle_factura_venta> vend = new List<Detalle_factura_venta>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("buscar_detfactura '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Detalle_factura_venta a = new Detalle_factura_venta();
                    a.id_detallefv = read.GetInt64(0);
                    a.cantidad_productos_detalle = read.GetInt64(1);
                    a.valor_total_productos_detalle = read.GetInt64(2);
                    a.Fk_prod_detallefv = read.GetInt64(3);
                    a.Fk_fv_detallefv = read.GetInt64(4);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_detalles_fv_todos " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }
        public static int destruir_fveliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_detfactura " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static Int64 con_nom_id_vendedor(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_alias_id_vendedor '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Vendedor> Lista_vendedores()
        {
            List<Vendedor> code = new List<Vendedor>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_vendedor_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Vendedor a = new Vendedor();
                    a.id = read.GetInt64(0);
                    a.Alias = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }
    }
}
