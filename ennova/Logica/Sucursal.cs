﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ennova.Presentacion;
using ennova.Conexion;

namespace ennova.Logica
{
    public class Sucursal
    {
        public Int64 id_sucursal { get; set;}
        public String nombre_sucursal {get; set;}
        public String direccion_sucursal {get; set;}
        public String ciudad_sucursal {get; set;}
        public String pais_sucursal {get; set;}
        public String telefono_sucursal {get; set;}
        public String correo_sucursal {get; set;}
        public String nit_estandar_sucursal { get; set; }

        public Sucursal() { }

        //codigo a proceder

        public static int registrar(Sucursal nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec crear_sucursal '"+nep.nombre_sucursal+"','"+nep.direccion_sucursal+"','"+nep.ciudad_sucursal+"','"+nep.pais_sucursal+"','"+nep.telefono_sucursal+"','"+nep.correo_sucursal+"','"+nep.nit_estandar_sucursal+"'"), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Sucursal> pedir()
        {
            List<Sucursal> vend = new List<Sucursal>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_sucursal"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Sucursal a = new Sucursal();
                    a.id_sucursal = read.GetInt64(0);
                    a.nombre_sucursal = read.GetString(1);
                    a.direccion_sucursal = read.GetString(2);
                    a.ciudad_sucursal = read.GetString(3);
                    a.pais_sucursal = read.GetString(4);
                    a.telefono_sucursal = read.GetString(5);
                    a.correo_sucursal = read.GetString(6);
                    a.nit_estandar_sucursal = read.GetString(7);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }

        public static Sucursal llamar(Int64 pid)
        {
            Sucursal a = new Sucursal();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_sucursal " + pid, Conn);
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                a.id_sucursal = read.GetInt64(0);
                a.nombre_sucursal = read.GetString(1);
                a.direccion_sucursal = read.GetString(2);
                a.ciudad_sucursal = read.GetString(3);
                a.pais_sucursal = read.GetString(4);
                a.telefono_sucursal = read.GetString(5);
                a.correo_sucursal = read.GetString(6);
                a.nit_estandar_sucursal = read.GetString(7);
                Conn.Close();
                return a;
            }
        }

        public static int modificar(Sucursal nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_sucursal " + nep.id_sucursal + ",'" + nep.nombre_sucursal + "','" + nep.direccion_sucursal + "','" + nep.ciudad_sucursal + "','" + nep.pais_sucursal + "','" + nep.telefono_sucursal + "','" + nep.correo_sucursal + "','" + nep.nit_estandar_sucursal + "'"), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Sucursal> buscar(string code)
        {
            List<Sucursal> vend = new List<Sucursal>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec Buscar_sucursal '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Sucursal a = new Sucursal();
                    a.id_sucursal = read.GetInt64(0);
                    a.nombre_sucursal = read.GetString(1);
                    a.direccion_sucursal = read.GetString(2);
                    a.ciudad_sucursal = read.GetString(3);
                    a.pais_sucursal = read.GetString(4);
                    a.telefono_sucursal = read.GetString(5);
                    a.correo_sucursal = read.GetString(6);
                    a.nit_estandar_sucursal = read.GetString(7);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_sucursal " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }
    }
}
