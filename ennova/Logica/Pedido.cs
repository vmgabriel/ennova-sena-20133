﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ennova.Conexion;

namespace ennova.Logica
{
    public class Pedido
    {
        public Int64 id_pedido {get; set;}
        public String nombre_pedido {get; set;}
        public String clase_pedido {get; set;}
        public DateTime fecha_entrega_pedido {get; set;}
        public DateTime fecha_expedida_pedido {get; set;}
        public Int64 valor_pedido {get; set;}
        public Int64 cantidad_productos_pedido {get; set;}
        public String necesidad_producto_pedido {get; set;}
        public Int64 FK_proveedor_pedido {get; set;}

        public Pedido() { }

        public static int registrar(Pedido nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                string fechaa, fechab;
                fechaa = nep.fecha_entrega_pedido.Year + "/" + nep.fecha_entrega_pedido.Month + "/" + nep.fecha_entrega_pedido.Day;
                fechab = nep.fecha_expedida_pedido.Year + "/" + nep.fecha_expedida_pedido.Month + "/" + nep.fecha_expedida_pedido.Day;
                SqlCommand comando = new SqlCommand(string.Format("exec crear_pedido '"+nep.nombre_pedido+"','"+nep.clase_pedido+"','"+fechaa+"','"+fechab+"',"+nep.valor_pedido+","+nep.cantidad_productos_pedido+",'"+nep.necesidad_producto_pedido+"',"+nep.FK_proveedor_pedido+""), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Pedido> pedir()
        {
            List<Pedido> vend = new List<Pedido>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_pedido"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Pedido a = new Pedido();
                    a.id_pedido = read.GetInt64(0);
                    a.nombre_pedido = read.GetString(1);
                    a.clase_pedido = read.GetString(2);
                    a.fecha_entrega_pedido = read.GetDateTime(3);
                    a.fecha_expedida_pedido = read.GetDateTime(4);
                    a.valor_pedido = read.GetInt64(5);
                    a.cantidad_productos_pedido = read.GetInt64(6);
                    a.necesidad_producto_pedido = read.GetString(7);
                    a.FK_proveedor_pedido = read.GetInt64(8);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }

        public static Pedido llamar(Int64 pid)
        {
            Pedido a = new Pedido();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_pedido " + pid, Conn);
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                a.id_pedido = read.GetInt64(0);
                a.nombre_pedido = read.GetString(1);
                a.clase_pedido = read.GetString(2);
                a.fecha_entrega_pedido = read.GetDateTime(3);
                a.fecha_expedida_pedido = read.GetDateTime(4);
                a.valor_pedido = read.GetInt64(5);
                a.cantidad_productos_pedido = read.GetInt64(6);
                a.necesidad_producto_pedido = read.GetString(7);
                a.FK_proveedor_pedido = read.GetInt64(8);
                Conn.Close();
                return a;
            }
        }

        public static int modificar(Pedido nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                string fechaa, fechab;
                fechaa = nep.fecha_entrega_pedido.Year + "/" + nep.fecha_entrega_pedido.Month + "/" + nep.fecha_entrega_pedido.Day;
                fechab = nep.fecha_expedida_pedido.Year + "/" + nep.fecha_expedida_pedido.Month + "/" + nep.fecha_expedida_pedido.Day;
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_pedido "+nep.id_pedido+",'" + nep.nombre_pedido + "','" + nep.clase_pedido + "','" + fechaa + "','" + fechab + "'," + nep.valor_pedido + "," + nep.cantidad_productos_pedido + ",'" + nep.necesidad_producto_pedido + "'," + nep.FK_proveedor_pedido + ""), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Pedido> buscar(string code)
        {
            List<Pedido> vend = new List<Pedido>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec buscar_pedido '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Pedido a = new Pedido();
                    a.id_pedido = read.GetInt64(0);
                    a.nombre_pedido = read.GetString(1);
                    a.clase_pedido = read.GetString(2);
                    a.fecha_entrega_pedido = read.GetDateTime(3);
                    a.fecha_expedida_pedido = read.GetDateTime(4);
                    a.valor_pedido = read.GetInt64(5);
                    a.cantidad_productos_pedido = read.GetInt64(6);
                    a.necesidad_producto_pedido = read.GetString(7);
                    a.FK_proveedor_pedido = read.GetInt64(8);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_pedido " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static Int64 id_proveedor_nombre(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec nombre_id_proveedor '"+nombre+"'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Proveedor> Lista_proveedor()
        {
            List<Proveedor> code = new List<Proveedor>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_proveedor_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Proveedor a = new Proveedor();
                    a.id = read.GetInt64(0);
                    a.nombre_proveedor= read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }

        public static string nombre_proveedor_id(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec id_nombre_proveedor " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }
    }
}
