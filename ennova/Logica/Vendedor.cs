﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace ennova.Logica
{
    public class Vendedor
    {
        public Int64 id { get; set; }
        public String Nombre { get; set; }
        public String Apellido1 { get; set; }
        public String Apellido2 { get; set; }
        public String Direccion { get; set; }
        public String Genero { get; set; }
        public String Correo { get; set; }
        public Int64 Telefono { get; set; }
        public Int64 CC { get; set; }
        public String Contraseña { get; set; }
        public String Alias { get; set; }
        public Int64 FK_sucursal { get; set; }

        public Vendedor() { }


        //codigo de segundo nivel con SQL SERVER 2008 ....array clue...multifase and encryted code

        public static int registrar(Vendedor nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec crear_vendedor '" + nep.Nombre + "','" + nep.Apellido1 + "','" + nep.Apellido2 + "','" + nep.Alias + "','" + nep.Contraseña + "','" + nep.Direccion + "','" + nep.Genero + "','" + nep.Correo + "','" + nep.Telefono + "','" + nep.CC + "',"+nep.FK_sucursal), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_vendedor "+ id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static int modificar(Vendedor nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_vendedor " + nep.id + ",'" + nep.Nombre + "','" + nep.Apellido1 + "','" + nep.Apellido2 + "','" + nep.Alias + "','" + nep.Contraseña + "','" + nep.Direccion + "','" + nep.Genero + "','" + nep.Correo + "','" + nep.Telefono + "','" + nep.CC + "'," + nep.FK_sucursal), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Vendedor> pedir()
        {
            List<Vendedor> vend = new List<Vendedor>();
            Int64 adas, bo;
            string hola, mundo;
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_vendedor"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Vendedor a = new Vendedor();
                    a.id = read.GetInt64(0);
                    a.Nombre = read.GetString(1);
                    a.Apellido1 = read.GetString(2);
                    a.Apellido2 = read.GetString(3);
                    a.Alias = read.GetString(4);
                    a.Direccion = read.GetString(6);
                    a.Genero = read.GetString(7);
                    a.Correo = read.GetString(8);
                    hola = read.GetString(9);
                    mundo = read.GetString(10);
                    a.FK_sucursal = read.GetInt64(11);
                    adas = Convert.ToInt64(hola);
                    bo = Convert.ToInt64(mundo);
                    a.Telefono = adas;
                    a.CC = bo;
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }
        public static List<Vendedor> buscar(string code)
        {
            List<Vendedor> vend = new List<Vendedor>();
            string hola, mundo;
            Int64 adas, bo;
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec Buscar_vendedor '"+code+"'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Vendedor a = new Vendedor();
                    a.id = read.GetInt64(0);
                    a.Nombre = read.GetString(1);
                    a.Apellido1 = read.GetString(2);
                    a.Apellido2 = read.GetString(3);
                    a.Alias = read.GetString(4);
                    a.Direccion = read.GetString(6);
                    a.Genero = read.GetString(7);
                    a.Correo = read.GetString(8);
                    hola = read.GetString(9);
                    mundo = read.GetString(10);
                    a.FK_sucursal = read.GetInt64(11);
                    adas = Convert.ToInt64(hola);
                    bo = Convert.ToInt64(mundo);
                    a.Telefono = adas;
                    a.CC = bo;
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }
        public static int ingreso(string usuario, string contraseña)
        {
            int a = 0;
            SqlDataReader sed;
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec ingresar_vendedor '"+usuario+"', "+contraseña, Conn);

                SqlDataReader eje = consultar.ExecuteReader();

                sed = eje;

                if (sed.Read() == true)
                {
                    Global.id_vendedor = sed.GetInt64(0);
                    a = 1;
                }
                // para enviar y decirle que si esta bien o mal
                Conn.Close();
                return a;
            }
        }
        public static Vendedor llamar(Int64 pid)
        {
            Vendedor vend = new Vendedor();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_vendedor "+pid, Conn);
                Int64 adas, bo;
                string hola, mundo;
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                vend.id = read.GetInt64(0);
                vend.Nombre = read.GetString(1);
                vend.Apellido1 = read.GetString(2);
                vend.Apellido2 = read.GetString(3);
                vend.Alias = read.GetString(4);
                vend.Contraseña = read.GetString(5);
                vend.Direccion = read.GetString(6);
                vend.Genero = read.GetString(7);
                vend.Correo = read.GetString(8);
                hola = read.GetString(9);
                mundo = read.GetString(10);
                vend.FK_sucursal = read.GetInt64(11);
                adas = Convert.ToInt64(hola);
                bo = Convert.ToInt64(mundo);
                vend.Telefono = adas;
                vend.CC = bo;
                Conn.Close();
                return vend;
            }
        }
        public static Int64 id_sucursal_nombre(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_nombre_id_sucursal '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Sucursal> Lista_sucursal()
        {
            List<Sucursal> code = new List<Sucursal>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_sucursal_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Sucursal a = new Sucursal();
                    a.id_sucursal = read.GetInt64(0);
                    a.nombre_sucursal = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }

        public static string nombre_sucursal_id(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_id_nombre_sucursal " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }
    }
}
