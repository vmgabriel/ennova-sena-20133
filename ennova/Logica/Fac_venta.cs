﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ennova.Conexion;

namespace ennova.Logica
{
    public class Fac_venta
    {
        public Int64 id_fv {get; set;}
        public String tipo_fv {get; set;}
        public Int64 total_fv {get; set;}
        public Int64 iva_fv {get; set;}
        public Int64 sub_total_fv {get; set;}
        public String comentarios_fv {get; set;}
        public Int64 Fk_cliente_fv {get; set;}
        public Int64 Fk_vendedor_fv {get; set;}
        public Int64 Fk_sucursal_fv {get; set;}
        public DateTime fecha_creacion_fv {get; set;}
        public DateTime feha_entrega_fv { get; set; }

        public Fac_venta() { }

        public static int registrar(Fac_venta nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                string fechaa,fechab;
                fechaa = nep.fecha_creacion_fv.Year + "/" + nep.fecha_creacion_fv.Month + "/" + nep.fecha_creacion_fv.Day;
                fechab = nep.feha_entrega_fv.Year + "/" + nep.feha_entrega_fv.Month + "/" + nep.feha_entrega_fv.Day;
                SqlCommand comando = new SqlCommand(string.Format("exec crear_fv '" + nep.tipo_fv + "'," + nep.total_fv + "," + nep.iva_fv + "," + nep.sub_total_fv + ",'" + nep.comentarios_fv + "'," + nep.Fk_cliente_fv + "," + nep.Fk_vendedor_fv + "," + nep.Fk_sucursal_fv + ",'" + fechaa + "','"+fechab+"'"), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Fac_venta> pedir()
        {
            List<Fac_venta> vend = new List<Fac_venta>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_fv"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Fac_venta a = new Fac_venta();
                    a.id_fv = read.GetInt64(0);
                    a.tipo_fv = read.GetString(1);
                    a.total_fv = read.GetInt64(2);
                    a.iva_fv = read.GetInt64(3);
                    a.sub_total_fv = read.GetInt64(4);
                    a.comentarios_fv = read.GetString(5);
                    a.Fk_cliente_fv = read.GetInt64(6);
                    a.Fk_vendedor_fv = read.GetInt64(7);
                    a.Fk_sucursal_fv = read.GetInt64(8);
                    a.fecha_creacion_fv = read.GetDateTime(9);
                    a.feha_entrega_fv = read.GetDateTime(10);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }

        public static Fac_venta llamar(Int64 pid)
        {
            Fac_venta a = new Fac_venta();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_fv " + pid, Conn);
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                a.id_fv = read.GetInt64(0);
                a.tipo_fv = read.GetString(1);
                a.total_fv = read.GetInt64(2);
                a.iva_fv = read.GetInt64(3);
                a.sub_total_fv = read.GetInt64(4);
                a.comentarios_fv = read.GetString(5);
                a.Fk_cliente_fv = read.GetInt64(6);
                a.Fk_vendedor_fv = read.GetInt64(7);
                a.Fk_sucursal_fv = read.GetInt64(8);
                a.fecha_creacion_fv = read.GetDateTime(9);
                a.feha_entrega_fv = read.GetDateTime(10);
                Conn.Close();
                return a;
            }
        }

        public static int modificar(Fac_venta nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                string fechaa, fechab;
                fechaa = nep.fecha_creacion_fv.Year + "/" + nep.fecha_creacion_fv.Month + "/" + nep.fecha_creacion_fv.Day;
                fechab = nep.feha_entrega_fv.Year + "/" + nep.feha_entrega_fv.Month + "/" + nep.feha_entrega_fv.Day;
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_fv "+nep.id_fv+",'" + nep.tipo_fv + "'," + nep.total_fv + "," + nep.iva_fv + "," + nep.sub_total_fv + ",'" + nep.comentarios_fv + "'," + nep.Fk_cliente_fv + "," + nep.Fk_vendedor_fv + "," + nep.Fk_sucursal_fv + ",'" + fechaa + "','" + fechab + "'"), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Fac_venta> buscar(string code)
        {
            List<Fac_venta> vend = new List<Fac_venta>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec buscar_fv '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Fac_venta a = new Fac_venta();
                    a.id_fv = read.GetInt64(0);
                    a.tipo_fv = read.GetString(1);
                    a.total_fv = read.GetInt64(2);
                    a.iva_fv = read.GetInt64(3);
                    a.sub_total_fv = read.GetInt64(4);
                    a.comentarios_fv = read.GetString(5);
                    a.Fk_cliente_fv = read.GetInt64(6);
                    a.Fk_vendedor_fv = read.GetInt64(7);
                    a.Fk_sucursal_fv = read.GetInt64(8);
                    a.fecha_creacion_fv = read.GetDateTime(9);
                    a.feha_entrega_fv = read.GetDateTime(10);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_fv " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static Int64 con_alias_id_vendedor(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_alias_id_vendedor '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Vendedor> Lista_vendedores()
        {
            List<Vendedor> code = new List<Vendedor>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_vendedor_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Vendedor a = new Vendedor();
                    a.id = read.GetInt64(0);
                    a.Alias = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }

        public static string con_id_nombre_Pedido(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_id_nombre_pedido " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }

        public static Int64 con_nombre_id_pedido(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_nombre_id_pedido '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Pedido> Lista_pedidos()
        {
            List<Pedido> code = new List<Pedido>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_pedido_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Pedido a = new Pedido();
                    a.id_pedido = read.GetInt64(0);
                    a.nombre_pedido = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }

        public static string con_id_alias_vendedor(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_id_alias_vendedor " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }

        public static Int64 ultimo_registro()
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec ultima_factura_venta"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }
    }
}
