﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ennova.Logica
{
    public class Factura_Propiedad
    {
        public Int64 id_Cod { get; set; }
        public Int64 id_producto { get; set; }
        public String Nombre_producto { get; set; }
        public Int64 cantidad_productos { get; set; }
        public Int64 costo_producto { get; set; }
        public Int64 valor_total_producto { get; set; }
        public Int64 Factura_venta { get; set; }

        public Factura_Propiedad() { }
    }
}
