﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ennova.Conexion;

namespace ennova.Logica
{
    public class Proveedor
    {
        public Int64 id {get; set;}
        public String nombre_proveedor {get; set;}
        public String direccion_proveedor {get; set;}
        public String telefono_proveedor {get; set;}
        public String correo_proveedor {get; set;}
        public String descripcion_proveedor { get; set; }

        public Proveedor() { }

        public static int registrar(Proveedor nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec crear_proveedor '" + nep.nombre_proveedor + "','" + nep.direccion_proveedor + "','" + nep.correo_proveedor + "','" + nep.telefono_proveedor + "','" + nep.descripcion_proveedor +"'"), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Proveedor> pedir()
        {
            List<Proveedor> vend = new List<Proveedor>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_proveedor"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Proveedor a = new Proveedor();
                    a.id = read.GetInt64(0);
                    a.nombre_proveedor = read.GetString(1);
                    a.direccion_proveedor = read.GetString(2);
                    a.telefono_proveedor = read.GetString(3);
                    a.correo_proveedor = read.GetString(4);
                    a.descripcion_proveedor = read.GetString(5);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }

        public static Proveedor llamar(Int64 pid)
        {
            Proveedor a = new Proveedor();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_proveedor " + pid, Conn);
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                a.id = read.GetInt64(0);
                a.nombre_proveedor = read.GetString(1);
                a.direccion_proveedor = read.GetString(2);
                a.telefono_proveedor = read.GetString(3);
                a.correo_proveedor = read.GetString(4);
                a.descripcion_proveedor = read.GetString(5);
                Conn.Close();
                return a;
            }
        }

        public static int modificar(Proveedor nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_proveedor " + nep.id + ",'" + nep.nombre_proveedor + "','" + nep.direccion_proveedor + "','" + nep.telefono_proveedor + "','" + nep.correo_proveedor + "','" + nep.descripcion_proveedor + "'"), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Proveedor> buscar(string code)
        {
            List<Proveedor> vend = new List<Proveedor>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec buscar_proveedor '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Proveedor a = new Proveedor();
                    a.id = read.GetInt64(0);
                    a.nombre_proveedor = read.GetString(1);
                    a.direccion_proveedor = read.GetString(2);
                    a.telefono_proveedor = read.GetString(3);
                    a.correo_proveedor = read.GetString(4);
                    a.descripcion_proveedor = read.GetString(5);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_proveedor " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }
    }
}
