﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ennova.Logica;

namespace ennova.Logica
{
    public class Producto
    {
        public Int64 id_producto {get; set;}
        public String nombre_producto {get; set;}
        public String tipo_producto {get; set;}
        public String clase_producto {get; set;}
        public String categoria_producto {get; set;}
        public String tela_producto {get; set;}
        public Int64 costo_producto {get; set;}
        public Int64 altura_producto {get; set;}
        public Int64 anchura_producto {get; set;}
        public Int64 largo_producto {get; set;}
        public Double garantia_producto {get; set;}
        public String tapizado_producto {get; set;}
        public String espuma_producto {get; set;}
        public Int64 Fk_proveedor_producto { get; set; }

        public Producto() { }

        public static int registrar(Producto nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec crear_producto '" + nep.nombre_producto + "','" + nep.tipo_producto + "','" + nep.clase_producto + "','" + nep.categoria_producto + "','" + nep.tela_producto + "'," + nep.costo_producto + "," + nep.altura_producto + "," + nep.anchura_producto + "," + nep.largo_producto + ","+nep.garantia_producto+",'"+nep.tapizado_producto+"','"+nep.espuma_producto+"',"+nep.Fk_proveedor_producto), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Producto> pedir()
        {
            List<Producto> vend = new List<Producto>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_producto"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Producto a = new Producto();
                    a.id_producto=read.GetInt64(0);
                    a.nombre_producto = read.GetString(1);
                    a.tipo_producto = read.GetString(2);
                    a.clase_producto = read.GetString(3);
                    a.categoria_producto = read.GetString(4);
                    a.tela_producto = read.GetString(5);
                    a.costo_producto = read.GetInt64(6);
                    a.altura_producto = read.GetInt64(7);
                    a.anchura_producto = read.GetInt64(8);
                    a.largo_producto = read.GetInt64(9);
                    a.garantia_producto = read.GetDouble(10);
                    a.tapizado_producto = read.GetString(11);
                    a.espuma_producto = read.GetString(12);
                    a.Fk_proveedor_producto = read.GetInt64(13);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }

        public static Producto llamar(Int64 pid)
        {
            Producto a = new Producto();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_producto " + pid, Conn);
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                a.id_producto = read.GetInt64(0);
                a.nombre_producto = read.GetString(1);
                a.tipo_producto = read.GetString(2);
                a.clase_producto = read.GetString(3);
                a.categoria_producto = read.GetString(4);
                a.tela_producto = read.GetString(5);
                a.costo_producto = read.GetInt64(6);
                a.altura_producto = read.GetInt64(7);
                a.anchura_producto = read.GetInt64(8);
                a.largo_producto = read.GetInt64(9);
                a.garantia_producto = read.GetDouble(10);
                a.tapizado_producto = read.GetString(11);
                a.espuma_producto = read.GetString(12);
                a.Fk_proveedor_producto = read.GetInt64(13);
                Conn.Close();
                return a;
            }
        }

        public static int modificar(Producto nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_producto "+nep.id_producto+",'" + nep.nombre_producto + "','" + nep.tipo_producto + "','" + nep.clase_producto + "','" + nep.categoria_producto + "','" + nep.tela_producto + "'," + nep.costo_producto + "," + nep.altura_producto + "," + nep.anchura_producto + "," + nep.largo_producto + "," + nep.garantia_producto + ",'" + nep.tapizado_producto + "','" + nep.espuma_producto + "'," + nep.Fk_proveedor_producto), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Producto> buscar(string code)
        {
            List<Producto> vend = new List<Producto>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec buscar_producto '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Producto a = new Producto();
                    a.id_producto = read.GetInt64(0);
                    a.nombre_producto = read.GetString(1);
                    a.tipo_producto = read.GetString(2);
                    a.clase_producto = read.GetString(3);
                    a.categoria_producto = read.GetString(4);
                    a.tela_producto = read.GetString(5);
                    a.costo_producto = read.GetInt64(6);
                    a.altura_producto = read.GetInt64(7);
                    a.anchura_producto = read.GetInt64(8);
                    a.largo_producto = read.GetInt64(9);
                    a.garantia_producto = read.GetDouble(10);
                    a.tapizado_producto = read.GetString(11);
                    a.espuma_producto = read.GetString(12);
                    a.Fk_proveedor_producto = read.GetInt64(13);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_producto " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static string con_id_nombre_Proveedor(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec id_nombre_proveedor " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }

        public static Int64 con_nombre_id_proveedor(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec nombre_id_proveedor '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Proveedor> Lista_proveedor()
        {
            List<Proveedor> code = new List<Proveedor>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_proveedor_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Proveedor a = new Proveedor();
                    a.id = read.GetInt64(0);
                    a.nombre_proveedor = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }
        public static List<Producto> buscar_especifica(string code)
        {
            List<Producto> vend = new List<Producto>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec busqueda_no_especifica_producto '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Producto a = new Producto();
                    a.id_producto = read.GetInt64(0);
                    a.nombre_producto = read.GetString(1);
                    a.tipo_producto = read.GetString(2);
                    a.clase_producto = read.GetString(3);
                    a.categoria_producto = read.GetString(4);
                    a.tela_producto = read.GetString(5);
                    a.costo_producto = read.GetInt64(6);
                    a.altura_producto = read.GetInt64(7);
                    a.anchura_producto = read.GetInt64(8);
                    a.largo_producto = read.GetInt64(9);
                    a.garantia_producto = read.GetDouble(10);
                    a.tapizado_producto = read.GetString(11);
                    a.espuma_producto = read.GetString(12);
                    a.Fk_proveedor_producto = read.GetInt64(13);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }
    }
}
