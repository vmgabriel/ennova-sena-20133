﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ennova.Conexion;

namespace ennova.Logica
{
    public class Fac_compra
    {
        public Int64 id_fc {get; set;}
        public String tipo_fc {get; set;}
        public String organizacion_fc {get; set;}
        public DateTime fecha_creacion_fc {get; set;}
        public String ciudad_fc {get; set;}
        public Int64 iva_fc {get; set;}
        public Int64 total_fc {get; set;}
        public Int64 sub_total_fc {get; set;}
        public Int64 Fk_vendedor_fc {get; set;}
        public Int64 Fk_pedido_fc { get; set; }

        public Fac_compra() { }

        public static int registrar(Fac_compra nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                string fechaa;
                fechaa = nep.fecha_creacion_fc.Year+"/"+nep.fecha_creacion_fc.Month+"/"+nep.fecha_creacion_fc.Day;
                SqlCommand comando = new SqlCommand(string.Format("exec crear_fc '"+nep.tipo_fc+"','"+nep.organizacion_fc+"','"+fechaa+"','"+nep.ciudad_fc+"',"+nep.iva_fc+","+nep.total_fc+","+nep.sub_total_fc+","+nep.Fk_vendedor_fc+","+nep.Fk_pedido_fc+""), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Fac_compra> pedir()
        {
            List<Fac_compra> vend = new List<Fac_compra>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_fc"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Fac_compra a = new Fac_compra();
                    a.id_fc = read.GetInt64(0);
                    a.tipo_fc = read.GetString(1);
                    a.organizacion_fc = read.GetString(2);
                    a.fecha_creacion_fc = read.GetDateTime(3);
                    a.ciudad_fc = read.GetString(4);
                    a.iva_fc = read.GetInt64(5);
                    a.total_fc = read.GetInt64(6);
                    a.sub_total_fc = read.GetInt64(7);
                    a.Fk_vendedor_fc=read.GetInt64(8);
                    a.Fk_pedido_fc = read.GetInt64(9);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }

        public static Fac_compra llamar(Int64 pid)
        {
            Fac_compra a = new Fac_compra();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_fc " + pid, Conn);
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                a.id_fc = read.GetInt64(0);
                a.tipo_fc = read.GetString(1);
                a.organizacion_fc = read.GetString(2);
                a.fecha_creacion_fc = read.GetDateTime(3);
                a.ciudad_fc = read.GetString(4);
                a.iva_fc = read.GetInt64(5);
                a.total_fc = read.GetInt64(6);
                a.sub_total_fc = read.GetInt64(7);
                a.Fk_vendedor_fc = read.GetInt64(8);
                a.Fk_pedido_fc = read.GetInt64(9);
                Conn.Close();
                return a;
            }
        }

        public static int modificar(Fac_compra nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                string fechaa;
                fechaa = nep.fecha_creacion_fc.Year + "/" + nep.fecha_creacion_fc.Month + "/" + nep.fecha_creacion_fc.Day;
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_fc "+nep.id_fc+",'" + nep.tipo_fc + "','" + nep.organizacion_fc + "','" + fechaa + "','" + nep.ciudad_fc + "'," + nep.iva_fc + "," + nep.total_fc + "," + nep.sub_total_fc + "," + nep.Fk_vendedor_fc + "," + nep.Fk_pedido_fc + ""), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Fac_compra> buscar(string code)
        {
            List<Fac_compra> vend = new List<Fac_compra>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec buscar_fc '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Fac_compra a = new Fac_compra();
                    a.id_fc = read.GetInt64(0);
                    a.tipo_fc = read.GetString(1);
                    a.organizacion_fc = read.GetString(2);
                    a.fecha_creacion_fc = read.GetDateTime(3);
                    a.ciudad_fc = read.GetString(4);
                    a.iva_fc = read.GetInt64(5);
                    a.total_fc = read.GetInt64(6);
                    a.sub_total_fc = read.GetInt64(7);
                    a.Fk_vendedor_fc = read.GetInt64(8);
                    a.Fk_pedido_fc = read.GetInt64(9);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_fc " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static Int64 con_alias_id_vendedor(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_alias_id_vendedor '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Vendedor> Lista_vendedores()
        {
            List<Vendedor> code = new List<Vendedor>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_vendedor_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Vendedor a = new Vendedor();
                    a.id = read.GetInt64(0);
                    a.Alias = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }

        public static string con_id_nombre_Pedido(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_id_nombre_pedido " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }

        public static Int64 con_nombre_id_pedido(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_nombre_id_pedido '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Pedido> Lista_pedidos()
        {
            List<Pedido> code = new List<Pedido>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_pedido_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Pedido a = new Pedido();
                    a.id_pedido = read.GetInt64(0);
                    a.nombre_pedido = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }

        public static string con_id_alias_vendedor(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec con_id_alias_vendedor " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }
    }
}
