﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ennova.Logica
{
    public class Global
    {
        public static Int64 modif_client;
        public static Int64 modif_proveedor;
        public static Int64 modif_sucursal;
        public static Int64 modif_pedido;
        public static Int64 modif_factura_compra;
        public static Int64 modif_pago;
        public static Int64 modif_producto;
        public static Int64 id_vendedor;
        public static Int64 costo_total;
    }
}
