﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ennova.Conexion;

namespace ennova.Logica
{
    public class Pago
    {
        public Int64 id_pago {get; set;}
        public String tipo_pago {get; set;}
        public Int64 iva_pago {get; set;}
        public Int64 total_pago {get; set;}
        public Int64 sub_total_pago {get; set;}
        public Int64 Fk_cliente_pago {get; set;}
        public Int64 Fk_vendedor_pago {get; set;}
        public Int64 Fk_sucursal_pago { get; set; }

        public Pago() { }

        public static int registrar(Pago nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec crear_pago '" + nep.tipo_pago + "'," + nep.iva_pago + "," + nep.total_pago + "," + nep.sub_total_pago + "," + nep.Fk_cliente_pago + "," + nep.Fk_vendedor_pago + "," + nep.Fk_sucursal_pago + ""), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Pago> pedir()
        {
            List<Pago> vend = new List<Pago>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec todos_pago"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Pago a = new Pago();
                    a.id_pago = read.GetInt64(0);
                    a.tipo_pago = read.GetString(1);
                    a.iva_pago = read.GetInt64(2);
                    a.total_pago = read.GetInt64(3);
                    a.sub_total_pago = read.GetInt64(4);
                    a.Fk_cliente_pago = read.GetInt64(5);
                    a.Fk_vendedor_pago = read.GetInt64(6);
                    a.Fk_sucursal_pago = read.GetInt64(7);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;
        }

        public static Pago llamar(Int64 pid)
        {
            Pago a = new Pago();
            //aqui se pone el codigo de ingresar
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand consultar = new SqlCommand("exec seleccionar_pago " + pid, Conn);
                SqlDataReader read = consultar.ExecuteReader();
                read.Read();
                a.id_pago = read.GetInt64(0);
                a.tipo_pago = read.GetString(1);
                a.iva_pago = read.GetInt64(2);
                a.total_pago = read.GetInt64(3);
                a.sub_total_pago = read.GetInt64(4);
                a.Fk_cliente_pago = read.GetInt64(5);
                a.Fk_vendedor_pago = read.GetInt64(6);
                a.Fk_sucursal_pago = read.GetInt64(7);
                Conn.Close();
                return a;
            }
        }

        public static int modificar(Pago nep)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec modificar_pago "+nep.id_pago+",'" + nep.tipo_pago + "'," + nep.iva_pago + "," + nep.total_pago + "," + nep.sub_total_pago + "," + nep.Fk_cliente_pago + "," + nep.Fk_vendedor_pago + "," + nep.Fk_sucursal_pago + ""), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static List<Pago> buscar(string code)
        {
            List<Pago> vend = new List<Pago>();
            using (SqlConnection Conn = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec buscar_pago '" + code + "'"), Conn);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Pago a = new Pago();
                    a.id_pago = read.GetInt64(0);
                    a.tipo_pago = read.GetString(1);
                    a.iva_pago = read.GetInt64(2);
                    a.total_pago = read.GetInt64(3);
                    a.sub_total_pago = read.GetInt64(4);
                    a.Fk_cliente_pago = read.GetInt64(5);
                    a.Fk_vendedor_pago = read.GetInt64(6);
                    a.Fk_sucursal_pago = read.GetInt64(7);
                    vend.Add(a);
                }
                Conn.Close();
            }
            return vend;


        }

        public static int eliminar(Int64 id)
        {
            int a;
            a = 0;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec eliminar_pago " + id), conexion);
                a = comando.ExecuteNonQuery();
            }
            return a;
        }

        public static Int64 con_alias_id_vendedor(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec nombre_id_vendedor '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Vendedor> Lista_vendedores()
        {
            List<Vendedor> code = new List<Vendedor>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_vendedor_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Vendedor a = new Vendedor();
                    a.id = read.GetInt64(0);
                    a.Alias = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }

        public static string con_id_nombre_cliente(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec id_nombre_cliente " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }

        public static Int64 con_nombre_id_cliente(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec nombre_id_cliente '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Cliente> Lista_cliente()
        {
            List<Cliente> code = new List<Cliente>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_cliente_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Cliente a = new Cliente();
                    a.id = read.GetInt64(0);
                    a.Nombre = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }

        public static string con_id_alias_vendedor(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec id_nombre_vendedor " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }
        public static string con_id_nombre_sucursal(Int64 id)
        {
            string a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec id_nombre_sucursal " + id + ""), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetString(0);
            }
            return a;
        }

        public static Int64 con_nombre_id_sucursal(string nombre)
        {
            Int64 a;
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec nombre_id_sucursal '" + nombre + "'"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                read.Read();
                a = read.GetInt64(0);
            }
            return a;
        }

        public static List<Sucursal> Lista_sucursal()
        {
            List<Sucursal> code = new List<Sucursal>();
            using (SqlConnection conexion = Conexion.Conexion.Coneccion_entrada())
            {
                SqlCommand comando = new SqlCommand(string.Format("exec lista_sucursal_seleccion"), conexion);
                SqlDataReader read = comando.ExecuteReader();
                while (read.Read())
                {
                    Sucursal a = new Sucursal();
                    a.id_sucursal = read.GetInt64(0);
                    a.nombre_sucursal = read.GetString(1);
                    code.Add(a);
                }
            }
            return code;
        }
    }
}
