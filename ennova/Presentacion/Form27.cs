﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form27 : Form
    {
        public Form27()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form22 a = new Form22();
            a.Show();
            this.Close();
        }

        private void txtbusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
                if (txtbusqueda.Text == "")
                {
                    dgvmostrar.DataSource = Sucursal.pedir();
                    dgvmostrar.Refresh();
                }
                else
                {
                    string code = txtbusqueda.Text;
                    dgvmostrar.DataSource = Sucursal.buscar(code);
                    dgvmostrar.Refresh();
                }
            }
        }

        private void Form27_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Sucursal.pedir();
        }

        
    }
}
