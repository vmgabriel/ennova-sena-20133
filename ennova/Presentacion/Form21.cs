﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ennova.Presentacion
{
    public partial class Form21 : Form
    {
        public Form21()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void btnvendedor_Click(object sender, EventArgs e)
        {
            Form28 a = new Form28();
            a.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form29 a = new Form29();
            a.Show();
            this.Close();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            Form31 a = new Form31();
            a.Show();
            this.Close();
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            Form32 a = new Form32();
            a.Show();
            this.Close();
        }
    }
}
