﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form40 : Form
    {
        public Form40()
        {
            InitializeComponent();
        }

        private void Form40_Load(object sender, EventArgs e)
        {
            Pago cargar = Pago.llamar(Global.modif_pago);
            txtiva.Text = Convert.ToString(cargar.iva_pago);
            txtsubtotal.Text = Convert.ToString(cargar.sub_total_pago);
            txttipo.Text = cargar.tipo_pago;
            txttotal.Text = Convert.ToString(cargar.total_pago);
            cbsucursal.DataSource = Pago.Lista_sucursal();
            cbsucursal.DisplayMember = "nombre_sucursal";
            cbsucursal.ValueMember = "id_sucursal";
            cbcliente.Text = Pago.con_id_nombre_sucursal(cargar.Fk_sucursal_pago);
            cbcliente.DataSource = Pago.Lista_cliente();
            cbcliente.DisplayMember = "Nombre";
            cbcliente.ValueMember = "id";
            cbcliente.Text = Pago.con_id_nombre_cliente(cargar.Fk_cliente_pago);
            cbvendedor.DataSource = Pago.Lista_vendedores();
            cbvendedor.DisplayMember = "Alias";
            cbvendedor.ValueMember = "id";
            cbvendedor.Text = Pago.con_id_alias_vendedor(cargar.Fk_vendedor_pago);
        }
        private void sololetras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        private void solonumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form20 a = new Form20();
            a.Show();
            this.Close();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtiva.Clear();
            txttipo.Clear();
            txttotal.Clear();
            txttipo.Focus();
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            string sucursal, vendedor, cliente, tipo;
            Int64 iva, total, subtotal;
            tipo = txttipo.Text;
            sucursal = cbsucursal.Text;
            vendedor = cbvendedor.Text;
            cliente = cbcliente.Text;
            iva = Convert.ToInt64(txtiva.Text);
            total = Convert.ToInt64(txttotal.Text);
            subtotal = Convert.ToInt64(txtsubtotal.Text);
            if (sucursal == "" || vendedor == "" || cliente == "" || iva == 0 || total == 0 || subtotal == 0)
            {
                MessageBox.Show("No se ha llenado el campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Pago nn = new Pago();
                nn.id_pago = Global.modif_pago;
                nn.Fk_cliente_pago = Pago.con_nombre_id_cliente(cliente);
                nn.Fk_sucursal_pago = Pago.con_nombre_id_sucursal(sucursal);
                nn.Fk_vendedor_pago = Pago.con_alias_id_vendedor(vendedor);
                nn.iva_pago = iva;
                nn.sub_total_pago = subtotal;
                nn.tipo_pago = tipo;
                nn.total_pago = total;
                int res = Pago.modificar(nn);
                if (res != 0)
                {
                    MessageBox.Show("Datos Guardados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form20 ad = new Form20();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
