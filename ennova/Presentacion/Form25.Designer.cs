﻿namespace ennova.Presentacion
{
    partial class Form25
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form25));
            this.btnregistrar = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.btnborrar = new System.Windows.Forms.Button();
            this.gbregistro = new System.Windows.Forms.GroupBox();
            this.txtnit = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcorreo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtdire = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtciudad = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtpais = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txttel = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbltitulo = new System.Windows.Forms.Label();
            this.gbregistro.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnregistrar
            // 
            this.btnregistrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnregistrar.BackgroundImage")));
            this.btnregistrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnregistrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnregistrar.FlatAppearance.BorderSize = 2;
            this.btnregistrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnregistrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnregistrar.Font = new System.Drawing.Font("Constantia", 12F);
            this.btnregistrar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnregistrar.Location = new System.Drawing.Point(12, 412);
            this.btnregistrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnregistrar.Name = "btnregistrar";
            this.btnregistrar.Size = new System.Drawing.Size(158, 67);
            this.btnregistrar.TabIndex = 47;
            this.btnregistrar.Text = "Registrar";
            this.btnregistrar.UseVisualStyleBackColor = true;
            this.btnregistrar.Click += new System.EventHandler(this.btnregistrar_Click);
            // 
            // btnsalir
            // 
            this.btnsalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalir.BackgroundImage")));
            this.btnsalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnsalir.FlatAppearance.BorderSize = 2;
            this.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnsalir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsalir.Font = new System.Drawing.Font("Constantia", 12F);
            this.btnsalir.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnsalir.Location = new System.Drawing.Point(404, 412);
            this.btnsalir.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(158, 67);
            this.btnsalir.TabIndex = 46;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // btnborrar
            // 
            this.btnborrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnborrar.BackgroundImage")));
            this.btnborrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnborrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnborrar.FlatAppearance.BorderSize = 2;
            this.btnborrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnborrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnborrar.Font = new System.Drawing.Font("Constantia", 12F);
            this.btnborrar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnborrar.Location = new System.Drawing.Point(203, 412);
            this.btnborrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnborrar.Name = "btnborrar";
            this.btnborrar.Size = new System.Drawing.Size(158, 67);
            this.btnborrar.TabIndex = 45;
            this.btnborrar.Text = "Borrar";
            this.btnborrar.UseVisualStyleBackColor = true;
            this.btnborrar.Click += new System.EventHandler(this.btnborrar_Click);
            // 
            // gbregistro
            // 
            this.gbregistro.Controls.Add(this.txtnit);
            this.gbregistro.Controls.Add(this.label4);
            this.gbregistro.Controls.Add(this.txtcorreo);
            this.gbregistro.Controls.Add(this.label1);
            this.gbregistro.Controls.Add(this.txtdire);
            this.gbregistro.Controls.Add(this.label9);
            this.gbregistro.Controls.Add(this.txtciudad);
            this.gbregistro.Controls.Add(this.label7);
            this.gbregistro.Controls.Add(this.txtpais);
            this.gbregistro.Controls.Add(this.label5);
            this.gbregistro.Controls.Add(this.txttel);
            this.gbregistro.Controls.Add(this.label2);
            this.gbregistro.Controls.Add(this.txtnombre);
            this.gbregistro.Controls.Add(this.label3);
            this.gbregistro.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gbregistro.Location = new System.Drawing.Point(12, 64);
            this.gbregistro.Name = "gbregistro";
            this.gbregistro.Size = new System.Drawing.Size(577, 328);
            this.gbregistro.TabIndex = 44;
            this.gbregistro.TabStop = false;
            this.gbregistro.Text = "Sucursal";
            // 
            // txtnit
            // 
            this.txtnit.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtnit.Location = new System.Drawing.Point(281, 230);
            this.txtnit.Name = "txtnit";
            this.txtnit.Size = new System.Drawing.Size(269, 23);
            this.txtnit.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(400, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 19);
            this.label4.TabIndex = 30;
            this.label4.Text = "Ingrese el NIT:";
            // 
            // txtcorreo
            // 
            this.txtcorreo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtcorreo.Location = new System.Drawing.Point(281, 149);
            this.txtcorreo.Name = "txtcorreo";
            this.txtcorreo.Size = new System.Drawing.Size(269, 23);
            this.txtcorreo.TabIndex = 27;
            this.txtcorreo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sinespacios_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(400, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 19);
            this.label1.TabIndex = 28;
            this.label1.Text = "Ingrese el correo:";
            // 
            // txtdire
            // 
            this.txtdire.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtdire.Location = new System.Drawing.Point(28, 127);
            this.txtdire.Name = "txtdire";
            this.txtdire.Size = new System.Drawing.Size(188, 23);
            this.txtdire.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(24, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(176, 19);
            this.label9.TabIndex = 26;
            this.label9.Text = "Ingrese la Direccion:";
            // 
            // txtciudad
            // 
            this.txtciudad.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtciudad.Location = new System.Drawing.Point(28, 189);
            this.txtciudad.Name = "txtciudad";
            this.txtciudad.Size = new System.Drawing.Size(188, 23);
            this.txtciudad.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(24, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 19);
            this.label7.TabIndex = 22;
            this.label7.Text = "Ingrese la ciudad:";
            // 
            // txtpais
            // 
            this.txtpais.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtpais.Location = new System.Drawing.Point(28, 251);
            this.txtpais.Name = "txtpais";
            this.txtpais.Size = new System.Drawing.Size(188, 23);
            this.txtpais.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(24, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 19);
            this.label5.TabIndex = 15;
            this.label5.Text = "Ingrese el Pais:";
            // 
            // txttel
            // 
            this.txttel.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txttel.Location = new System.Drawing.Point(352, 67);
            this.txttel.Name = "txttel";
            this.txttel.Size = new System.Drawing.Size(198, 23);
            this.txttel.TabIndex = 10;
            this.txttel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(387, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(163, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ingrese el telefono:";
            // 
            // txtnombre
            // 
            this.txtnombre.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtnombre.Location = new System.Drawing.Point(28, 67);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(188, 23);
            this.txtnombre.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(24, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ingrese el Nombre:";
            // 
            // lbltitulo
            // 
            this.lbltitulo.AutoSize = true;
            this.lbltitulo.Font = new System.Drawing.Font("Cooper Black", 20.25F);
            this.lbltitulo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltitulo.Location = new System.Drawing.Point(184, 18);
            this.lbltitulo.Name = "lbltitulo";
            this.lbltitulo.Size = new System.Drawing.Size(272, 31);
            this.lbltitulo.TabIndex = 48;
            this.lbltitulo.Text = "Modificar Sucursal";
            // 
            // Form25
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 489);
            this.ControlBox = false;
            this.Controls.Add(this.lbltitulo);
            this.Controls.Add(this.btnregistrar);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.btnborrar);
            this.Controls.Add(this.gbregistro);
            this.Name = "Form25";
            this.Text = "Modificar";
            this.Load += new System.EventHandler(this.Form25_Load);
            this.gbregistro.ResumeLayout(false);
            this.gbregistro.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnregistrar;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.Button btnborrar;
        private System.Windows.Forms.GroupBox gbregistro;
        private System.Windows.Forms.TextBox txtnit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcorreo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdire;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtciudad;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtpais;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txttel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbltitulo;
    }
}