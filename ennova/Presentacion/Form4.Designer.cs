﻿namespace ennova.Presentacion
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.pnseleccion = new System.Windows.Forms.Panel();
            this.dgvseleccion = new System.Windows.Forms.DataGridView();
            this.btnsalir = new System.Windows.Forms.Button();
            this.btnseleccionar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.PE2 = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnseleccion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvseleccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PE2)).BeginInit();
            this.SuspendLayout();
            // 
            // pnseleccion
            // 
            this.pnseleccion.Controls.Add(this.dgvseleccion);
            this.pnseleccion.Location = new System.Drawing.Point(13, 13);
            this.pnseleccion.Name = "pnseleccion";
            this.pnseleccion.Size = new System.Drawing.Size(671, 446);
            this.pnseleccion.TabIndex = 0;
            // 
            // dgvseleccion
            // 
            this.dgvseleccion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvseleccion.Location = new System.Drawing.Point(2, 3);
            this.dgvseleccion.Name = "dgvseleccion";
            this.dgvseleccion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvseleccion.Size = new System.Drawing.Size(666, 443);
            this.dgvseleccion.TabIndex = 0;
            // 
            // btnsalir
            // 
            this.btnsalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalir.BackgroundImage")));
            this.btnsalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnsalir.FlatAppearance.BorderSize = 2;
            this.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnsalir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsalir.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalir.Location = new System.Drawing.Point(516, 466);
            this.btnsalir.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(158, 67);
            this.btnsalir.TabIndex = 14;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // btnseleccionar
            // 
            this.btnseleccionar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnseleccionar.BackgroundImage")));
            this.btnseleccionar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnseleccionar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnseleccionar.FlatAppearance.BorderSize = 2;
            this.btnseleccionar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnseleccionar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnseleccionar.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnseleccionar.Location = new System.Drawing.Point(15, 466);
            this.btnseleccionar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnseleccionar.Name = "btnseleccionar";
            this.btnseleccionar.Size = new System.Drawing.Size(158, 67);
            this.btnseleccionar.TabIndex = 13;
            this.btnseleccionar.Text = "Seleccionar";
            this.btnseleccionar.UseVisualStyleBackColor = true;
            this.btnseleccionar.Click += new System.EventHandler(this.btnseleccionar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(232, 489);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 33);
            this.label1.TabIndex = 33;
            this.label1.Text = "Seleccionar";
            // 
            // PE2
            // 
            this.PE2.ContainerControl = this;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(696, 540);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.pnseleccion);
            this.Controls.Add(this.btnseleccionar);
            this.Name = "Form4";
            this.Text = "Modificar vendedor";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.pnseleccion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvseleccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PE2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnseleccion;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.Button btnseleccionar;
        private System.Windows.Forms.DataGridView dgvseleccion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider PE2;
    }
}