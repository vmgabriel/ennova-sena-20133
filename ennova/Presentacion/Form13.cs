﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form13 : Form
    {
        public Form13()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtcom.Clear();
            txtcorreo.Clear();
            txtdire.Clear();
            txtnombre.Clear();
            txtnumero.Clear();
            txtnombre.Focus();
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            string nombre, correo, direccion, telefono,descripcion;
            nombre = txtnombre.Text;
            descripcion = txtcom.Text;
            correo = txtcorreo.Text;
            direccion = txtdire.Text;
            telefono = txtnumero.Text;
            if (nombre == "" || descripcion=="" || correo=="" || direccion=="" || telefono=="")
            {
                MessageBox.Show("No se han llenado algunos campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Proveedor code = new Proveedor();
                code.nombre_proveedor = nombre;
                code.correo_proveedor = correo;
                code.descripcion_proveedor = descripcion;
                code.direccion_proveedor = direccion;
                code.telefono_proveedor = telefono;
                int res = Proveedor.registrar(code);
                if (res != 0)
                {
                    MessageBox.Show("Datos Guardados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form2 ad = new Form2();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void sololetras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        private void solonumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
