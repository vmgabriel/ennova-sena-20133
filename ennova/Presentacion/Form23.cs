﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form23 : Form
    {
        public Form23()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form22 a = new Form22();
            a.Show();
            this.Close();
        }

        private void sololetras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        private void solonumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtciudad.Clear();
            txtcorreo.Clear();
            txtdire.Clear();
            txtnit.Clear();
            txtnombre.Clear();
            txtpais.Clear();
            txttel.Clear();
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            string nombre, direccion, ciudad, pais, telefono, correo, nit;
            nombre = txtnombre.Text;
            direccion = txtdire.Text;
            ciudad = txtciudad.Text;
            pais = txtpais.Text;
            telefono = txttel.Text;
            correo = txtcorreo.Text;
            nit = txtnit.Text;
            if (nombre == "" || direccion == "" || ciudad == "" || pais == "" || telefono == "" || correo == "" || nit == "")
            {
                MessageBox.Show("No se ha llenado el campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Sucursal a = new Sucursal();
                a.nombre_sucursal = nombre;
                a.direccion_sucursal = direccion;
                a.ciudad_sucursal = ciudad;
                a.pais_sucursal = pais;
                a.telefono_sucursal = telefono;
                a.correo_sucursal = correo;
                a.nit_estandar_sucursal = nit;
                int res = Sucursal.registrar(a);
                if (res != 0)
                {
                    MessageBox.Show("Datos Guardados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form22 ad = new Form22();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
