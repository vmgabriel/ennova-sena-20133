﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;
using System.Data.SqlClient;

namespace ennova.Presentacion
{
    public partial class Form45 : Form
    {
        public Form45()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtaltura.Clear();
            txtanchura.Clear();
            txtcategoria.Clear();
            txtclase.Clear();
            txtcosto.Clear();
            txtespuma.Clear();
            txtgarantia.Clear();
            txtlargo.Clear();
            txtnombre.Clear();
            txttapizado.Clear();
            txttela.Clear();
            txttipo.Clear();
            txtnombre.Focus();
        }

        private void sololetras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        private void solonumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            string categoria, clase, espuma, nombre, tapizado, tela, tipo;
            Int64 altura, anchura, largo, costo, garantia, proveedor;
            categoria = txtcategoria.Text;
            clase = txtclase.Text;
            espuma = txtespuma.Text;
            nombre = txtnombre.Text;
            tapizado = txttapizado.Text;
            tela = txttela.Text;
            tipo = txttipo.Text;
            altura = Convert.ToInt64(txtaltura.Text);
            anchura = Convert.ToInt64(txtanchura.Text);
            largo = Convert.ToInt64(txtlargo.Text);
            costo = Convert.ToInt64(txtcosto.Text);
            garantia = Convert.ToInt64(txtgarantia.Text);
            if (categoria == "" || clase == "" || espuma == "" || nombre == "" || tapizado == "" || tela == "" || tipo == "" || altura == 0 || anchura == 0 || largo == 0 || costo == 0)
            {
                MessageBox.Show("No se han llenado los campos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                proveedor = Producto.con_nombre_id_proveedor(cbproveedor.Text);
                Producto nuevo = new Producto();
                nuevo.altura_producto = altura;
                nuevo.anchura_producto = anchura;
                nuevo.categoria_producto = categoria;
                nuevo.clase_producto = clase;
                nuevo.costo_producto = costo;
                nuevo.espuma_producto = espuma;
                nuevo.Fk_proveedor_producto = proveedor;
                nuevo.garantia_producto = garantia;
                nuevo.largo_producto = largo;
                nuevo.nombre_producto = nombre;
                nuevo.tapizado_producto = tapizado;
                nuevo.tela_producto = tela;
                nuevo.tipo_producto = tipo;
                nuevo.id_producto = Global.modif_producto;
                int res = Producto.modificar(nuevo);
                if (res > 0)
                {
                    MessageBox.Show("Datos Modificados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form2 ad = new Form2();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar Modificar el registro en la base", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Form45_Load(object sender, EventArgs e)
        {
            Producto cargar = Producto.llamar(Global.modif_producto);
            txtaltura.Text = Convert.ToString(cargar.altura_producto);
            txtanchura.Text = Convert.ToString(cargar.anchura_producto);
            txtcategoria.Text = cargar.categoria_producto;
            txtclase.Text = cargar.clase_producto;
            txtcosto.Text = Convert.ToString(cargar.costo_producto);
            txtespuma.Text = cargar.espuma_producto;
            txtgarantia.Text = Convert.ToString(cargar.garantia_producto);
            txtlargo.Text = Convert.ToString(cargar.largo_producto);
            txtnombre.Text = cargar.nombre_producto;
            txttapizado.Text = cargar.tapizado_producto;
            txttela.Text = cargar.tela_producto;
            txttipo.Text = cargar.tipo_producto;
            cbproveedor.DataSource = Producto.Lista_proveedor();
            cbproveedor.DisplayMember = "nombre_proveedor";
            cbproveedor.ValueMember = "id";
        }
    }
}
