﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ennova.Presentacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtusuario.Clear();
            txtcontraseña.Clear();
            txtusuario.Focus();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Screen a = new Screen();
            this.Close();
            a.Close();
            Application.Exit();
        }

        private void btningresar_Click(object sender, EventArgs e)
        {
            string a, b;
            a = txtusuario.Text;
            b= txtcontraseña.Text;
            if (a == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                PE1.SetError(txtusuario, "error");
            }
            else if (b == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                PE1.SetError(txtcontraseña, "Error");
            }
            else
            {
                int ase = Logica.Vendedor.ingreso(a, b);
                if (ase == 1)
                {
                    Form2 ad = new Form2();
                    this.Close();
                    ad.Show();
                }
                else
                {
                    MessageBox.Show("Datos introducidos invalidos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    PE1.SetError(txtcontraseña, "Error");
                    PE1.SetError(txtusuario, "error");
                }

            }
        }

    }
}
