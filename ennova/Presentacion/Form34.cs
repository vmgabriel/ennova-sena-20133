﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form34 : Form
    {
        public Form34()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form18 a = new Form18();
            a.Show();
            this.Close();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            Global.modif_factura_compra = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            Form35 v = new Form35();
            v.Show();
            this.Close();
        }

        private void Form34_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Fac_compra.pedir();
        }
    }
}
