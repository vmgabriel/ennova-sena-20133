﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form26 : Form
    {
        public Form26()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form22 a = new Form22();
            a.Show();
            this.Close();
        }

        private void Form26_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Sucursal.pedir();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            int aq;
            Int64 id = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            DialogResult a = MessageBox.Show("Desea eliminar el dato????...recuerde que sera removido", "Avizo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (a == DialogResult.Yes)
            {
                aq = Sucursal.eliminar(id);
                if (aq >= 1)
                {
                    MessageBox.Show("Sucursal Eliminada", "Avizo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form22 entry = new Form22();
                    entry.Show();
                    this.Close();
                }
            }
        }

    }
}
