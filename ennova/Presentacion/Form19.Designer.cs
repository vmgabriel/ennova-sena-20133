﻿namespace ennova.Presentacion
{
    partial class Form19
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form19));
            this.btnsalir = new System.Windows.Forms.Button();
            this.btnnuevafactura = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gbmenu = new System.Windows.Forms.GroupBox();
            this.btneliminarfactura = new System.Windows.Forms.Button();
            this.btnbuscarfactura = new System.Windows.Forms.Button();
            this.btnmodificarfactura = new System.Windows.Forms.Button();
            this.gbnuevofactura = new System.Windows.Forms.GroupBox();
            this.gbfactura = new System.Windows.Forms.GroupBox();
            this.btnimprimirfact = new System.Windows.Forms.Button();
            this.btnborrardet = new System.Windows.Forms.Button();
            this.dgvfactura = new System.Windows.Forms.DataGridView();
            this.txtcantidad = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnbuscarproducto = new System.Windows.Forms.Button();
            this.btnseleccionarproducto = new System.Windows.Forms.Button();
            this.txtbuscar = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvproductos = new System.Windows.Forms.DataGridView();
            this.cbsucursal = new System.Windows.Forms.ComboBox();
            this.lblfecha = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.gbcliente = new System.Windows.Forms.GroupBox();
            this.btnseleccionarcliente = new System.Windows.Forms.Button();
            this.dgvseleccioncliente = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpfecha_entrega = new System.Windows.Forms.DateTimePicker();
            this.txtcomentarios = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblsubtotal = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbltotal = new System.Windows.Forms.Label();
            this.txtiva = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdescuento = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txttipo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.gbmodificarfv = new System.Windows.Forms.GroupBox();
            this.gbModificacionfv = new System.Windows.Forms.GroupBox();
            this.btnimprimirmodi = new System.Windows.Forms.Button();
            this.btnborrarmodi = new System.Windows.Forms.Button();
            this.dgvfacturamodi = new System.Windows.Forms.DataGridView();
            this.txtcantidadmodi = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnbuscarmodi = new System.Windows.Forms.Button();
            this.btnagregarmodi = new System.Windows.Forms.Button();
            this.txtbusquedamodi = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dgvmodibusqueda = new System.Windows.Forms.DataGridView();
            this.cbsucursalmodi = new System.Windows.Forms.ComboBox();
            this.lblfechcreomodi = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.gbseleccionfactura = new System.Windows.Forms.GroupBox();
            this.btnseleccionarfacturamodi = new System.Windows.Forms.Button();
            this.dgvfacturasmod = new System.Windows.Forms.DataGridView();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.dtpfechaentremodi = new System.Windows.Forms.DateTimePicker();
            this.txtcomentariomodi = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.lblsubtotalmodi = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbltotalmodi = new System.Windows.Forms.Label();
            this.txtivamodi = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtdescuentomodi = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txttipomodi = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.gbbuscar = new System.Windows.Forms.GroupBox();
            this.btnborrarbusc = new System.Windows.Forms.Button();
            this.dgvbusquedabusc = new System.Windows.Forms.DataGridView();
            this.txtbusquedabusc = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.btnbuscarbusc = new System.Windows.Forms.Button();
            this.gbeliminar = new System.Windows.Forms.GroupBox();
            this.btneliminar = new System.Windows.Forms.Button();
            this.dgveliminar = new System.Windows.Forms.DataGridView();
            this.gbmenu.SuspendLayout();
            this.gbnuevofactura.SuspendLayout();
            this.gbfactura.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvfactura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvproductos)).BeginInit();
            this.gbcliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvseleccioncliente)).BeginInit();
            this.gbmodificarfv.SuspendLayout();
            this.gbModificacionfv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvfacturamodi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvmodibusqueda)).BeginInit();
            this.gbseleccionfactura.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvfacturasmod)).BeginInit();
            this.gbbuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbusquedabusc)).BeginInit();
            this.gbeliminar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgveliminar)).BeginInit();
            this.SuspendLayout();
            // 
            // btnsalir
            // 
            resources.ApplyResources(this.btnsalir, "btnsalir");
            this.btnsalir.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnsalir.FlatAppearance.BorderSize = 0;
            this.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnsalir.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // btnnuevafactura
            // 
            resources.ApplyResources(this.btnnuevafactura, "btnnuevafactura");
            this.btnnuevafactura.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnnuevafactura.FlatAppearance.BorderSize = 0;
            this.btnnuevafactura.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnnuevafactura.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnnuevafactura.Name = "btnnuevafactura";
            this.btnnuevafactura.UseVisualStyleBackColor = true;
            this.btnnuevafactura.Click += new System.EventHandler(this.btnnuevafactura_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // gbmenu
            // 
            this.gbmenu.Controls.Add(this.btneliminarfactura);
            this.gbmenu.Controls.Add(this.btnbuscarfactura);
            this.gbmenu.Controls.Add(this.btnmodificarfactura);
            this.gbmenu.Controls.Add(this.btnnuevafactura);
            resources.ApplyResources(this.gbmenu, "gbmenu");
            this.gbmenu.Name = "gbmenu";
            this.gbmenu.TabStop = false;
            // 
            // btneliminarfactura
            // 
            resources.ApplyResources(this.btneliminarfactura, "btneliminarfactura");
            this.btneliminarfactura.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btneliminarfactura.FlatAppearance.BorderSize = 0;
            this.btneliminarfactura.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btneliminarfactura.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btneliminarfactura.Name = "btneliminarfactura";
            this.btneliminarfactura.UseVisualStyleBackColor = true;
            this.btneliminarfactura.Click += new System.EventHandler(this.btneliminarfactura_Click);
            // 
            // btnbuscarfactura
            // 
            resources.ApplyResources(this.btnbuscarfactura, "btnbuscarfactura");
            this.btnbuscarfactura.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnbuscarfactura.FlatAppearance.BorderSize = 0;
            this.btnbuscarfactura.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnbuscarfactura.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnbuscarfactura.Name = "btnbuscarfactura";
            this.btnbuscarfactura.UseVisualStyleBackColor = true;
            this.btnbuscarfactura.Click += new System.EventHandler(this.btnbuscarfactura_Click);
            // 
            // btnmodificarfactura
            // 
            resources.ApplyResources(this.btnmodificarfactura, "btnmodificarfactura");
            this.btnmodificarfactura.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnmodificarfactura.FlatAppearance.BorderSize = 0;
            this.btnmodificarfactura.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnmodificarfactura.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnmodificarfactura.Name = "btnmodificarfactura";
            this.btnmodificarfactura.UseVisualStyleBackColor = true;
            this.btnmodificarfactura.Click += new System.EventHandler(this.btnmodificarfactura_Click);
            // 
            // gbnuevofactura
            // 
            this.gbnuevofactura.Controls.Add(this.gbfactura);
            this.gbnuevofactura.Controls.Add(this.cbsucursal);
            this.gbnuevofactura.Controls.Add(this.lblfecha);
            this.gbnuevofactura.Controls.Add(this.label14);
            this.gbnuevofactura.Controls.Add(this.gbcliente);
            this.gbnuevofactura.Controls.Add(this.label12);
            this.gbnuevofactura.Controls.Add(this.label11);
            this.gbnuevofactura.Controls.Add(this.label6);
            this.gbnuevofactura.Controls.Add(this.dtpfecha_entrega);
            this.gbnuevofactura.Controls.Add(this.txtcomentarios);
            this.gbnuevofactura.Controls.Add(this.label10);
            this.gbnuevofactura.Controls.Add(this.lblsubtotal);
            this.gbnuevofactura.Controls.Add(this.label8);
            this.gbnuevofactura.Controls.Add(this.lbltotal);
            this.gbnuevofactura.Controls.Add(this.txtiva);
            this.gbnuevofactura.Controls.Add(this.label5);
            this.gbnuevofactura.Controls.Add(this.txtdescuento);
            this.gbnuevofactura.Controls.Add(this.label4);
            this.gbnuevofactura.Controls.Add(this.label2);
            this.gbnuevofactura.Controls.Add(this.txttipo);
            this.gbnuevofactura.Controls.Add(this.label3);
            resources.ApplyResources(this.gbnuevofactura, "gbnuevofactura");
            this.gbnuevofactura.Name = "gbnuevofactura";
            this.gbnuevofactura.TabStop = false;
            // 
            // gbfactura
            // 
            this.gbfactura.Controls.Add(this.gbeliminar);
            this.gbfactura.Controls.Add(this.btnimprimirfact);
            this.gbfactura.Controls.Add(this.btnborrardet);
            this.gbfactura.Controls.Add(this.dgvfactura);
            this.gbfactura.Controls.Add(this.txtcantidad);
            this.gbfactura.Controls.Add(this.label9);
            this.gbfactura.Controls.Add(this.btnbuscarproducto);
            this.gbfactura.Controls.Add(this.btnseleccionarproducto);
            this.gbfactura.Controls.Add(this.txtbuscar);
            this.gbfactura.Controls.Add(this.label7);
            this.gbfactura.Controls.Add(this.dgvproductos);
            resources.ApplyResources(this.gbfactura, "gbfactura");
            this.gbfactura.Name = "gbfactura";
            this.gbfactura.TabStop = false;
            // 
            // btnimprimirfact
            // 
            resources.ApplyResources(this.btnimprimirfact, "btnimprimirfact");
            this.btnimprimirfact.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnimprimirfact.FlatAppearance.BorderSize = 0;
            this.btnimprimirfact.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnimprimirfact.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnimprimirfact.Name = "btnimprimirfact";
            this.btnimprimirfact.UseVisualStyleBackColor = true;
            this.btnimprimirfact.Click += new System.EventHandler(this.btnimprimirfact_Click);
            // 
            // btnborrardet
            // 
            resources.ApplyResources(this.btnborrardet, "btnborrardet");
            this.btnborrardet.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnborrardet.FlatAppearance.BorderSize = 0;
            this.btnborrardet.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnborrardet.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnborrardet.Name = "btnborrardet";
            this.btnborrardet.UseVisualStyleBackColor = true;
            this.btnborrardet.Click += new System.EventHandler(this.btnborrardet_Click);
            // 
            // dgvfactura
            // 
            this.dgvfactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvfactura, "dgvfactura");
            this.dgvfactura.Name = "dgvfactura";
            this.dgvfactura.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // txtcantidad
            // 
            resources.ApplyResources(this.txtcantidad, "txtcantidad");
            this.txtcantidad.Name = "txtcantidad";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // btnbuscarproducto
            // 
            resources.ApplyResources(this.btnbuscarproducto, "btnbuscarproducto");
            this.btnbuscarproducto.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnbuscarproducto.FlatAppearance.BorderSize = 0;
            this.btnbuscarproducto.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnbuscarproducto.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnbuscarproducto.Name = "btnbuscarproducto";
            this.btnbuscarproducto.UseVisualStyleBackColor = true;
            this.btnbuscarproducto.Click += new System.EventHandler(this.btnseleccionarproducto_Click);
            // 
            // btnseleccionarproducto
            // 
            resources.ApplyResources(this.btnseleccionarproducto, "btnseleccionarproducto");
            this.btnseleccionarproducto.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnseleccionarproducto.FlatAppearance.BorderSize = 0;
            this.btnseleccionarproducto.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnseleccionarproducto.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnseleccionarproducto.Name = "btnseleccionarproducto";
            this.btnseleccionarproducto.UseVisualStyleBackColor = true;
            this.btnseleccionarproducto.Click += new System.EventHandler(this.btnseleccionarproducto_Click_1);
            // 
            // txtbuscar
            // 
            resources.ApplyResources(this.txtbuscar, "txtbuscar");
            this.txtbuscar.Name = "txtbuscar";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // dgvproductos
            // 
            this.dgvproductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvproductos, "dgvproductos");
            this.dgvproductos.Name = "dgvproductos";
            this.dgvproductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // cbsucursal
            // 
            this.cbsucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cbsucursal, "cbsucursal");
            this.cbsucursal.FormattingEnabled = true;
            this.cbsucursal.Name = "cbsucursal";
            // 
            // lblfecha
            // 
            resources.ApplyResources(this.lblfecha, "lblfecha");
            this.lblfecha.Name = "lblfecha";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // gbcliente
            // 
            this.gbcliente.Controls.Add(this.btnseleccionarcliente);
            this.gbcliente.Controls.Add(this.dgvseleccioncliente);
            this.gbcliente.Controls.Add(this.label15);
            resources.ApplyResources(this.gbcliente, "gbcliente");
            this.gbcliente.Name = "gbcliente";
            this.gbcliente.TabStop = false;
            // 
            // btnseleccionarcliente
            // 
            resources.ApplyResources(this.btnseleccionarcliente, "btnseleccionarcliente");
            this.btnseleccionarcliente.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnseleccionarcliente.FlatAppearance.BorderSize = 0;
            this.btnseleccionarcliente.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnseleccionarcliente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnseleccionarcliente.Name = "btnseleccionarcliente";
            this.btnseleccionarcliente.UseVisualStyleBackColor = true;
            this.btnseleccionarcliente.Click += new System.EventHandler(this.btnseleccionarcliente_Click);
            // 
            // dgvseleccioncliente
            // 
            this.dgvseleccioncliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvseleccioncliente, "dgvseleccioncliente");
            this.dgvseleccioncliente.Name = "dgvseleccioncliente";
            this.dgvseleccioncliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // dtpfecha_entrega
            // 
            resources.ApplyResources(this.dtpfecha_entrega, "dtpfecha_entrega");
            this.dtpfecha_entrega.Name = "dtpfecha_entrega";
            // 
            // txtcomentarios
            // 
            resources.ApplyResources(this.txtcomentarios, "txtcomentarios");
            this.txtcomentarios.Name = "txtcomentarios";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // lblsubtotal
            // 
            resources.ApplyResources(this.lblsubtotal, "lblsubtotal");
            this.lblsubtotal.Name = "lblsubtotal";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // lbltotal
            // 
            resources.ApplyResources(this.lbltotal, "lbltotal");
            this.lbltotal.Name = "lbltotal";
            // 
            // txtiva
            // 
            resources.ApplyResources(this.txtiva, "txtiva");
            this.txtiva.Name = "txtiva";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // txtdescuento
            // 
            resources.ApplyResources(this.txtdescuento, "txtdescuento");
            this.txtdescuento.Name = "txtdescuento";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // txttipo
            // 
            resources.ApplyResources(this.txttipo, "txttipo");
            this.txttipo.Name = "txttipo";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // gbmodificarfv
            // 
            this.gbmodificarfv.Controls.Add(this.gbModificacionfv);
            this.gbmodificarfv.Controls.Add(this.cbsucursalmodi);
            this.gbmodificarfv.Controls.Add(this.lblfechcreomodi);
            this.gbmodificarfv.Controls.Add(this.label18);
            this.gbmodificarfv.Controls.Add(this.gbseleccionfactura);
            this.gbmodificarfv.Controls.Add(this.label20);
            this.gbmodificarfv.Controls.Add(this.label21);
            this.gbmodificarfv.Controls.Add(this.label22);
            this.gbmodificarfv.Controls.Add(this.dtpfechaentremodi);
            this.gbmodificarfv.Controls.Add(this.txtcomentariomodi);
            this.gbmodificarfv.Controls.Add(this.label23);
            this.gbmodificarfv.Controls.Add(this.lblsubtotalmodi);
            this.gbmodificarfv.Controls.Add(this.label25);
            this.gbmodificarfv.Controls.Add(this.lbltotalmodi);
            this.gbmodificarfv.Controls.Add(this.txtivamodi);
            this.gbmodificarfv.Controls.Add(this.label27);
            this.gbmodificarfv.Controls.Add(this.txtdescuentomodi);
            this.gbmodificarfv.Controls.Add(this.label28);
            this.gbmodificarfv.Controls.Add(this.label29);
            this.gbmodificarfv.Controls.Add(this.txttipomodi);
            this.gbmodificarfv.Controls.Add(this.label30);
            resources.ApplyResources(this.gbmodificarfv, "gbmodificarfv");
            this.gbmodificarfv.Name = "gbmodificarfv";
            this.gbmodificarfv.TabStop = false;
            // 
            // gbModificacionfv
            // 
            this.gbModificacionfv.Controls.Add(this.btnimprimirmodi);
            this.gbModificacionfv.Controls.Add(this.btnborrarmodi);
            this.gbModificacionfv.Controls.Add(this.dgvfacturamodi);
            this.gbModificacionfv.Controls.Add(this.txtcantidadmodi);
            this.gbModificacionfv.Controls.Add(this.label13);
            this.gbModificacionfv.Controls.Add(this.btnbuscarmodi);
            this.gbModificacionfv.Controls.Add(this.btnagregarmodi);
            this.gbModificacionfv.Controls.Add(this.txtbusquedamodi);
            this.gbModificacionfv.Controls.Add(this.label16);
            this.gbModificacionfv.Controls.Add(this.dgvmodibusqueda);
            resources.ApplyResources(this.gbModificacionfv, "gbModificacionfv");
            this.gbModificacionfv.Name = "gbModificacionfv";
            this.gbModificacionfv.TabStop = false;
            // 
            // btnimprimirmodi
            // 
            resources.ApplyResources(this.btnimprimirmodi, "btnimprimirmodi");
            this.btnimprimirmodi.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnimprimirmodi.FlatAppearance.BorderSize = 0;
            this.btnimprimirmodi.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnimprimirmodi.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnimprimirmodi.Name = "btnimprimirmodi";
            this.btnimprimirmodi.UseVisualStyleBackColor = true;
            this.btnimprimirmodi.Click += new System.EventHandler(this.btnimprimirmodi_Click);
            // 
            // btnborrarmodi
            // 
            resources.ApplyResources(this.btnborrarmodi, "btnborrarmodi");
            this.btnborrarmodi.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnborrarmodi.FlatAppearance.BorderSize = 0;
            this.btnborrarmodi.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnborrarmodi.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnborrarmodi.Name = "btnborrarmodi";
            this.btnborrarmodi.UseVisualStyleBackColor = true;
            this.btnborrarmodi.Click += new System.EventHandler(this.btnborrarmodi_Click);
            // 
            // dgvfacturamodi
            // 
            this.dgvfacturamodi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvfacturamodi, "dgvfacturamodi");
            this.dgvfacturamodi.Name = "dgvfacturamodi";
            this.dgvfacturamodi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // txtcantidadmodi
            // 
            resources.ApplyResources(this.txtcantidadmodi, "txtcantidadmodi");
            this.txtcantidadmodi.Name = "txtcantidadmodi";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // btnbuscarmodi
            // 
            resources.ApplyResources(this.btnbuscarmodi, "btnbuscarmodi");
            this.btnbuscarmodi.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnbuscarmodi.FlatAppearance.BorderSize = 0;
            this.btnbuscarmodi.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnbuscarmodi.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnbuscarmodi.Name = "btnbuscarmodi";
            this.btnbuscarmodi.UseVisualStyleBackColor = true;
            this.btnbuscarmodi.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnagregarmodi
            // 
            resources.ApplyResources(this.btnagregarmodi, "btnagregarmodi");
            this.btnagregarmodi.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnagregarmodi.FlatAppearance.BorderSize = 0;
            this.btnagregarmodi.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnagregarmodi.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnagregarmodi.Name = "btnagregarmodi";
            this.btnagregarmodi.UseVisualStyleBackColor = true;
            this.btnagregarmodi.Click += new System.EventHandler(this.btnagregarmodi_Click);
            // 
            // txtbusquedamodi
            // 
            resources.ApplyResources(this.txtbusquedamodi, "txtbusquedamodi");
            this.txtbusquedamodi.Name = "txtbusquedamodi";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // dgvmodibusqueda
            // 
            this.dgvmodibusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvmodibusqueda, "dgvmodibusqueda");
            this.dgvmodibusqueda.Name = "dgvmodibusqueda";
            this.dgvmodibusqueda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // cbsucursalmodi
            // 
            this.cbsucursalmodi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cbsucursalmodi, "cbsucursalmodi");
            this.cbsucursalmodi.FormattingEnabled = true;
            this.cbsucursalmodi.Name = "cbsucursalmodi";
            // 
            // lblfechcreomodi
            // 
            resources.ApplyResources(this.lblfechcreomodi, "lblfechcreomodi");
            this.lblfechcreomodi.Name = "lblfechcreomodi";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // gbseleccionfactura
            // 
            this.gbseleccionfactura.Controls.Add(this.btnseleccionarfacturamodi);
            this.gbseleccionfactura.Controls.Add(this.dgvfacturasmod);
            this.gbseleccionfactura.Controls.Add(this.label19);
            resources.ApplyResources(this.gbseleccionfactura, "gbseleccionfactura");
            this.gbseleccionfactura.Name = "gbseleccionfactura";
            this.gbseleccionfactura.TabStop = false;
            // 
            // btnseleccionarfacturamodi
            // 
            resources.ApplyResources(this.btnseleccionarfacturamodi, "btnseleccionarfacturamodi");
            this.btnseleccionarfacturamodi.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnseleccionarfacturamodi.FlatAppearance.BorderSize = 0;
            this.btnseleccionarfacturamodi.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnseleccionarfacturamodi.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnseleccionarfacturamodi.Name = "btnseleccionarfacturamodi";
            this.btnseleccionarfacturamodi.UseVisualStyleBackColor = true;
            this.btnseleccionarfacturamodi.Click += new System.EventHandler(this.button5_Click);
            // 
            // dgvfacturasmod
            // 
            this.dgvfacturasmod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvfacturasmod, "dgvfacturasmod");
            this.dgvfacturasmod.Name = "dgvfacturasmod";
            this.dgvfacturasmod.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // dtpfechaentremodi
            // 
            resources.ApplyResources(this.dtpfechaentremodi, "dtpfechaentremodi");
            this.dtpfechaentremodi.Name = "dtpfechaentremodi";
            // 
            // txtcomentariomodi
            // 
            resources.ApplyResources(this.txtcomentariomodi, "txtcomentariomodi");
            this.txtcomentariomodi.Name = "txtcomentariomodi";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // lblsubtotalmodi
            // 
            resources.ApplyResources(this.lblsubtotalmodi, "lblsubtotalmodi");
            this.lblsubtotalmodi.Name = "lblsubtotalmodi";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // lbltotalmodi
            // 
            resources.ApplyResources(this.lbltotalmodi, "lbltotalmodi");
            this.lbltotalmodi.Name = "lbltotalmodi";
            // 
            // txtivamodi
            // 
            resources.ApplyResources(this.txtivamodi, "txtivamodi");
            this.txtivamodi.Name = "txtivamodi";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.Name = "label27";
            // 
            // txtdescuentomodi
            // 
            resources.ApplyResources(this.txtdescuentomodi, "txtdescuentomodi");
            this.txtdescuentomodi.Name = "txtdescuentomodi";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.Name = "label28";
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // txttipomodi
            // 
            resources.ApplyResources(this.txttipomodi, "txttipomodi");
            this.txttipomodi.Name = "txttipomodi";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.Name = "label30";
            // 
            // gbbuscar
            // 
            this.gbbuscar.Controls.Add(this.btnborrarbusc);
            this.gbbuscar.Controls.Add(this.dgvbusquedabusc);
            this.gbbuscar.Controls.Add(this.txtbusquedabusc);
            this.gbbuscar.Controls.Add(this.label32);
            this.gbbuscar.Controls.Add(this.btnbuscarbusc);
            resources.ApplyResources(this.gbbuscar, "gbbuscar");
            this.gbbuscar.Name = "gbbuscar";
            this.gbbuscar.TabStop = false;
            // 
            // btnborrarbusc
            // 
            resources.ApplyResources(this.btnborrarbusc, "btnborrarbusc");
            this.btnborrarbusc.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnborrarbusc.FlatAppearance.BorderSize = 0;
            this.btnborrarbusc.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnborrarbusc.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnborrarbusc.Name = "btnborrarbusc";
            this.btnborrarbusc.UseVisualStyleBackColor = true;
            this.btnborrarbusc.Click += new System.EventHandler(this.btnborrarbusc_Click);
            // 
            // dgvbusquedabusc
            // 
            this.dgvbusquedabusc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvbusquedabusc, "dgvbusquedabusc");
            this.dgvbusquedabusc.Name = "dgvbusquedabusc";
            this.dgvbusquedabusc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // txtbusquedabusc
            // 
            resources.ApplyResources(this.txtbusquedabusc, "txtbusquedabusc");
            this.txtbusquedabusc.Name = "txtbusquedabusc";
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.Name = "label32";
            // 
            // btnbuscarbusc
            // 
            resources.ApplyResources(this.btnbuscarbusc, "btnbuscarbusc");
            this.btnbuscarbusc.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnbuscarbusc.FlatAppearance.BorderSize = 0;
            this.btnbuscarbusc.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnbuscarbusc.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnbuscarbusc.Name = "btnbuscarbusc";
            this.btnbuscarbusc.UseVisualStyleBackColor = true;
            this.btnbuscarbusc.Click += new System.EventHandler(this.btnbuscarbusc_Click);
            // 
            // gbeliminar
            // 
            this.gbeliminar.Controls.Add(this.btneliminar);
            this.gbeliminar.Controls.Add(this.dgveliminar);
            resources.ApplyResources(this.gbeliminar, "gbeliminar");
            this.gbeliminar.Name = "gbeliminar";
            this.gbeliminar.TabStop = false;
            // 
            // btneliminar
            // 
            resources.ApplyResources(this.btneliminar, "btneliminar");
            this.btneliminar.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btneliminar.FlatAppearance.BorderSize = 0;
            this.btneliminar.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btneliminar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.UseVisualStyleBackColor = true;
            this.btneliminar.Click += new System.EventHandler(this.btneliminar_Click);
            // 
            // dgveliminar
            // 
            this.dgveliminar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgveliminar, "dgveliminar");
            this.dgveliminar.Name = "dgveliminar";
            this.dgveliminar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // Form19
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add(this.gbbuscar);
            this.Controls.Add(this.gbmodificarfv);
            this.Controls.Add(this.gbnuevofactura);
            this.Controls.Add(this.gbmenu);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnsalir);
            this.Name = "Form19";
            this.gbmenu.ResumeLayout(false);
            this.gbnuevofactura.ResumeLayout(false);
            this.gbnuevofactura.PerformLayout();
            this.gbfactura.ResumeLayout(false);
            this.gbfactura.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvfactura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvproductos)).EndInit();
            this.gbcliente.ResumeLayout(false);
            this.gbcliente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvseleccioncliente)).EndInit();
            this.gbmodificarfv.ResumeLayout(false);
            this.gbmodificarfv.PerformLayout();
            this.gbModificacionfv.ResumeLayout(false);
            this.gbModificacionfv.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvfacturamodi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvmodibusqueda)).EndInit();
            this.gbseleccionfactura.ResumeLayout(false);
            this.gbseleccionfactura.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvfacturasmod)).EndInit();
            this.gbbuscar.ResumeLayout(false);
            this.gbbuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbusquedabusc)).EndInit();
            this.gbeliminar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgveliminar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.Button btnnuevafactura;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbmenu;
        private System.Windows.Forms.Button btneliminarfactura;
        private System.Windows.Forms.Button btnbuscarfactura;
        private System.Windows.Forms.Button btnmodificarfactura;
        private System.Windows.Forms.GroupBox gbnuevofactura;
        private System.Windows.Forms.TextBox txtdescuento;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txttipo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtcomentarios;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblsubtotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbltotal;
        private System.Windows.Forms.TextBox txtiva;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblfecha;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpfecha_entrega;
        private System.Windows.Forms.ComboBox cbsucursal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox gbcliente;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnseleccionarcliente;
        private System.Windows.Forms.DataGridView dgvseleccioncliente;
        private System.Windows.Forms.GroupBox gbfactura;
        private System.Windows.Forms.Button btnborrardet;
        private System.Windows.Forms.DataGridView dgvfactura;
        private System.Windows.Forms.TextBox txtcantidad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnbuscarproducto;
        private System.Windows.Forms.Button btnseleccionarproducto;
        private System.Windows.Forms.TextBox txtbuscar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvproductos;
        private System.Windows.Forms.Button btnimprimirfact;
        private System.Windows.Forms.GroupBox gbmodificarfv;
        private System.Windows.Forms.GroupBox gbModificacionfv;
        private System.Windows.Forms.Button btnimprimirmodi;
        private System.Windows.Forms.Button btnborrarmodi;
        private System.Windows.Forms.DataGridView dgvfacturamodi;
        private System.Windows.Forms.TextBox txtcantidadmodi;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnbuscarmodi;
        private System.Windows.Forms.Button btnagregarmodi;
        private System.Windows.Forms.TextBox txtbusquedamodi;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView dgvmodibusqueda;
        private System.Windows.Forms.ComboBox cbsucursalmodi;
        private System.Windows.Forms.Label lblfechcreomodi;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DateTimePicker dtpfechaentremodi;
        private System.Windows.Forms.TextBox txtcomentariomodi;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblsubtotalmodi;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbltotalmodi;
        private System.Windows.Forms.TextBox txtivamodi;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtdescuentomodi;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txttipomodi;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox gbseleccionfactura;
        private System.Windows.Forms.Button btnseleccionarfacturamodi;
        private System.Windows.Forms.DataGridView dgvfacturasmod;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox gbbuscar;
        private System.Windows.Forms.Button btnborrarbusc;
        private System.Windows.Forms.DataGridView dgvbusquedabusc;
        private System.Windows.Forms.TextBox txtbusquedabusc;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button btnbuscarbusc;
        private System.Windows.Forms.GroupBox gbeliminar;
        private System.Windows.Forms.Button btneliminar;
        private System.Windows.Forms.DataGridView dgveliminar;
    }
}