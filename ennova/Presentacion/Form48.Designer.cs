﻿namespace ennova.Presentacion
{
    partial class Form48
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crwver = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crwver
            // 
            this.crwver.ActiveViewIndex = -1;
            this.crwver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crwver.Cursor = System.Windows.Forms.Cursors.Default;
            this.crwver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crwver.Location = new System.Drawing.Point(0, 0);
            this.crwver.Name = "crwver";
            this.crwver.Size = new System.Drawing.Size(1060, 491);
            this.crwver.TabIndex = 0;
            // 
            // Form48
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 491);
            this.Controls.Add(this.crwver);
            this.Name = "Form48";
            this.Text = "Impresion";
            this.Load += new System.EventHandler(this.Form48_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crwver;
    }
}