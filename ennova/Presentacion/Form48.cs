﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Crystal_Report;
using ennova.Logica;
using CrystalDecisions.CrystalReports.Engine;

namespace ennova.Presentacion
{
    public partial class Form48 : Form
    {
        ennova.Crystal_Report.Base_crystal datosreporte;

        private Form48()
        {
            InitializeComponent();
        }

        public Form48(ennova.Crystal_Report.Base_crystal datos)
            : this()
        {
            datosreporte = datos;
        }

        private void Form48_Load(object sender, EventArgs e)
        {
            ReportDocument c = new ReportDocument();
            c.Load(@"C:\Users\Gabriel\Desktop\ennova\ennova\ennova\Crystal Report\Facturarpt.rpt");
            c.SetDataSource(datosreporte);
            crwver.ReportSource = c;
        }
    }

}

