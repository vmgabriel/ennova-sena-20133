﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form9 : Form
    {
        public Form9()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            Logica.Global.modif_client = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            Form10 v = new Form10();
            v.Show();
            this.Close();
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Cliente.pedir();
        }
    }
}
