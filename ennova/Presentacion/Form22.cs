﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form22 : Form
    {
        public Form22()
        {
            InitializeComponent();
        }

        private void btnvendedor_Click(object sender, EventArgs e)
        {
            Form23 a = new Form23();
            a.Show();
            this.Close();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form24 a = new Form24();
            a.Show();
            this.Close();
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            Form26 a = new Form26();
            a.Show();
            this.Close();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            Form27 a = new Form27();
            a.Show();
            this.Close();
        }
    }
}
