﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form33 : Form
    {
        public Form33()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form18 a = new Form18();
            a.Show();
            this.Close();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtciudad.Clear();
            txtempresa.Clear();
            txtiva.Clear();
            txtsubtotal.Clear();
            txttipo.Clear();
            txttotal.Clear();
            dtpfechacreacion.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Date.Day);
            cbpedido.Text = "";
            cbvendedor.Text = "";
            txtciudad.Focus();
        }

        private void Form33_Load(object sender, EventArgs e)
        {
            cbpedido.DataSource = Fac_compra.Lista_pedidos();
            cbpedido.DisplayMember = "nombre_pedido";
            cbpedido.ValueMember = "id_pedido";
            cbvendedor.DataSource = Fac_compra.Lista_vendedores();
            cbvendedor.DisplayMember = "Alias";
            cbvendedor.ValueMember = "Id";
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            string tipo, ciudad, vendedor, pedido, empresa;
            tipo = txttipo.Text;
            ciudad = txtciudad.Text;
            vendedor = cbvendedor.Text;
            pedido = cbpedido.Text;
            empresa = txtempresa.Text;
            DateTime fecha;
            Int64 iva, total, subtotal;
            fecha = dtpfechacreacion.Value;
            iva = Convert.ToInt64(txtiva.Text);
            total = Convert.ToInt64(txttotal.Text);
            subtotal = Convert.ToInt64(txtsubtotal.Text);
            if (tipo == "" || ciudad == "" || vendedor == "" || pedido == "" || empresa == "" || iva == 0 || total == 0 || subtotal == 0)
            {
                MessageBox.Show("No se ha llenado el campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Int64 fkvendedor, fkpedido;
                fkvendedor = Fac_compra.con_alias_id_vendedor(vendedor);
                fkpedido = Fac_compra.con_nombre_id_pedido(pedido);
                Fac_compra code = new Fac_compra();
                code.ciudad_fc = ciudad;
                code.fecha_creacion_fc = fecha;
                code.Fk_pedido_fc = fkpedido;
                code.Fk_vendedor_fc = fkvendedor;
                code.iva_fc = iva;
                code.organizacion_fc = empresa;
                code.sub_total_fc = subtotal;
                code.tipo_fc = tipo;
                code.total_fc = total;
                int res = Fac_compra.registrar(code);
                if (res != 0)
                {
                    MessageBox.Show("Datos Guardados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form18 a = new Form18();
                    a.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void sololetras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        private void solonumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
