﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form44 : Form
    {
        public Form44()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void Form44_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Producto.pedir();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            Global.modif_producto = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            Form45 v = new Form45();
            v.Show();
            this.Close();
        }
    }
}
