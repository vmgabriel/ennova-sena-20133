﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form28 : Form
    {
        public Form28()
        {
            InitializeComponent();
        }

        private void Form28_Load(object sender, EventArgs e)
        {
            cbproveedor.DataSource = Pedido.Lista_proveedor();
            cbproveedor.DisplayMember = "nombre_proveedor";
            cbproveedor.ValueMember = "id";
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form21 a = new Form21();
            a.Show();
            this.Close();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtcantidad.Clear();
            txtclase.Clear();
            txtnombre.Clear();
            txtvalor.Clear();
            dtpfechacreacion.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Date.Day);
            dtpfechaentrega.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Date.Day);
            cbprioridad.Text = "";
            cbproveedor.Text = "";
            txtnombre.Focus();
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            string nombre, clase, valor, cantidad, prioridad,seleccion;
            DateTime fechacrea, fechlimit;
            seleccion = cbproveedor.Text;
            Int64 fk;
            fk = Pedido.id_proveedor_nombre(seleccion);
            nombre = txtnombre.Text;
            clase = txtclase.Text;
            valor = txtvalor.Text;
            cantidad = txtcantidad.Text;
            prioridad = cbprioridad.Text;
            fechacrea = new DateTime(dtpfechacreacion.Value.Year, dtpfechacreacion.Value.Month, dtpfechacreacion.Value.Day);
            fechlimit = new DateTime(dtpfechaentrega.Value.Year, dtpfechaentrega.Value.Month, dtpfechaentrega.Value.Day);
            if (nombre == "" || clase == "" || valor == "" || cantidad == "" || prioridad == "")
            {
                MessageBox.Show("No se ha llenado el campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Pedido nuevo = new Pedido();
                nuevo.nombre_pedido = nombre;
                nuevo.cantidad_productos_pedido = Convert.ToInt64(cantidad);
                nuevo.clase_pedido = clase;
                nuevo.fecha_entrega_pedido = fechlimit;
                nuevo.fecha_expedida_pedido=fechacrea;
                nuevo.FK_proveedor_pedido = fk;
                nuevo.necesidad_producto_pedido = prioridad;
                nuevo.valor_pedido = Convert.ToInt64(valor);
                int res = Pedido.registrar(nuevo);
                if (res != 0)
                {
                    MessageBox.Show("Datos Guardados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form21 ad = new Form21();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void sololetras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        private void solonumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
