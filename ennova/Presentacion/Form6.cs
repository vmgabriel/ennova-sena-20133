﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ennova.Presentacion
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 d = new Form2();
            d.Show();
            this.Close();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Logica.Vendedor.pedir();
            dgvmostrar.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtbusqueda.Text == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string code = txtbusqueda.Text;
                dgvmostrar.DataSource = Logica.Vendedor.buscar(code);
                dgvmostrar.Refresh();
            }
        }
        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
