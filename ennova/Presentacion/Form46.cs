﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form46 : Form
    {
        public Form46()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 d = new Form2();
            d.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtbusqueda.Text == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string code = txtbusqueda.Text;
                dgvseleccion.DataSource = Producto.buscar(code);
                dgvseleccion.Refresh();
            }
        }

        private void Form46_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Producto.pedir();
        }
    }
}
