﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form36 : Form
    {
        public Form36()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form18 a = new Form18();
            a.Show();
            this.Close();
        }

        private void Form36_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Fac_compra.pedir();
        }

        private void txtbusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
                if (txtbusqueda.Text == "")
                {
                    dgvmostrar.DataSource = Fac_compra.pedir();
                    dgvmostrar.Refresh();
                }
                else
                {
                    string code = txtbusqueda.Text;
                    dgvmostrar.DataSource = Fac_compra.buscar(code);
                    dgvmostrar.Refresh();
                }
                string sas = txtbusqueda.Text;
                dgvmostrar.DataSource = Fac_compra.buscar(sas);
                dgvmostrar.Refresh();

            }
        }
    }
}
