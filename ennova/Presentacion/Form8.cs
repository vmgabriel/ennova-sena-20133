﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form8 : Form
    {
        public Form8()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtape1.Clear();
            txtape2.Clear();
            txtcc.Clear();
            txtcorreo.Clear();
            txtdire.Clear();
            txtnombre.Clear();
            txtnumero.Clear();
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            PE3.Clear();
            string nombre, apellido1, apellido2, correo, direccion,genero,cc,telefono;
            nombre = txtnombre.Text;
            apellido1 = txtape1.Text;
            apellido2 = txtape2.Text;
            correo = txtcorreo.Text;
            direccion = txtdire.Text;
            cc = txtcc.Text;
            telefono = txtnumero.Text;
            if (true == rbmasculino.Checked)
            {
                genero = "masculino";
            }
            else if (rbfemenino.Checked == true)
            {
                genero = "femenino";
            }
            else
            {
                genero = "";
            }
            if (nombre == "" || apellido1 == "" || apellido2 == "" || correo == "" || direccion == "" || cc == "" || telefono == "" || genero == "")
            {
                MessageBox.Show("No se han llenado algunos campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Cliente code = new Cliente();
                code.Nombre = nombre;
                code.Apellido1 = apellido1;
                code.Apellido2 = apellido2;
                code.CC = Convert.ToInt64(cc);
                code.Correo = correo;
                code.Direccion = direccion;
                code.Telefono = telefono;
                code.Genero = genero;
                int res = Cliente.registrar(code);
                if (res != 0)
                {
                    MessageBox.Show("Datos Guardados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form2 ad = new Form2();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void sololetras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void solonumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
