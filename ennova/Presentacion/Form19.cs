﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;
using ennova.Crystal_Report;
using CrystalDecisions.CrystalReports.Engine;

namespace ennova.Presentacion
{
    public partial class Form19 : Form
    {
        Int64 ultima,costo=0,id_CCli;
        public Form19()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void btnnuevafactura_Click(object sender, EventArgs e)
        {
            gbnuevofactura.Location = new Point(8, 139);
            gbnuevofactura.Visible = true;
            gbmodificarfv.Visible = false;
            gbbuscar.Visible = false;
            gbeliminar.Visible = false;
            gbcliente.Location = new Point(13,50);
            gbfactura.Visible = false;
            gbcliente.Visible = true;
            dgvseleccioncliente.DataSource = Cliente.pedir();
            cbsucursal.DataSource = Vendedor.Lista_sucursal();
            cbsucursal.DisplayMember = "nombre_sucursal";
            cbsucursal.ValueMember = "id_sucursal";
            lblfecha.Text = Convert.ToString(DateTime.Now.Day+" / "+DateTime.Now.Month+" / "+DateTime.Now.Year);
        }

        private void btnseleccionarcliente_Click(object sender, EventArgs e)
        {
            Global.costo_total = 0;
            if(txtcomentarios.Text==""||txtdescuento.Text==""||txtiva.Text==""||txttipo.Text=="")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string tipo, comentarios;
                DateTime fechaentrega, actual;
                Int64 iva, descuento, subtotal, total, id_suc, id_cliente;
                id_cliente = Convert.ToInt64(dgvseleccioncliente.CurrentRow.Cells[0].Value);
                tipo = txttipo.Text;
                comentarios = txtcomentarios.Text;
                fechaentrega = new DateTime(dtpfecha_entrega.Value.Year, dtpfecha_entrega.Value.Month, dtpfecha_entrega.Value.Day);
                actual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                iva = Convert.ToInt64(txtiva.Text);
                descuento = Convert.ToInt64(txtdescuento.Text);
                subtotal = 0;
                lblsubtotal.Text="0";
                total = (subtotal - descuento);
                id_suc = Vendedor.id_sucursal_nombre(cbsucursal.Text);
                Fac_venta nuevafac = new Fac_venta();
                nuevafac.comentarios_fv = comentarios;
                nuevafac.fecha_creacion_fv = actual;
                nuevafac.feha_entrega_fv = fechaentrega;
                nuevafac.Fk_cliente_fv = id_cliente;
                nuevafac.Fk_sucursal_fv = id_suc;
                nuevafac.Fk_vendedor_fv = Global.id_vendedor;
                nuevafac.iva_fv = iva;
                nuevafac.sub_total_fv = Convert.ToInt64(lblsubtotal.Text);
                nuevafac.tipo_fv = tipo;
                nuevafac.total_fv = Convert.ToInt64(lblsubtotal.Text)-descuento;
                int a;
                id_CCli = id_cliente;
                a = Fac_venta.registrar(nuevafac);
                if (a != 0)
                {
                    MessageBox.Show("Datos Guardados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    gbcliente.Visible = false;
                    dgvproductos.DataSource = Producto.pedir();
                    gbfactura.Visible = true;
                    ultima = Fac_venta.ultimo_registro();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnseleccionarproducto_Click(object sender, EventArgs e)
        {
            string busqueda = txtbuscar.Text;
            if (busqueda == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvproductos.DataSource = Producto.buscar_especifica(busqueda);
                dgvproductos.Refresh();
            }
        }

        private void btnseleccionarproducto_Click_1(object sender, EventArgs e)
        {
            Global.costo_total = 0;
            costo = 0;
            if (txtcantidad.Text=="")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Detalle_factura_venta nuevon = new Detalle_factura_venta();
                nuevon.cantidad_productos_detalle = Convert.ToInt64(txtcantidad.Text);
                nuevon.Fk_fv_detallefv = ultima;
                nuevon.Fk_prod_detallefv = Convert.ToInt64(dgvproductos.CurrentRow.Cells[0].Value);
                nuevon.valor_total_productos_detalle = nuevon.cantidad_productos_detalle*(Convert.ToInt64(dgvproductos.CurrentRow.Cells[6].Value));
                int total = Detalle_factura_venta.registrar(nuevon);
                if (total != 0)
                {
                    dgvfactura.DataSource = Detalle_factura_venta.pedir_factura(ultima);
                    dgvfactura.Refresh();
                    costo = Global.costo_total;
                    lblsubtotal.Text = Convert.ToString(costo);
                    lbltotal.Text = Convert.ToString(costo - Convert.ToInt64(txtdescuento.Text));
                    string tipo, comentarios;
                    DateTime fechaentrega, actual;
                    Int64 iva, descuento, subtotal, totala, id_suc, id_cliente;
                    id_cliente = id_CCli;
                    tipo = txttipo.Text;
                    comentarios = txtcomentarios.Text;
                    fechaentrega = new DateTime(dtpfecha_entrega.Value.Year, dtpfecha_entrega.Value.Month, dtpfecha_entrega.Value.Day);
                    actual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    iva = Convert.ToInt64(txtiva.Text);
                    descuento = Convert.ToInt64(txtdescuento.Text);
                    subtotal = Global.costo_total;
                    totala = (subtotal - descuento);
                    id_suc = Vendedor.id_sucursal_nombre(cbsucursal.Text);
                    Fac_venta nuevafac = new Fac_venta();
                    nuevafac.comentarios_fv = comentarios;
                    nuevafac.fecha_creacion_fv = actual;
                    nuevafac.feha_entrega_fv = fechaentrega;
                    nuevafac.Fk_cliente_fv = id_cliente;
                    nuevafac.Fk_sucursal_fv = id_suc;
                    nuevafac.Fk_vendedor_fv = Global.id_vendedor;
                    nuevafac.iva_fv = iva;
                    nuevafac.sub_total_fv = Convert.ToInt64(lblsubtotal.Text);
                    nuevafac.tipo_fv = tipo;
                    nuevafac.total_fv = Convert.ToInt64(lblsubtotal.Text) - descuento;
                    nuevafac.id_fv = ultima;
                    int a = Fac_venta.modificar(nuevafac);
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnborrardet_Click(object sender, EventArgs e)
        {
            Global.costo_total = 0;
            Int64 codigo = Convert.ToInt64(dgvfactura.CurrentRow.Cells[0].Value);
            int aq = Detalle_factura_venta.eliminar(codigo);
            dgvfactura.DataSource = Detalle_factura_venta.pedir_factura(ultima);
            dgvfactura.Refresh();
            lblsubtotal.Text = Convert.ToString(Global.costo_total);
            lbltotal.Text = Convert.ToString(Global.costo_total - Convert.ToInt64(txtdescuento.Text));
        }

        private void btnimprimirfact_Click(object sender, EventArgs e)
        {
            Form19 onj = new Form19();
            Base_crystal datos = GenerarFactura();
            Form48 en= new Form48(datos);
            en.ShowDialog();
            
        }

        private void btnmodificarfactura_Click(object sender, EventArgs e)
        {
            cbsucursalmodi.DataSource = Vendedor.Lista_sucursal();
            cbsucursalmodi.DisplayMember = "nombre_sucursal";
            cbsucursalmodi.ValueMember = "id_sucursal";
            gbnuevofactura.Location = new Point(8, 139);
            gbmodificarfv.Location = new Point(8, 139);
            gbnuevofactura.Visible = false;
            gbbuscar.Visible = false;
            gbeliminar.Visible = false;
            gbmodificarfv.Visible = true;
            gbseleccionfactura.Location = new Point(13, 50);
            gbseleccionfactura.Visible = true;
            gbModificacionfv.Visible = false;
            dgvfacturasmod.DataSource = Fac_venta.pedir();
            dgvfacturasmod.Refresh();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Global.costo_total = 0;
            Int64 id = Convert.ToInt64(dgvfacturasmod.CurrentRow.Cells[0].Value);
            ultima = id;
            Fac_venta nuevo = Fac_venta.llamar(id);
            dgvfacturamodi.DataSource = Detalle_factura_venta.pedir_factura(nuevo.id_fv);
            id_CCli = nuevo.Fk_cliente_fv;
            txtivamodi.Enabled = true;
            txtcomentariomodi.Enabled = true;
            txtdescuentomodi.Enabled = true;
            txttipomodi.Enabled = true;
            dtpfechaentremodi.Enabled = true;
            cbsucursalmodi.Enabled = true;
            txtivamodi.Text = Convert.ToString(nuevo.iva_fv);
            txtcomentariomodi.Text = nuevo.comentarios_fv;
            txtdescuentomodi.Text = "0";
            txttipomodi.Text = nuevo.tipo_fv;
            lblsubtotalmodi.Text = Convert.ToString(Global.costo_total);
            lbltotalmodi.Text = Convert.ToString(Global.costo_total-Convert.ToInt64(txtdescuentomodi.Text));
            dtpfechaentremodi.Value = nuevo.feha_entrega_fv;
            lblfechcreomodi.Text = Convert.ToString(nuevo.fecha_creacion_fv.Day + "/" + nuevo.fecha_creacion_fv.Month + "/" + nuevo.fecha_creacion_fv.Year);
            dgvmodibusqueda.DataSource = Producto.pedir();
            dgvfacturamodi.Refresh();
            gbModificacionfv.Location = new Point(13, 50);
            gbseleccionfactura.Visible = false;
            gbModificacionfv.Visible = true;
        }

        private void btnimprimirmodi_Click(object sender, EventArgs e)
        {
            Form19 onj = new Form19();
            ennova.Crystal_Report.Base_crystal datos = GenerarFactura();
            Form48 en = new Form48(datos);
            en.ShowDialog();
        }

        private void btnborrarmodi_Click(object sender, EventArgs e)
        {
            Global.costo_total = 0;
            Int64 codigo = Convert.ToInt64(dgvfacturamodi.CurrentRow.Cells[0].Value);
            int aq = Detalle_factura_venta.eliminar(codigo);
            dgvfacturamodi.DataSource = Detalle_factura_venta.pedir_factura(ultima);
            this.dgvfacturamodi.Refresh();
            lblsubtotalmodi.Text = Convert.ToString(Global.costo_total);
            lbltotalmodi.Text = Convert.ToString(Global.costo_total-Convert.ToInt64(txtdescuentomodi.Text));
        }

        private void btnagregarmodi_Click(object sender, EventArgs e)
        {
            Global.costo_total = 0;
            costo = 0;
            if (txtcantidadmodi.Text == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Detalle_factura_venta nuevon = new Detalle_factura_venta();
                nuevon.cantidad_productos_detalle = Convert.ToInt64(txtcantidadmodi.Text);
                nuevon.Fk_fv_detallefv = ultima;
                nuevon.Fk_prod_detallefv = Convert.ToInt64(dgvmodibusqueda.CurrentRow.Cells[0].Value);
                nuevon.valor_total_productos_detalle = nuevon.cantidad_productos_detalle * (Convert.ToInt64(dgvmodibusqueda.CurrentRow.Cells[6].Value));
                int total = Detalle_factura_venta.registrar(nuevon);
                if (total != 0)
                {
                    dgvfacturamodi.DataSource = Detalle_factura_venta.pedir_factura(ultima);
                    dgvfacturamodi.Refresh();
                    costo = Global.costo_total;
                    lblsubtotalmodi.Text = Convert.ToString(costo);
                    lbltotalmodi.Text = Convert.ToString(costo - Convert.ToInt64(txtdescuentomodi.Text));
                    string tipo, comentarios;
                    DateTime fechaentrega, actual;
                    Int64 iva, descuento, subtotal, totala, id_suc, id_cliente;
                    id_cliente = id_CCli;
                    tipo = txttipomodi.Text;
                    comentarios = txtcomentariomodi.Text;
                    fechaentrega = new DateTime(dtpfechaentremodi.Value.Year, dtpfechaentremodi.Value.Month, dtpfechaentremodi.Value.Day);
                    actual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    iva = Convert.ToInt64(txtivamodi.Text);
                    descuento = Convert.ToInt64(txtdescuentomodi.Text);
                    subtotal = Global.costo_total;
                    totala = (subtotal - descuento);
                    id_suc = Vendedor.id_sucursal_nombre(cbsucursalmodi.Text);
                    Fac_venta nuevafac = new Fac_venta();
                    nuevafac.comentarios_fv = comentarios;
                    nuevafac.fecha_creacion_fv = actual;
                    nuevafac.feha_entrega_fv = fechaentrega;
                    nuevafac.Fk_cliente_fv = id_cliente;
                    nuevafac.Fk_sucursal_fv = id_suc;
                    nuevafac.Fk_vendedor_fv = Global.id_vendedor;
                    nuevafac.iva_fv = iva;
                    nuevafac.sub_total_fv = Convert.ToInt64(lblsubtotalmodi.Text);
                    nuevafac.tipo_fv = tipo;
                    nuevafac.total_fv = Convert.ToInt64(lblsubtotalmodi.Text) - descuento;
                    nuevafac.id_fv = ultima;
                    int a = Fac_venta.modificar(nuevafac);
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string busqueda = txtbuscar.Text;
            if (busqueda == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvproductos.DataSource = Producto.buscar_especifica(busqueda);
                dgvproductos.Refresh();
            }
        }

        private void btnbuscarfactura_Click(object sender, EventArgs e)
        {
            gbnuevofactura.Location = new Point(8, 139);
            gbmodificarfv.Location = new Point(8, 139);
            gbbuscar.Location = new Point(8, 139);
            gbnuevofactura.Visible = false;
            gbmodificarfv.Visible = false;
            gbeliminar.Visible = false;
            gbbuscar.Visible = true;
            dgvbusquedabusc.DataSource = Fac_venta.pedir();
            this.dgvbusquedabusc.Refresh();
        }

        private void btnbuscarbusc_Click(object sender, EventArgs e)
        {
            string busqueda = txtbusquedabusc.Text;
            if (busqueda == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvbusquedabusc.DataSource = Fac_venta.buscar(busqueda);
                dgvbusquedabusc.Refresh();
            }
        }

        private void btnborrarbusc_Click(object sender, EventArgs e)
        {
            txtbusquedabusc.Clear();
            txtbusquedabusc.Focus();
        }

        private void btneliminarfactura_Click(object sender, EventArgs e)
        {
            gbnuevofactura.Location = new Point(8, 139);
            gbmodificarfv.Location = new Point(8, 139);
            gbbuscar.Location = new Point(8, 139);
            gbeliminar.Location = new Point(8, 139);
            gbnuevofactura.Visible = false;
            gbmodificarfv.Visible = false;
            gbbuscar.Visible = false;
            gbeliminar.Visible = true;
            dgveliminar.DataSource = Fac_venta.pedir();
            this.dgveliminar.Refresh();
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            int aq;
            Int64 id = Convert.ToInt64(dgveliminar.CurrentRow.Cells[0].Value);
            DialogResult a = MessageBox.Show("Desea eliminar el dato????...recuerde que sera removido", "Avizo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (a == DialogResult.Yes)
            {
                int xd=Detalle_factura_venta.destruir_fveliminar(id);
                if (xd !=0)
                {
                    MessageBox.Show("Hecho 1 de 2", "Avizo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    aq = Fac_venta.eliminar(id);
                    if (aq >= 1)
                    {
                        MessageBox.Show("Factura de Venta eliminada", "Avizo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgveliminar.DataSource = Fac_venta.pedir();
                        this.dgveliminar.Refresh();
                    }
                }
                else
                {
                    MessageBox.Show("¡¡¡NO se han podido eliminar!!!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private ennova.Crystal_Report.Base_crystal GenerarFactura()
        {
            ennova.Crystal_Report.Base_crystal facturacion = new ennova.Crystal_Report.Base_crystal();

            //
            // Agrego el registro con la info del cliente
            //
            ennova.Crystal_Report.Base_crystal.Detalles_FVRow rowDatosfacturadetalles = facturacion.Detalles_FV.NewDetalles_FVRow();
            rowDatosfacturadetalles.Comentarios = txtcomentariomodi.Text;
            rowDatosfacturadetalles.Fecha_Creacion = DateTime.Now;
            rowDatosfacturadetalles.Fecha_entrega = Convert.ToDateTime(dtpfechaentremodi.Text);
            rowDatosfacturadetalles.Id = ultima;
            rowDatosfacturadetalles.Subtotal = Convert.ToInt64(lblsubtotalmodi.Text);
            rowDatosfacturadetalles.Tipo = txttipomodi.Text;
            rowDatosfacturadetalles.Total = Convert.ToInt64(lbltotalmodi.Text);

            facturacion.Detalles_FV.AddDetalles_FVRow(rowDatosfacturadetalles);
    
            //
            // Itero por cada fila del DataGridView creando el registro
            // en el DataTabla
            //
            foreach (DataGridViewRow row in dgvfacturamodi.Rows)
            {
                ennova.Crystal_Report.Base_crystal.Factura_VentaRow rowCompra = facturacion.Factura_Venta.NewFactura_VentaRow();
                rowCompra.Id_registro = Convert.ToInt64(row.Cells["id_Cod"].Value);
                rowCompra.Id_producto = Convert.ToInt64(row.Cells["id_producto"].Value);
                rowCompra.Nombre_producto = Convert.ToString(row.Cells["Nombre_producto"].Value);
                rowCompra.Cantidad = Convert.ToInt64(row.Cells["cantidad_productos"].Value);
                rowCompra.Costo_unitario = Convert.ToInt64(row.Cells["costo_producto"].Value);
                rowCompra.Costo_total = Convert.ToInt64(row.Cells["valor_total_producto"].Value);
                facturacion.Factura_Venta.AddFactura_VentaRow(rowCompra);
            }

            return facturacion;
        }
    }
}
