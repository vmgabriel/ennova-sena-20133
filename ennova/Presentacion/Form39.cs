﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form39 : Form
    {
        public Form39()
        {
            InitializeComponent();
        }

        private void Form39_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Pago.pedir();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form20 a = new Form20();
            a.Show();
            this.Close();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            Global.modif_pago = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            Form40 v = new Form40();
            v.Show();
            this.Close();
        }
    }
}
