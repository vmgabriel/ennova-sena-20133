﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form29 : Form
    {
        public Form29()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form21 a = new Form21();
            a.Show();
            this.Close();
        }

        private void Form29_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Pedido.pedir();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            Global.modif_pedido = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            Form30 v = new Form30();
            v.Show();
            this.Close();
        }
    }
}
