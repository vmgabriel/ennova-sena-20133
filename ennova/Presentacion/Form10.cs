﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form10 : Form
    {
        public Form10()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtape1.Clear();
            txtape2.Clear();
            txtcc.Clear();
            txtcorreo.Clear();
            txtdire.Clear();
            txtnombre.Clear();
            txtnumero.Clear();
        }

        private void Form10_Load(object sender, EventArgs e)
        {
            Cliente vendi = Cliente.llamar(Global.modif_client);
            txtnombre.Text = vendi.Nombre;
            txtape1.Text = vendi.Apellido1;
            txtape2.Text = vendi.Apellido2;
            txtcc.Text = Convert.ToString(vendi.CC);
            txtcorreo.Text = vendi.Correo;
            txtdire.Text = vendi.Direccion;
            txtnumero.Text = Convert.ToString(vendi.Telefono);
            if (vendi.Genero == "masculino")
            {
                rbmasculino.Checked = true;
                rbfemenino.Checked = false;
            }
            else
            {
                rbfemenino.Checked = true;
                rbmasculino.Checked = false;
            }
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            PE3.Clear();
            string nombre, apellido1, apellido2, direccion, correo, genero = "",numero;
            Int64 cc;
            nombre = txtnombre.Text;
            apellido1 = txtape1.Text;
            apellido2 = txtape2.Text;
            direccion = txtdire.Text;
            correo = txtcorreo.Text;
            cc = Convert.ToInt64(txtcc.Text);
            numero = txtnumero.Text;
            genero = "";
            if (rbmasculino.Checked == true)
            {
                genero = "masculino";
            }
            else
            {
                genero = "fememino";
            }
            if (nombre == "" || apellido1 == "" || apellido2 == "" || direccion == "" || correo == "" || cc == 0 || numero == "" || genero == "")
            {
                MessageBox.Show("No se ha llenado el campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Cliente a = new Cliente();
                a.Nombre = nombre;
                a.Apellido1 = apellido1;
                a.Apellido2 = apellido2;
                a.CC = cc;
                a.Correo = correo;
                a.Direccion = direccion;
                a.Genero = genero;
                a.Telefono = numero;
                a.id = Global.modif_client;
                int res = Cliente.modificar(a);
                if (res != 0)
                {
                    MessageBox.Show("Datos actualizados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form2 ad = new Form2();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar modificar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
