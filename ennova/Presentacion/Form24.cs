﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form24 : Form
    {
        public Form24()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form22 a = new Form22();
            a.Show();
            this.Close();
        }

        private void Form24_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Sucursal.pedir();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            Global.modif_sucursal = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            Form25 v = new Form25();
            v.Show();
            this.Close();
        }
    }
}
