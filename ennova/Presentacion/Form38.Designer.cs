﻿namespace ennova.Presentacion
{
    partial class Form38
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form38));
            this.gbregistro = new System.Windows.Forms.GroupBox();
            this.cbsucursal = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtiva = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txttipo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtsubtotal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbcliente = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbvendedor = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnborrar = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.btnregistrar = new System.Windows.Forms.Button();
            this.gbregistro.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbregistro
            // 
            this.gbregistro.Controls.Add(this.cbvendedor);
            this.gbregistro.Controls.Add(this.label5);
            this.gbregistro.Controls.Add(this.cbcliente);
            this.gbregistro.Controls.Add(this.label4);
            this.gbregistro.Controls.Add(this.txtsubtotal);
            this.gbregistro.Controls.Add(this.label1);
            this.gbregistro.Controls.Add(this.cbsucursal);
            this.gbregistro.Controls.Add(this.label8);
            this.gbregistro.Controls.Add(this.txttotal);
            this.gbregistro.Controls.Add(this.label10);
            this.gbregistro.Controls.Add(this.txtiva);
            this.gbregistro.Controls.Add(this.label2);
            this.gbregistro.Controls.Add(this.txttipo);
            this.gbregistro.Controls.Add(this.label3);
            this.gbregistro.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gbregistro.Location = new System.Drawing.Point(12, 57);
            this.gbregistro.Name = "gbregistro";
            this.gbregistro.Size = new System.Drawing.Size(577, 483);
            this.gbregistro.TabIndex = 7;
            this.gbregistro.TabStop = false;
            // 
            // cbsucursal
            // 
            this.cbsucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbsucursal.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cbsucursal.FormattingEnabled = true;
            this.cbsucursal.Location = new System.Drawing.Point(27, 361);
            this.cbsucursal.MaxDropDownItems = 90;
            this.cbsucursal.Name = "cbsucursal";
            this.cbsucursal.Size = new System.Drawing.Size(198, 25);
            this.cbsucursal.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(25, 339);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(192, 19);
            this.label8.TabIndex = 34;
            this.label8.Text = "Seleccione la Sucursal:\r\n";
            // 
            // txttotal
            // 
            this.txttotal.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txttotal.Location = new System.Drawing.Point(29, 282);
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(290, 23);
            this.txttotal.TabIndex = 27;
            this.txttotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label10.Location = new System.Drawing.Point(24, 260);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 19);
            this.label10.TabIndex = 28;
            this.label10.Text = "Ingrese el total:";
            // 
            // txtiva
            // 
            this.txtiva.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtiva.Location = new System.Drawing.Point(28, 134);
            this.txtiva.Name = "txtiva";
            this.txtiva.Size = new System.Drawing.Size(290, 23);
            this.txtiva.TabIndex = 10;
            this.txtiva.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label2.Location = new System.Drawing.Point(24, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ingrese el IVA:";
            // 
            // txttipo
            // 
            this.txttipo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txttipo.Location = new System.Drawing.Point(28, 67);
            this.txttipo.Name = "txttipo";
            this.txttipo.Size = new System.Drawing.Size(419, 23);
            this.txttipo.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label3.Location = new System.Drawing.Point(24, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ingrese el Tipo";
            // 
            // txtsubtotal
            // 
            this.txtsubtotal.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtsubtotal.Location = new System.Drawing.Point(27, 208);
            this.txtsubtotal.Name = "txtsubtotal";
            this.txtsubtotal.Size = new System.Drawing.Size(289, 23);
            this.txtsubtotal.TabIndex = 36;
            this.txtsubtotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label1.Location = new System.Drawing.Point(26, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 19);
            this.label1.TabIndex = 37;
            this.label1.Text = "Ingrese el sub-total:";
            // 
            // cbcliente
            // 
            this.cbcliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbcliente.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cbcliente.FormattingEnabled = true;
            this.cbcliente.Location = new System.Drawing.Point(351, 361);
            this.cbcliente.MaxDropDownItems = 90;
            this.cbcliente.Name = "cbcliente";
            this.cbcliente.Size = new System.Drawing.Size(198, 25);
            this.cbcliente.TabIndex = 39;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(349, 339);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(177, 19);
            this.label4.TabIndex = 38;
            this.label4.Text = "Seleccione el Cliente:\r\n";
            // 
            // cbvendedor
            // 
            this.cbvendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbvendedor.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cbvendedor.FormattingEnabled = true;
            this.cbvendedor.Location = new System.Drawing.Point(185, 436);
            this.cbvendedor.MaxDropDownItems = 90;
            this.cbvendedor.Name = "cbvendedor";
            this.cbvendedor.Size = new System.Drawing.Size(198, 25);
            this.cbvendedor.TabIndex = 41;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(181, 414);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(198, 19);
            this.label5.TabIndex = 40;
            this.label5.Text = "Seleccione el Vendedor:\r\n";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Showcard Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(163, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(251, 33);
            this.label6.TabIndex = 33;
            this.label6.Text = "Registrar Pago";
            // 
            // btnborrar
            // 
            this.btnborrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnborrar.BackgroundImage")));
            this.btnborrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnborrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnborrar.FlatAppearance.BorderSize = 2;
            this.btnborrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnborrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnborrar.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnborrar.Location = new System.Drawing.Point(212, 556);
            this.btnborrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnborrar.Name = "btnborrar";
            this.btnborrar.Size = new System.Drawing.Size(158, 67);
            this.btnborrar.TabIndex = 36;
            this.btnborrar.Text = "Borrar";
            this.btnborrar.UseVisualStyleBackColor = true;
            this.btnborrar.Click += new System.EventHandler(this.btnborrar_Click);
            // 
            // btnsalir
            // 
            this.btnsalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalir.BackgroundImage")));
            this.btnsalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnsalir.FlatAppearance.BorderSize = 2;
            this.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnsalir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsalir.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalir.Location = new System.Drawing.Point(427, 556);
            this.btnsalir.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(158, 67);
            this.btnsalir.TabIndex = 35;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // btnregistrar
            // 
            this.btnregistrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnregistrar.BackgroundImage")));
            this.btnregistrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnregistrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnregistrar.FlatAppearance.BorderSize = 2;
            this.btnregistrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnregistrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnregistrar.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnregistrar.Location = new System.Drawing.Point(8, 556);
            this.btnregistrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnregistrar.Name = "btnregistrar";
            this.btnregistrar.Size = new System.Drawing.Size(158, 67);
            this.btnregistrar.TabIndex = 34;
            this.btnregistrar.Text = "Registrar";
            this.btnregistrar.UseVisualStyleBackColor = true;
            this.btnregistrar.Click += new System.EventHandler(this.btnregistrar_Click);
            // 
            // Form38
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 633);
            this.ControlBox = false;
            this.Controls.Add(this.btnborrar);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.btnregistrar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.gbregistro);
            this.Name = "Form38";
            this.Text = "Nuevo";
            this.Load += new System.EventHandler(this.Form38_Load);
            this.gbregistro.ResumeLayout(false);
            this.gbregistro.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbregistro;
        private System.Windows.Forms.ComboBox cbvendedor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbcliente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtsubtotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbsucursal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtiva;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txttipo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnborrar;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.Button btnregistrar;
    }
}