﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtape1.Clear();
            txtape2.Clear();
            txtcc.Clear();
            txtcorreo.Clear();
            txtdire.Clear();
            txtnombre.Clear();
            txtnumero.Clear();
            txtcontraseña.Clear();
            txtalias.Clear();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void sololetras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        private void solonumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            PE1.Clear();
            string nombre, apellido1, apellido2, direccion, correo, alias, genero = "",sucursal;
            Int64 cc, numero;
            nombre = txtnombre.Text;
            apellido1 = txtape1.Text;
            apellido2 = txtape2.Text;
            direccion = txtdire.Text;
            correo = txtcorreo.Text;
            sucursal = cbsucursal.Text;
            alias = txtalias.Text;
            cc = Convert.ToInt64(txtcc.Text);
            numero = Convert.ToInt64(txtnumero.Text);
            String contra = txtcontraseña.Text;
            genero = "";
            if (rbmasculino.Checked == true)
            {
                genero = "masculino";
            }
            else
            {
                genero = "fememino";
            }
            if (nombre == "" || apellido1 == "" || apellido2 == "" || direccion == "" || correo == "" || alias == "" || cc == 0 || numero == 0 || genero == "" || contra == "" || sucursal=="")
            {
                MessageBox.Show("No se ha llenado el campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Vendedor a = new Vendedor();
                a.Nombre = nombre;
                a.Alias = alias;
                a.Apellido1 = apellido1;
                a.Apellido2 = apellido2;
                a.CC = cc;
                a.Contraseña = contra;
                a.Correo = correo;
                a.Direccion = direccion;
                a.Genero = genero;
                a.Telefono = numero;
                a.FK_sucursal = Vendedor.id_sucursal_nombre(sucursal);
                int res = Vendedor.registrar(a);
                if (res != 0)
                {
                    MessageBox.Show("Datos Guardados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form2 ad = new Form2();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar guardar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            cbsucursal.DataSource = Vendedor.Lista_sucursal();
            cbsucursal.DisplayMember = "nombre_sucursal";
            cbsucursal.ValueMember = "id_sucursal";
        }
    }
}
