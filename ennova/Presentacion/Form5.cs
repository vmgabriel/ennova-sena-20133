﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            Vendedor vendi = Vendedor.llamar(Global.modif_client);
            txtnombre.Text = vendi.Nombre;
            txtalias.Text = vendi.Alias;
            txtape1.Text = vendi.Apellido1;
            txtape2.Text = vendi.Apellido2;
            txtcc.Text = Convert.ToString(vendi.CC);
            txtcontraseña.Text = vendi.Contraseña;
            txtcorreo.Text = vendi.Correo;
            txtdire.Text = vendi.Direccion;
            txtnumero.Text = Convert.ToString(vendi.Telefono);
            if (vendi.Genero == "masculino")
            {
                rbmasculino.Checked = true;
                rbfemenino.Checked = false;
            }
            else
            {
                rbfemenino.Checked = true;
                rbmasculino.Checked = false;
            }
            cbsucursal.DataSource = Vendedor.Lista_sucursal();
            cbsucursal.DisplayMember = "nombre_sucursal";
            cbsucursal.ValueMember = "id_sucursal";
            cbsucursal.Text = Vendedor.nombre_sucursal_id(vendi.FK_sucursal);
        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
            PE2.Clear();
            string nombre, apellido1, apellido2, direccion, correo, alias, genero = "", sucursal;
            Int64 cc, numero;
            nombre = txtnombre.Text;
            apellido1 = txtape1.Text;
            apellido2 = txtape2.Text;
            direccion = txtdire.Text;
            correo = txtcorreo.Text;
            alias = txtalias.Text;
            cc = Convert.ToInt64(txtcc.Text);
            numero = Convert.ToInt64(txtnumero.Text);
            String contra = txtcontraseña.Text;
            genero = "";
            if (rbmasculino.Checked == true)
            {
                genero = "masculino";
            }
            else
            {
                genero = "fememino";
            }
            sucursal = cbsucursal.Text;
            if (nombre == "" || apellido1 == "" || apellido2 == "" || direccion == "" || correo == "" || alias == "" || cc == 0 || numero == 0 || genero == "" || contra == "")
            {
                MessageBox.Show("No se ha llenado el campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Vendedor a = new Vendedor();
                a.Nombre = nombre;
                a.Alias = alias;
                a.Apellido1 = apellido1;
                a.Apellido2 = apellido2;
                a.CC = cc;
                a.Contraseña = contra;
                a.Correo = correo;
                a.Direccion = direccion;
                a.Genero = genero;
                a.Telefono = numero;
                a.id = Global.modif_client;
                a.FK_sucursal = Vendedor.id_sucursal_nombre(sucursal);
                int res = Vendedor.modificar(a);
                if (res != 0)
                {
                    MessageBox.Show("Datos actualizados Correctamente", "Datos Guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form2 ad = new Form2();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar modificar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtape1.Clear();
            txtape2.Clear();
            txtcc.Clear();
            txtcorreo.Clear();
            txtdire.Clear();
            txtnombre.Clear();
            txtnumero.Clear();
            txtcontraseña.Clear();
            txtalias.Clear();
        }
    }
}
