﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ennova.Presentacion
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btncliente_MouseEnter(object sender, EventArgs e)
        {
            pnfactura.Visible = false;
            pncliente.Visible = false;
            pnproducto.Visible = false;
            pnproveedor.Visible = false;
            pnvendedor.Location = new Point(20, 139);
            pnvendedor.Visible = true;
        }

        private void btnvendedor_MouseEnter(object sender, EventArgs e)
        {
            pnfactura.Visible = false;
            pnproducto.Visible = false;
            pnproveedor.Visible = false;
            pncliente.Location = new Point(20, 139);
            pnvendedor.Visible = false;
            pncliente.Visible = true;
        }

        private void btnncliente_Click(object sender, EventArgs e)
        {
            Form3 a = new Form3();
            a.Show();
            this.Close();
        }

        private void btnmsalir_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.Show();
            this.Close();
        }

        private void btnmodcliente_Click(object sender, EventArgs e)
        {
            Form4 a = new Form4();
            a.Show();
            this.Close();
        }

        private void btnbusccliente_Click(object sender, EventArgs e)
        {
            Form6 a = new Form6();
            a.Show();
            this.Close();
        }

        private void btnelicliente_Click(object sender, EventArgs e)
        {
            Form7 a = new Form7();
            a.Show();
            this.Close();
        }

        private void btnnvendedor_Click(object sender, EventArgs e)
        {
            Form8 a = new Form8();
            a.Show();
            this.Close();
        }

        private void btnmodvendedor_Click(object sender, EventArgs e)
        {
            Form9 a = new Form9();
            a.Show();
            this.Close();
        }

        private void btnbusvendedor_Click(object sender, EventArgs e)
        {
            Form11 a = new Form11();
            a.Show();
            this.Close();
        }

        private void btnelimvendedor_Click(object sender, EventArgs e)
        {
            Form12 a = new Form12();
            a.Show();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form13 a = new Form13();
            a.Show();
            this.Close();
        }

        private void btnproveedor_MouseEnter(object sender, EventArgs e)
        {
            pnfactura.Visible = false;
            pnproducto.Visible = false;
            pnproveedor.Location = new Point(20, 139);
            pncliente.Visible = false;
            pnvendedor.Visible = false;
            pnproveedor.Visible = true;
        }

        private void btnfactura_MouseEnter(object sender, EventArgs e)
        {
            pncliente.Visible = false;
            pnproducto.Visible = false;
            pnproveedor.Visible = false;
            pnvendedor.Visible = false;
            pnfactura.Location = new Point(20, 139);
            pnfactura.Visible = true;
        }

        private void btnproducto_MouseEnter(object sender, EventArgs e)
        {
            pnfactura.Visible = false;
            pnproducto.Location = new Point(20, 139);
            pncliente.Visible = false;
            pnvendedor.Visible = false;
            pnproveedor.Visible = false;
            pnproducto.Visible = true;

        }

        private void btnmodprovee_Click(object sender, EventArgs e)
        {
            Form14 a = new Form14();
            a.Show();
            this.Close();
        }

        private void btnbusprov_Click(object sender, EventArgs e)
        {
            Form16 a = new Form16();
            a.Show();
            this.Close();
        }

        private void btneliprov_Click(object sender, EventArgs e)
        {
            Form17 a = new Form17();
            a.Show();
            this.Close();
        }

        private void btnfaccompra_Click(object sender, EventArgs e)
        {
            Form18 a = new Form18();
            a.Show();
            this.Close();
        }

        private void btnfacventa_Click(object sender, EventArgs e)
        {
            Form19 a = new Form19();
            a.Show();
            this.Close();
        }

        private void btnfacpago_Click(object sender, EventArgs e)
        {
            Form20 a = new Form20();
            a.Show();
            this.Close();
        }

        private void btnfacpedido_Click(object sender, EventArgs e)
        {
            Form21 a = new Form21();
            a.Show();
            this.Close();
        }

        private void btnfacsucursal_Click(object sender, EventArgs e)
        {
            Form22 a = new Form22();
            a.Show();
            this.Close();
        }

        private void btnnprod_Click(object sender, EventArgs e)
        {
            Form43 a = new Form43();
            a.Show();
            this.Close();
        }

        private void btnmodprod_Click(object sender, EventArgs e)
        {
            Form44 a = new Form44();
            a.Show();
            this.Close();
        }

        private void btnbusprod_Click(object sender, EventArgs e)
        {
            Form46 a = new Form46();
            a.Show();
            this.Close();
        }

        private void btneliprod_Click(object sender, EventArgs e)
        {
            Form47 a = new Form47();
            a.Show();
            this.Close();
        }
    }
}
