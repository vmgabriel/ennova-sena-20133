﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form17 : Form
    {
        public Form17()
        {
            InitializeComponent();
        }

        private void Form17_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Proveedor.pedir();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            int aq;
            Int64 id = Convert.ToInt64(dgvmostrar.CurrentRow.Cells[0].Value);
            DialogResult a = MessageBox.Show("Desea eliminar el dato????...recuerde que sera removido", "Avizo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (a == DialogResult.Yes)
            {
                aq = Proveedor.eliminar(id);
                if (aq >= 1)
                {
                    MessageBox.Show("Proveedor eliminado Satisfactoriamente", "Avizo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form2 entry = new Form2();
                    entry.Show();
                    this.Close();
                }
            }
        }
    }
}
