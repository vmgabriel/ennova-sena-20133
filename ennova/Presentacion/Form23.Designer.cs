﻿namespace ennova.Presentacion
{
    partial class Form23
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form23));
            this.gbregistro = new System.Windows.Forms.GroupBox();
            this.txtnit = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcorreo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtdire = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtciudad = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtpais = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txttel = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnregistrar = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.btnborrar = new System.Windows.Forms.Button();
            this.lbltitulo = new System.Windows.Forms.Label();
            this.gbregistro.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbregistro
            // 
            this.gbregistro.Controls.Add(this.txtnit);
            this.gbregistro.Controls.Add(this.label4);
            this.gbregistro.Controls.Add(this.txtcorreo);
            this.gbregistro.Controls.Add(this.label1);
            this.gbregistro.Controls.Add(this.txtdire);
            this.gbregistro.Controls.Add(this.label9);
            this.gbregistro.Controls.Add(this.txtciudad);
            this.gbregistro.Controls.Add(this.label7);
            this.gbregistro.Controls.Add(this.txtpais);
            this.gbregistro.Controls.Add(this.label5);
            this.gbregistro.Controls.Add(this.txttel);
            this.gbregistro.Controls.Add(this.label2);
            this.gbregistro.Controls.Add(this.txtnombre);
            this.gbregistro.Controls.Add(this.label3);
            this.gbregistro.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            resources.ApplyResources(this.gbregistro, "gbregistro");
            this.gbregistro.Name = "gbregistro";
            this.gbregistro.TabStop = false;
            // 
            // txtnit
            // 
            resources.ApplyResources(this.txtnit, "txtnit");
            this.txtnit.Name = "txtnit";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // txtcorreo
            // 
            resources.ApplyResources(this.txtcorreo, "txtcorreo");
            this.txtcorreo.Name = "txtcorreo";
            this.txtcorreo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sinespacios_KeyPress);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // txtdire
            // 
            resources.ApplyResources(this.txtdire, "txtdire");
            this.txtdire.Name = "txtdire";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // txtciudad
            // 
            resources.ApplyResources(this.txtciudad, "txtciudad");
            this.txtciudad.Name = "txtciudad";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // txtpais
            // 
            resources.ApplyResources(this.txtpais, "txtpais");
            this.txtpais.Name = "txtpais";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // txttel
            // 
            resources.ApplyResources(this.txttel, "txttel");
            this.txttel.Name = "txttel";
            this.txttel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // txtnombre
            // 
            resources.ApplyResources(this.txtnombre, "txtnombre");
            this.txtnombre.Name = "txtnombre";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // btnregistrar
            // 
            resources.ApplyResources(this.btnregistrar, "btnregistrar");
            this.btnregistrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnregistrar.FlatAppearance.BorderSize = 2;
            this.btnregistrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnregistrar.Name = "btnregistrar";
            this.btnregistrar.UseVisualStyleBackColor = true;
            this.btnregistrar.Click += new System.EventHandler(this.btnregistrar_Click);
            // 
            // btnsalir
            // 
            resources.ApplyResources(this.btnsalir, "btnsalir");
            this.btnsalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnsalir.FlatAppearance.BorderSize = 2;
            this.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // btnborrar
            // 
            resources.ApplyResources(this.btnborrar, "btnborrar");
            this.btnborrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnborrar.FlatAppearance.BorderSize = 2;
            this.btnborrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnborrar.Name = "btnborrar";
            this.btnborrar.UseVisualStyleBackColor = true;
            this.btnborrar.Click += new System.EventHandler(this.btnborrar_Click);
            // 
            // lbltitulo
            // 
            resources.ApplyResources(this.lbltitulo, "lbltitulo");
            this.lbltitulo.Name = "lbltitulo";
            // 
            // Form23
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add(this.lbltitulo);
            this.Controls.Add(this.btnregistrar);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.btnborrar);
            this.Controls.Add(this.gbregistro);
            this.Name = "Form23";
            this.gbregistro.ResumeLayout(false);
            this.gbregistro.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbregistro;
        private System.Windows.Forms.TextBox txtnit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcorreo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdire;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtciudad;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtpais;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txttel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnregistrar;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.Button btnborrar;
        private System.Windows.Forms.Label lbltitulo;
    }
}