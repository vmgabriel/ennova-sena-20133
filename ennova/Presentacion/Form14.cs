﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form14 : Form
    {
        public Form14()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void Form14_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Proveedor.pedir();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            Global.modif_proveedor = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            Form15 v = new Form15();
            v.Show();
            this.Close();
        }
    }
}
