﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form32 : Form
    {
        public Form32()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form21 a = new Form21();
            a.Show();
            this.Close();
        }

        private void Form32_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Pedido.pedir();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            int aq;
            Int64 id = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            DialogResult a = MessageBox.Show("Desea eliminar el dato????...recuerde que sera removido", "Avizo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (a == DialogResult.Yes)
            {
                aq = Pedido.eliminar(id);
                if (aq >= 1)
                {
                    MessageBox.Show("Pedido Eliminado", "Avizo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form21 entry = new Form21();
                    entry.Show();
                    this.Close();
                }
            }
        }
    }
}
