﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ennova.Presentacion
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Logica.Vendedor.pedir();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            Logica.Global.modif_client = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            Form5 v = new Form5();
            v.Show();
            this.Close();
        }
    }
}
