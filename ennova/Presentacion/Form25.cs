﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form25 : Form
    {
        public Form25()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form22 a = new Form22();
            a.Show();
            this.Close();
        }

        private void Form25_Load(object sender, EventArgs e)
        {
            Sucursal code = Sucursal.llamar(Global.modif_sucursal);
            txtciudad.Text = code.ciudad_sucursal;
            txtcorreo.Text = code.correo_sucursal;
            txtdire.Text = code.direccion_sucursal;
            txtnit.Text = code.nit_estandar_sucursal;
            txtnombre.Text = code.nombre_sucursal;
            txtpais.Text = code.pais_sucursal;
            txttel.Text = code.telefono_sucursal;
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            txtciudad.Clear();
            txtcorreo.Clear();
            txtdire.Clear();
            txtnit.Clear();
            txtnombre.Clear();
            txtpais.Clear();
            txttel.Clear();
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            string nombre, ciudad, correo, dire, nit,pais, tel;
            nombre = txtnombre.Text;
            ciudad = txtciudad.Text;
            correo = txtcorreo.Text;
            dire = txtdire.Text;
            nit = txtnit.Text;
            pais = txtpais.Text;
            tel = txttel.Text;
            if (nombre == "" || ciudad =="" || correo=="" || dire ==""|| nit =="" || pais==""||tel=="")
            {
                MessageBox.Show("No se han llenado algunos campo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Sucursal code = new Sucursal();
                code.id_sucursal = Global.modif_sucursal;
                code.ciudad_sucursal = txtciudad.Text;
                code.correo_sucursal = txtcorreo.Text;
                code.direccion_sucursal = txtdire.Text;
                code.nit_estandar_sucursal = txtnit.Text;
                code.nombre_sucursal = txtnombre.Text;
                code.pais_sucursal = txtpais.Text;
                code.telefono_sucursal = txttel.Text;
                int res = Sucursal.modificar(code);
                if (res != 0)
                {
                    MessageBox.Show("Datos Modificados Correctamente", "Datos Actualizados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form22 ad = new Form22();
                    ad.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error al intentar Modificar a base de datos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void sololetras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        private void solonumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
