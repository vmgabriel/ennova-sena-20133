﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form37 : Form
    {
        public Form37()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form18 a = new Form18();
            a.Show();
            this.Close();
        }

        private void Form37_Load(object sender, EventArgs e)
        {
            dgvseleccion.DataSource = Fac_compra.pedir();
        }

        private void btnseleccionar_Click(object sender, EventArgs e)
        {
            int aq;
            Int64 id = Convert.ToInt64(dgvseleccion.CurrentRow.Cells[0].Value);
            DialogResult a = MessageBox.Show("Desea eliminar el dato????...recuerde que sera removido", "Avizo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (a == DialogResult.Yes)
            {
                aq = Fac_compra.eliminar(id);
                if (aq >= 1)
                {
                    MessageBox.Show("Factura de Compra Eliminada", "Avizo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form18 entry = new Form18();
                    entry.Show();
                    this.Close();
                }
            }
        }
    }
}
