﻿namespace ennova.Presentacion
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.pnmenu = new System.Windows.Forms.Panel();
            this.pnfactura = new System.Windows.Forms.Panel();
            this.btnfacpedido = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnfacsucursal = new System.Windows.Forms.Button();
            this.btnfacpago = new System.Windows.Forms.Button();
            this.btnfacventa = new System.Windows.Forms.Button();
            this.btnfaccompra = new System.Windows.Forms.Button();
            this.pnproducto = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.btneliprod = new System.Windows.Forms.Button();
            this.btnbusprod = new System.Windows.Forms.Button();
            this.btnmodprod = new System.Windows.Forms.Button();
            this.btnnprod = new System.Windows.Forms.Button();
            this.pnproveedor = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.btneliprov = new System.Windows.Forms.Button();
            this.btnbusprov = new System.Windows.Forms.Button();
            this.btnmodprovee = new System.Windows.Forms.Button();
            this.btnnuevoprov = new System.Windows.Forms.Button();
            this.pnvendedor = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnelimvendedor = new System.Windows.Forms.Button();
            this.btnbusvendedor = new System.Windows.Forms.Button();
            this.btnmodvendedor = new System.Windows.Forms.Button();
            this.btnnvendedor = new System.Windows.Forms.Button();
            this.pncliente = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnelicliente = new System.Windows.Forms.Button();
            this.btnbusccliente = new System.Windows.Forms.Button();
            this.btnmodcliente = new System.Windows.Forms.Button();
            this.btnncliente = new System.Windows.Forms.Button();
            this.btnproducto = new System.Windows.Forms.Button();
            this.btnfactura = new System.Windows.Forms.Button();
            this.btnproveedor = new System.Windows.Forms.Button();
            this.btnmsalir = new System.Windows.Forms.Button();
            this.btnvendedor = new System.Windows.Forms.Button();
            this.btncliente = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnmenu.SuspendLayout();
            this.pnfactura.SuspendLayout();
            this.pnproducto.SuspendLayout();
            this.pnproveedor.SuspendLayout();
            this.pnvendedor.SuspendLayout();
            this.pncliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnmenu
            // 
            this.pnmenu.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.pnmenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnmenu.Controls.Add(this.pnfactura);
            this.pnmenu.Controls.Add(this.pnproducto);
            this.pnmenu.Controls.Add(this.pnproveedor);
            this.pnmenu.Controls.Add(this.pnvendedor);
            this.pnmenu.Controls.Add(this.pncliente);
            this.pnmenu.Controls.Add(this.btnproducto);
            this.pnmenu.Controls.Add(this.btnfactura);
            this.pnmenu.Controls.Add(this.btnproveedor);
            this.pnmenu.Controls.Add(this.btnmsalir);
            this.pnmenu.Controls.Add(this.btnvendedor);
            this.pnmenu.Controls.Add(this.btncliente);
            this.pnmenu.Controls.Add(this.label5);
            this.pnmenu.Controls.Add(this.label4);
            this.pnmenu.Controls.Add(this.label3);
            this.pnmenu.Location = new System.Drawing.Point(12, 12);
            this.pnmenu.Name = "pnmenu";
            this.pnmenu.Size = new System.Drawing.Size(1092, 466);
            this.pnmenu.TabIndex = 2;
            // 
            // pnfactura
            // 
            this.pnfactura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnfactura.Controls.Add(this.btnfacpedido);
            this.pnfactura.Controls.Add(this.label8);
            this.pnfactura.Controls.Add(this.btnfacsucursal);
            this.pnfactura.Controls.Add(this.btnfacpago);
            this.pnfactura.Controls.Add(this.btnfacventa);
            this.pnfactura.Controls.Add(this.btnfaccompra);
            this.pnfactura.Location = new System.Drawing.Point(793, 131);
            this.pnfactura.Name = "pnfactura";
            this.pnfactura.Size = new System.Drawing.Size(251, 300);
            this.pnfactura.TabIndex = 35;
            this.pnfactura.Visible = false;
            // 
            // btnfacpedido
            // 
            this.btnfacpedido.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnfacpedido.BackgroundImage")));
            this.btnfacpedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfacpedido.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnfacpedido.FlatAppearance.BorderSize = 0;
            this.btnfacpedido.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnfacpedido.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnfacpedido.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfacpedido.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnfacpedido.Location = new System.Drawing.Point(-1, 209);
            this.btnfacpedido.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnfacpedido.Name = "btnfacpedido";
            this.btnfacpedido.Size = new System.Drawing.Size(251, 30);
            this.btnfacpedido.TabIndex = 33;
            this.btnfacpedido.Text = "Pedido";
            this.btnfacpedido.UseVisualStyleBackColor = true;
            this.btnfacpedido.Click += new System.EventHandler(this.btnfacpedido_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(86, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 19);
            this.label8.TabIndex = 32;
            this.label8.Text = "Factura";
            // 
            // btnfacsucursal
            // 
            this.btnfacsucursal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnfacsucursal.BackgroundImage")));
            this.btnfacsucursal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfacsucursal.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnfacsucursal.FlatAppearance.BorderSize = 0;
            this.btnfacsucursal.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnfacsucursal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnfacsucursal.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfacsucursal.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnfacsucursal.Location = new System.Drawing.Point(-1, 254);
            this.btnfacsucursal.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnfacsucursal.Name = "btnfacsucursal";
            this.btnfacsucursal.Size = new System.Drawing.Size(251, 30);
            this.btnfacsucursal.TabIndex = 30;
            this.btnfacsucursal.Text = "Sucursal";
            this.btnfacsucursal.UseVisualStyleBackColor = true;
            this.btnfacsucursal.Click += new System.EventHandler(this.btnfacsucursal_Click);
            // 
            // btnfacpago
            // 
            this.btnfacpago.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnfacpago.BackgroundImage")));
            this.btnfacpago.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfacpago.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnfacpago.FlatAppearance.BorderSize = 0;
            this.btnfacpago.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnfacpago.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnfacpago.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfacpago.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnfacpago.Location = new System.Drawing.Point(-1, 171);
            this.btnfacpago.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnfacpago.Name = "btnfacpago";
            this.btnfacpago.Size = new System.Drawing.Size(251, 30);
            this.btnfacpago.TabIndex = 29;
            this.btnfacpago.Text = "Pago";
            this.btnfacpago.UseVisualStyleBackColor = true;
            this.btnfacpago.Click += new System.EventHandler(this.btnfacpago_Click);
            // 
            // btnfacventa
            // 
            this.btnfacventa.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnfacventa.BackgroundImage")));
            this.btnfacventa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfacventa.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnfacventa.FlatAppearance.BorderSize = 0;
            this.btnfacventa.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnfacventa.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnfacventa.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfacventa.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnfacventa.Location = new System.Drawing.Point(-1, 133);
            this.btnfacventa.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnfacventa.Name = "btnfacventa";
            this.btnfacventa.Size = new System.Drawing.Size(251, 30);
            this.btnfacventa.TabIndex = 28;
            this.btnfacventa.Text = "De Venta";
            this.btnfacventa.UseVisualStyleBackColor = true;
            this.btnfacventa.Click += new System.EventHandler(this.btnfacventa_Click);
            // 
            // btnfaccompra
            // 
            this.btnfaccompra.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnfaccompra.BackgroundImage")));
            this.btnfaccompra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfaccompra.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnfaccompra.FlatAppearance.BorderSize = 0;
            this.btnfaccompra.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnfaccompra.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnfaccompra.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfaccompra.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnfaccompra.Location = new System.Drawing.Point(-2, 79);
            this.btnfaccompra.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnfaccompra.Name = "btnfaccompra";
            this.btnfaccompra.Size = new System.Drawing.Size(251, 30);
            this.btnfaccompra.TabIndex = 27;
            this.btnfaccompra.Text = "de Compra";
            this.btnfaccompra.UseVisualStyleBackColor = true;
            this.btnfaccompra.Click += new System.EventHandler(this.btnfaccompra_Click);
            // 
            // pnproducto
            // 
            this.pnproducto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnproducto.Controls.Add(this.label7);
            this.pnproducto.Controls.Add(this.btneliprod);
            this.pnproducto.Controls.Add(this.btnbusprod);
            this.pnproducto.Controls.Add(this.btnmodprod);
            this.pnproducto.Controls.Add(this.btnnprod);
            this.pnproducto.Location = new System.Drawing.Point(1050, 131);
            this.pnproducto.Name = "pnproducto";
            this.pnproducto.Size = new System.Drawing.Size(251, 300);
            this.pnproducto.TabIndex = 34;
            this.pnproducto.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(86, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 19);
            this.label7.TabIndex = 32;
            this.label7.Text = "Producto";
            // 
            // btneliprod
            // 
            this.btneliprod.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btneliprod.BackgroundImage")));
            this.btneliprod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btneliprod.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btneliprod.FlatAppearance.BorderSize = 0;
            this.btneliprod.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btneliprod.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btneliprod.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneliprod.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btneliprod.Location = new System.Drawing.Point(-2, 254);
            this.btneliprod.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btneliprod.Name = "btneliprod";
            this.btneliprod.Size = new System.Drawing.Size(251, 30);
            this.btneliprod.TabIndex = 30;
            this.btneliprod.Text = "Eliminar";
            this.btneliprod.UseVisualStyleBackColor = true;
            this.btneliprod.Click += new System.EventHandler(this.btneliprod_Click);
            // 
            // btnbusprod
            // 
            this.btnbusprod.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnbusprod.BackgroundImage")));
            this.btnbusprod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnbusprod.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnbusprod.FlatAppearance.BorderSize = 0;
            this.btnbusprod.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnbusprod.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnbusprod.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbusprod.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnbusprod.Location = new System.Drawing.Point(-2, 195);
            this.btnbusprod.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnbusprod.Name = "btnbusprod";
            this.btnbusprod.Size = new System.Drawing.Size(251, 30);
            this.btnbusprod.TabIndex = 29;
            this.btnbusprod.Text = "Buscar";
            this.btnbusprod.UseVisualStyleBackColor = true;
            this.btnbusprod.Click += new System.EventHandler(this.btnbusprod_Click);
            // 
            // btnmodprod
            // 
            this.btnmodprod.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnmodprod.BackgroundImage")));
            this.btnmodprod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnmodprod.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnmodprod.FlatAppearance.BorderSize = 0;
            this.btnmodprod.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnmodprod.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnmodprod.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodprod.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnmodprod.Location = new System.Drawing.Point(-2, 137);
            this.btnmodprod.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnmodprod.Name = "btnmodprod";
            this.btnmodprod.Size = new System.Drawing.Size(251, 30);
            this.btnmodprod.TabIndex = 28;
            this.btnmodprod.Text = "Modificar";
            this.btnmodprod.UseVisualStyleBackColor = true;
            this.btnmodprod.Click += new System.EventHandler(this.btnmodprod_Click);
            // 
            // btnnprod
            // 
            this.btnnprod.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnnprod.BackgroundImage")));
            this.btnnprod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnnprod.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnnprod.FlatAppearance.BorderSize = 0;
            this.btnnprod.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnnprod.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnnprod.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnprod.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnnprod.Location = new System.Drawing.Point(-2, 79);
            this.btnnprod.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnnprod.Name = "btnnprod";
            this.btnnprod.Size = new System.Drawing.Size(251, 30);
            this.btnnprod.TabIndex = 27;
            this.btnnprod.Text = "Nuevo";
            this.btnnprod.UseVisualStyleBackColor = true;
            this.btnnprod.Click += new System.EventHandler(this.btnnprod_Click);
            // 
            // pnproveedor
            // 
            this.pnproveedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnproveedor.Controls.Add(this.label6);
            this.pnproveedor.Controls.Add(this.btneliprov);
            this.pnproveedor.Controls.Add(this.btnbusprov);
            this.pnproveedor.Controls.Add(this.btnmodprovee);
            this.pnproveedor.Controls.Add(this.btnnuevoprov);
            this.pnproveedor.Location = new System.Drawing.Point(536, 131);
            this.pnproveedor.Name = "pnproveedor";
            this.pnproveedor.Size = new System.Drawing.Size(251, 300);
            this.pnproveedor.TabIndex = 33;
            this.pnproveedor.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(86, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 19);
            this.label6.TabIndex = 32;
            this.label6.Text = "Proveedor";
            // 
            // btneliprov
            // 
            this.btneliprov.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btneliprov.BackgroundImage")));
            this.btneliprov.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btneliprov.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btneliprov.FlatAppearance.BorderSize = 0;
            this.btneliprov.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btneliprov.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btneliprov.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneliprov.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btneliprov.Location = new System.Drawing.Point(-2, 254);
            this.btneliprov.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btneliprov.Name = "btneliprov";
            this.btneliprov.Size = new System.Drawing.Size(251, 30);
            this.btneliprov.TabIndex = 30;
            this.btneliprov.Text = "Eliminar";
            this.btneliprov.UseVisualStyleBackColor = true;
            this.btneliprov.Click += new System.EventHandler(this.btneliprov_Click);
            // 
            // btnbusprov
            // 
            this.btnbusprov.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnbusprov.BackgroundImage")));
            this.btnbusprov.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnbusprov.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnbusprov.FlatAppearance.BorderSize = 0;
            this.btnbusprov.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnbusprov.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnbusprov.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbusprov.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnbusprov.Location = new System.Drawing.Point(-2, 195);
            this.btnbusprov.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnbusprov.Name = "btnbusprov";
            this.btnbusprov.Size = new System.Drawing.Size(251, 30);
            this.btnbusprov.TabIndex = 29;
            this.btnbusprov.Text = "Buscar";
            this.btnbusprov.UseVisualStyleBackColor = true;
            this.btnbusprov.Click += new System.EventHandler(this.btnbusprov_Click);
            // 
            // btnmodprovee
            // 
            this.btnmodprovee.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnmodprovee.BackgroundImage")));
            this.btnmodprovee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnmodprovee.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnmodprovee.FlatAppearance.BorderSize = 0;
            this.btnmodprovee.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnmodprovee.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnmodprovee.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodprovee.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnmodprovee.Location = new System.Drawing.Point(-2, 137);
            this.btnmodprovee.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnmodprovee.Name = "btnmodprovee";
            this.btnmodprovee.Size = new System.Drawing.Size(251, 30);
            this.btnmodprovee.TabIndex = 28;
            this.btnmodprovee.Text = "Modificar";
            this.btnmodprovee.UseVisualStyleBackColor = true;
            this.btnmodprovee.Click += new System.EventHandler(this.btnmodprovee_Click);
            // 
            // btnnuevoprov
            // 
            this.btnnuevoprov.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnnuevoprov.BackgroundImage")));
            this.btnnuevoprov.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnnuevoprov.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnnuevoprov.FlatAppearance.BorderSize = 0;
            this.btnnuevoprov.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnnuevoprov.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnnuevoprov.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnuevoprov.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnnuevoprov.Location = new System.Drawing.Point(-2, 79);
            this.btnnuevoprov.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnnuevoprov.Name = "btnnuevoprov";
            this.btnnuevoprov.Size = new System.Drawing.Size(251, 30);
            this.btnnuevoprov.TabIndex = 27;
            this.btnnuevoprov.Text = "Nuevo";
            this.btnnuevoprov.UseVisualStyleBackColor = true;
            this.btnnuevoprov.Click += new System.EventHandler(this.button4_Click);
            // 
            // pnvendedor
            // 
            this.pnvendedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnvendedor.Controls.Add(this.label2);
            this.pnvendedor.Controls.Add(this.btnelimvendedor);
            this.pnvendedor.Controls.Add(this.btnbusvendedor);
            this.pnvendedor.Controls.Add(this.btnmodvendedor);
            this.pnvendedor.Controls.Add(this.btnnvendedor);
            this.pnvendedor.Location = new System.Drawing.Point(277, 131);
            this.pnvendedor.Name = "pnvendedor";
            this.pnvendedor.Size = new System.Drawing.Size(251, 300);
            this.pnvendedor.TabIndex = 31;
            this.pnvendedor.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(86, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 19);
            this.label2.TabIndex = 32;
            this.label2.Text = "Cliente";
            // 
            // btnelimvendedor
            // 
            this.btnelimvendedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnelimvendedor.BackgroundImage")));
            this.btnelimvendedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnelimvendedor.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnelimvendedor.FlatAppearance.BorderSize = 0;
            this.btnelimvendedor.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnelimvendedor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnelimvendedor.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnelimvendedor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnelimvendedor.Location = new System.Drawing.Point(-2, 254);
            this.btnelimvendedor.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnelimvendedor.Name = "btnelimvendedor";
            this.btnelimvendedor.Size = new System.Drawing.Size(251, 30);
            this.btnelimvendedor.TabIndex = 30;
            this.btnelimvendedor.Text = "Eliminar";
            this.btnelimvendedor.UseVisualStyleBackColor = true;
            this.btnelimvendedor.Click += new System.EventHandler(this.btnelimvendedor_Click);
            // 
            // btnbusvendedor
            // 
            this.btnbusvendedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnbusvendedor.BackgroundImage")));
            this.btnbusvendedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnbusvendedor.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnbusvendedor.FlatAppearance.BorderSize = 0;
            this.btnbusvendedor.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnbusvendedor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnbusvendedor.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbusvendedor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnbusvendedor.Location = new System.Drawing.Point(-2, 195);
            this.btnbusvendedor.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnbusvendedor.Name = "btnbusvendedor";
            this.btnbusvendedor.Size = new System.Drawing.Size(251, 30);
            this.btnbusvendedor.TabIndex = 29;
            this.btnbusvendedor.Text = "Buscar";
            this.btnbusvendedor.UseVisualStyleBackColor = true;
            this.btnbusvendedor.Click += new System.EventHandler(this.btnbusvendedor_Click);
            // 
            // btnmodvendedor
            // 
            this.btnmodvendedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnmodvendedor.BackgroundImage")));
            this.btnmodvendedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnmodvendedor.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnmodvendedor.FlatAppearance.BorderSize = 0;
            this.btnmodvendedor.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnmodvendedor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnmodvendedor.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodvendedor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnmodvendedor.Location = new System.Drawing.Point(-2, 137);
            this.btnmodvendedor.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnmodvendedor.Name = "btnmodvendedor";
            this.btnmodvendedor.Size = new System.Drawing.Size(251, 30);
            this.btnmodvendedor.TabIndex = 28;
            this.btnmodvendedor.Text = "Modificar";
            this.btnmodvendedor.UseVisualStyleBackColor = true;
            this.btnmodvendedor.Click += new System.EventHandler(this.btnmodvendedor_Click);
            // 
            // btnnvendedor
            // 
            this.btnnvendedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnnvendedor.BackgroundImage")));
            this.btnnvendedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnnvendedor.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnnvendedor.FlatAppearance.BorderSize = 0;
            this.btnnvendedor.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnnvendedor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnnvendedor.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnvendedor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnnvendedor.Location = new System.Drawing.Point(-2, 79);
            this.btnnvendedor.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnnvendedor.Name = "btnnvendedor";
            this.btnnvendedor.Size = new System.Drawing.Size(251, 30);
            this.btnnvendedor.TabIndex = 27;
            this.btnnvendedor.Text = "Nuevo";
            this.btnnvendedor.UseVisualStyleBackColor = true;
            this.btnnvendedor.Click += new System.EventHandler(this.btnnvendedor_Click);
            // 
            // pncliente
            // 
            this.pncliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pncliente.Controls.Add(this.label1);
            this.pncliente.Controls.Add(this.btnelicliente);
            this.pncliente.Controls.Add(this.btnbusccliente);
            this.pncliente.Controls.Add(this.btnmodcliente);
            this.pncliente.Controls.Add(this.btnncliente);
            this.pncliente.Location = new System.Drawing.Point(16, 131);
            this.pncliente.Name = "pncliente";
            this.pncliente.Size = new System.Drawing.Size(251, 300);
            this.pncliente.TabIndex = 26;
            this.pncliente.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(86, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 19);
            this.label1.TabIndex = 31;
            this.label1.Text = "Vendedor";
            // 
            // btnelicliente
            // 
            this.btnelicliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnelicliente.BackgroundImage")));
            this.btnelicliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnelicliente.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnelicliente.FlatAppearance.BorderSize = 0;
            this.btnelicliente.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnelicliente.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnelicliente.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnelicliente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnelicliente.Location = new System.Drawing.Point(0, 254);
            this.btnelicliente.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnelicliente.Name = "btnelicliente";
            this.btnelicliente.Size = new System.Drawing.Size(251, 30);
            this.btnelicliente.TabIndex = 30;
            this.btnelicliente.Text = "Eliminar";
            this.btnelicliente.UseVisualStyleBackColor = true;
            this.btnelicliente.Click += new System.EventHandler(this.btnelicliente_Click);
            // 
            // btnbusccliente
            // 
            this.btnbusccliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnbusccliente.BackgroundImage")));
            this.btnbusccliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnbusccliente.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnbusccliente.FlatAppearance.BorderSize = 0;
            this.btnbusccliente.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnbusccliente.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnbusccliente.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbusccliente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnbusccliente.Location = new System.Drawing.Point(-1, 195);
            this.btnbusccliente.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnbusccliente.Name = "btnbusccliente";
            this.btnbusccliente.Size = new System.Drawing.Size(251, 30);
            this.btnbusccliente.TabIndex = 29;
            this.btnbusccliente.Text = "Buscar";
            this.btnbusccliente.UseVisualStyleBackColor = true;
            this.btnbusccliente.Click += new System.EventHandler(this.btnbusccliente_Click);
            // 
            // btnmodcliente
            // 
            this.btnmodcliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnmodcliente.BackgroundImage")));
            this.btnmodcliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnmodcliente.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnmodcliente.FlatAppearance.BorderSize = 0;
            this.btnmodcliente.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnmodcliente.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnmodcliente.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodcliente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnmodcliente.Location = new System.Drawing.Point(0, 137);
            this.btnmodcliente.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnmodcliente.Name = "btnmodcliente";
            this.btnmodcliente.Size = new System.Drawing.Size(251, 30);
            this.btnmodcliente.TabIndex = 28;
            this.btnmodcliente.Text = "Modificar";
            this.btnmodcliente.UseVisualStyleBackColor = true;
            this.btnmodcliente.Click += new System.EventHandler(this.btnmodcliente_Click);
            // 
            // btnncliente
            // 
            this.btnncliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnncliente.BackgroundImage")));
            this.btnncliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnncliente.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnncliente.FlatAppearance.BorderSize = 0;
            this.btnncliente.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnncliente.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnncliente.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnncliente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnncliente.Location = new System.Drawing.Point(-1, 79);
            this.btnncliente.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnncliente.Name = "btnncliente";
            this.btnncliente.Size = new System.Drawing.Size(251, 30);
            this.btnncliente.TabIndex = 27;
            this.btnncliente.Text = "Nuevo";
            this.btnncliente.UseVisualStyleBackColor = true;
            this.btnncliente.Click += new System.EventHandler(this.btnncliente_Click);
            // 
            // btnproducto
            // 
            this.btnproducto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnproducto.BackgroundImage")));
            this.btnproducto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnproducto.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnproducto.FlatAppearance.BorderSize = 0;
            this.btnproducto.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnproducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnproducto.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnproducto.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnproducto.Location = new System.Drawing.Point(912, 47);
            this.btnproducto.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnproducto.Name = "btnproducto";
            this.btnproducto.Size = new System.Drawing.Size(140, 55);
            this.btnproducto.TabIndex = 25;
            this.btnproducto.Text = "Producto";
            this.btnproducto.UseVisualStyleBackColor = true;
            this.btnproducto.MouseEnter += new System.EventHandler(this.btnproducto_MouseEnter);
            // 
            // btnfactura
            // 
            this.btnfactura.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnfactura.BackgroundImage")));
            this.btnfactura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfactura.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnfactura.FlatAppearance.BorderSize = 0;
            this.btnfactura.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnfactura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfactura.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfactura.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnfactura.Location = new System.Drawing.Point(690, 47);
            this.btnfactura.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnfactura.Name = "btnfactura";
            this.btnfactura.Size = new System.Drawing.Size(140, 55);
            this.btnfactura.TabIndex = 22;
            this.btnfactura.Text = "Factura";
            this.btnfactura.UseVisualStyleBackColor = true;
            this.btnfactura.MouseEnter += new System.EventHandler(this.btnfactura_MouseEnter);
            // 
            // btnproveedor
            // 
            this.btnproveedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnproveedor.BackgroundImage")));
            this.btnproveedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnproveedor.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnproveedor.FlatAppearance.BorderSize = 0;
            this.btnproveedor.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnproveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnproveedor.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnproveedor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnproveedor.Location = new System.Drawing.Point(460, 47);
            this.btnproveedor.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnproveedor.Name = "btnproveedor";
            this.btnproveedor.Size = new System.Drawing.Size(140, 55);
            this.btnproveedor.TabIndex = 21;
            this.btnproveedor.Text = "Proveedor";
            this.btnproveedor.UseVisualStyleBackColor = true;
            this.btnproveedor.MouseEnter += new System.EventHandler(this.btnproveedor_MouseEnter);
            // 
            // btnmsalir
            // 
            this.btnmsalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnmsalir.BackgroundImage")));
            this.btnmsalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnmsalir.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnmsalir.FlatAppearance.BorderSize = 0;
            this.btnmsalir.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnmsalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnmsalir.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmsalir.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnmsalir.Location = new System.Drawing.Point(460, 390);
            this.btnmsalir.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnmsalir.Name = "btnmsalir";
            this.btnmsalir.Size = new System.Drawing.Size(140, 55);
            this.btnmsalir.TabIndex = 20;
            this.btnmsalir.Text = "Salir";
            this.btnmsalir.UseVisualStyleBackColor = true;
            this.btnmsalir.Click += new System.EventHandler(this.btnmsalir_Click);
            // 
            // btnvendedor
            // 
            this.btnvendedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnvendedor.BackgroundImage")));
            this.btnvendedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnvendedor.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btnvendedor.FlatAppearance.BorderSize = 0;
            this.btnvendedor.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btnvendedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnvendedor.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnvendedor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnvendedor.Location = new System.Drawing.Point(44, 47);
            this.btnvendedor.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnvendedor.Name = "btnvendedor";
            this.btnvendedor.Size = new System.Drawing.Size(140, 55);
            this.btnvendedor.TabIndex = 19;
            this.btnvendedor.Text = "Vendedor";
            this.btnvendedor.UseVisualStyleBackColor = true;
            this.btnvendedor.MouseEnter += new System.EventHandler(this.btnvendedor_MouseEnter);
            // 
            // btncliente
            // 
            this.btncliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btncliente.BackgroundImage")));
            this.btncliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btncliente.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.btncliente.FlatAppearance.BorderSize = 0;
            this.btncliente.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.btncliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncliente.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncliente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btncliente.Location = new System.Drawing.Point(234, 47);
            this.btncliente.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btncliente.Name = "btncliente";
            this.btncliente.Size = new System.Drawing.Size(140, 55);
            this.btncliente.TabIndex = 15;
            this.btncliente.Text = "Cliente";
            this.btncliente.UseVisualStyleBackColor = true;
            this.btncliente.MouseEnter += new System.EventHandler(this.btncliente_MouseEnter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Chiller", 26.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(824, 412);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(259, 42);
            this.label5.TabIndex = 12;
            this.label5.Text = "Te Cambia Tus Sueños!";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Chiller", 26.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(773, 304);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 42);
            this.label4.TabIndex = 11;
            this.label4.Text = "Colchones";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Chiller", 57.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(765, 342);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(306, 89);
            this.label3.TabIndex = 10;
            this.label3.Text = "EN-NOVVA";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1116, 490);
            this.ControlBox = false;
            this.Controls.Add(this.pnmenu);
            this.Name = "Form2";
            this.Text = "Principal";
            this.pnmenu.ResumeLayout(false);
            this.pnmenu.PerformLayout();
            this.pnfactura.ResumeLayout(false);
            this.pnfactura.PerformLayout();
            this.pnproducto.ResumeLayout(false);
            this.pnproducto.PerformLayout();
            this.pnproveedor.ResumeLayout(false);
            this.pnproveedor.PerformLayout();
            this.pnvendedor.ResumeLayout(false);
            this.pnvendedor.PerformLayout();
            this.pncliente.ResumeLayout(false);
            this.pncliente.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnmenu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btncliente;
        private System.Windows.Forms.Button btnproducto;
        private System.Windows.Forms.Button btnfactura;
        private System.Windows.Forms.Button btnproveedor;
        private System.Windows.Forms.Button btnmsalir;
        private System.Windows.Forms.Button btnvendedor;
        private System.Windows.Forms.Panel pnvendedor;
        private System.Windows.Forms.Button btnelimvendedor;
        private System.Windows.Forms.Button btnbusvendedor;
        private System.Windows.Forms.Button btnmodvendedor;
        private System.Windows.Forms.Button btnnvendedor;
        private System.Windows.Forms.Panel pncliente;
        private System.Windows.Forms.Button btnelicliente;
        private System.Windows.Forms.Button btnbusccliente;
        private System.Windows.Forms.Button btnmodcliente;
        private System.Windows.Forms.Button btnncliente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnproducto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btneliprod;
        private System.Windows.Forms.Button btnbusprod;
        private System.Windows.Forms.Button btnmodprod;
        private System.Windows.Forms.Button btnnprod;
        private System.Windows.Forms.Panel pnproveedor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btneliprov;
        private System.Windows.Forms.Button btnbusprov;
        private System.Windows.Forms.Button btnmodprovee;
        private System.Windows.Forms.Button btnnuevoprov;
        private System.Windows.Forms.Panel pnfactura;
        private System.Windows.Forms.Button btnfacpedido;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnfacsucursal;
        private System.Windows.Forms.Button btnfacpago;
        private System.Windows.Forms.Button btnfacventa;
        private System.Windows.Forms.Button btnfaccompra;
    }
}