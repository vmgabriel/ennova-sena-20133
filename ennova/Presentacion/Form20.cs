﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ennova.Presentacion
{
    public partial class Form20 : Form
    {
        public Form20()
        {
            InitializeComponent();
        }

        private void btnvendedor_Click(object sender, EventArgs e)
        {
            Form38 a = new Form38();
            a.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form39 a = new Form39();
            a.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form41 a = new Form41();
            a.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form42 a = new Form42();
            a.Show();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form2 a = new Form2();
            a.Show();
            this.Close();
        }
    }
}
