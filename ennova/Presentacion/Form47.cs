﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;
using System.Data.SqlClient;

namespace ennova.Presentacion
{
    public partial class Form47 : Form
    {
        public Form47()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 d = new Form2();
            d.Show();
            this.Close();
        }

        private void Form47_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Producto.pedir();
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            int aq;
            Int64 id = Convert.ToInt64(dgvmostrar.CurrentRow.Cells[0].Value);
            DialogResult a = MessageBox.Show("Desea eliminar el dato????...recuerde que sera removido", "Avizo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (a == DialogResult.Yes)
            {
                aq = Producto.eliminar(id);
                if (aq >= 1)
                {
                    MessageBox.Show("Producto eliminado", "Avizo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form2 entry = new Form2();
                    entry.Show();
                    this.Close();
                }
            }
        }
    }
}
