﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form11 : Form
    {
        public Form11()
        {
            InitializeComponent();
        }

        private void Form11_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Cliente.pedir();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 d = new Form2();
            d.Show();
            this.Close();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            if (txtbusqueda.Text == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string code = txtbusqueda.Text;
                dgvmostrar.DataSource = Logica.Cliente.buscar(txtbusqueda.Text);
                dgvmostrar.Refresh();
            }
        }
    }
}
