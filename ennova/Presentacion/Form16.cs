﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form16 : Form
    {
        public Form16()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form2 d = new Form2();
            d.Show();
            this.Close();
        }

        private void Form16_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Proveedor.pedir();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            if (txtbusqueda.Text == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string code = txtbusqueda.Text;
                dgvmostrar.DataSource = Proveedor.buscar(code);
                dgvmostrar.Refresh();
            }
        }

        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
