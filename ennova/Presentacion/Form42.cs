﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form42 : Form
    {
        public Form42()
        {
            InitializeComponent();
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            int aq;
            Int64 id = Convert.ToInt64(dgvmostrar.CurrentRow.Cells[0].Value);
            DialogResult a = MessageBox.Show("Desea eliminar el dato????...recuerde que sera removido sin poder recuperarlo", "Avizo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (a == DialogResult.Yes)
            {
                aq = Pago.eliminar(id);
                if (aq >= 1)
                {
                    MessageBox.Show("Pago eliminado", "Avizo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form20 entry = new Form20();
                    entry.Show();
                    this.Close();
                }
            }
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form20 a = new Form20();
            a.Show();
            this.Close();
        }

        private void Form42_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Pago.pedir();
        }
    }
}
