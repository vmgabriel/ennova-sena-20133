﻿namespace ennova.Presentacion
{
    partial class Form28
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form28));
            this.gbregistro = new System.Windows.Forms.GroupBox();
            this.cbproveedor = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbprioridad = new System.Windows.Forms.ComboBox();
            this.dtpfechacreacion = new System.Windows.Forms.DateTimePicker();
            this.dtpfechaentrega = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcantidad = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtclase = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtvalor = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbltitulo = new System.Windows.Forms.Label();
            this.btnregistrar = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.btnborrar = new System.Windows.Forms.Button();
            this.gbregistro.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbregistro
            // 
            this.gbregistro.Controls.Add(this.cbproveedor);
            this.gbregistro.Controls.Add(this.label6);
            this.gbregistro.Controls.Add(this.cbprioridad);
            this.gbregistro.Controls.Add(this.dtpfechacreacion);
            this.gbregistro.Controls.Add(this.dtpfechaentrega);
            this.gbregistro.Controls.Add(this.label4);
            this.gbregistro.Controls.Add(this.txtcantidad);
            this.gbregistro.Controls.Add(this.label1);
            this.gbregistro.Controls.Add(this.txtclase);
            this.gbregistro.Controls.Add(this.label9);
            this.gbregistro.Controls.Add(this.label7);
            this.gbregistro.Controls.Add(this.label5);
            this.gbregistro.Controls.Add(this.txtvalor);
            this.gbregistro.Controls.Add(this.label2);
            this.gbregistro.Controls.Add(this.txtnombre);
            this.gbregistro.Controls.Add(this.label3);
            this.gbregistro.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gbregistro.Location = new System.Drawing.Point(12, 71);
            this.gbregistro.Name = "gbregistro";
            this.gbregistro.Size = new System.Drawing.Size(668, 328);
            this.gbregistro.TabIndex = 11;
            this.gbregistro.TabStop = false;
            this.gbregistro.Text = "Sucursal";
            // 
            // cbproveedor
            // 
            this.cbproveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbproveedor.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cbproveedor.FormattingEnabled = true;
            this.cbproveedor.Location = new System.Drawing.Point(384, 254);
            this.cbproveedor.MaxDropDownItems = 90;
            this.cbproveedor.Name = "cbproveedor";
            this.cbproveedor.Size = new System.Drawing.Size(198, 25);
            this.cbproveedor.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(380, 230);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(207, 19);
            this.label6.TabIndex = 34;
            this.label6.Text = "Seleccione El Proveedor:";
            // 
            // cbprioridad
            // 
            this.cbprioridad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbprioridad.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cbprioridad.FormattingEnabled = true;
            this.cbprioridad.Items.AddRange(new object[] {
            "Maxima",
            "Alta",
            "Media",
            "Baja",
            "Minima"});
            this.cbprioridad.Location = new System.Drawing.Point(384, 192);
            this.cbprioridad.MaxDropDownItems = 90;
            this.cbprioridad.Name = "cbprioridad";
            this.cbprioridad.Size = new System.Drawing.Size(164, 25);
            this.cbprioridad.TabIndex = 33;
            // 
            // dtpfechacreacion
            // 
            this.dtpfechacreacion.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.dtpfechacreacion.Location = new System.Drawing.Point(10, 261);
            this.dtpfechacreacion.Name = "dtpfechacreacion";
            this.dtpfechacreacion.Size = new System.Drawing.Size(261, 23);
            this.dtpfechacreacion.TabIndex = 32;
            // 
            // dtpfechaentrega
            // 
            this.dtpfechaentrega.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.dtpfechaentrega.Location = new System.Drawing.Point(10, 193);
            this.dtpfechaentrega.Name = "dtpfechaentrega";
            this.dtpfechaentrega.Size = new System.Drawing.Size(261, 23);
            this.dtpfechaentrega.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(380, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(200, 19);
            this.label4.TabIndex = 30;
            this.label4.Text = "Seleccione la Prioridad:";
            // 
            // txtcantidad
            // 
            this.txtcantidad.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtcantidad.Location = new System.Drawing.Point(384, 123);
            this.txtcantidad.Name = "txtcantidad";
            this.txtcantidad.Size = new System.Drawing.Size(160, 23);
            this.txtcantidad.TabIndex = 27;
            this.txtcantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(380, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(277, 19);
            this.label1.TabIndex = 28;
            this.label1.Text = "Ingrese la cantidad de productos:";
            // 
            // txtclase
            // 
            this.txtclase.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtclase.Location = new System.Drawing.Point(6, 123);
            this.txtclase.Name = "txtclase";
            this.txtclase.Size = new System.Drawing.Size(204, 23);
            this.txtclase.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(6, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(221, 19);
            this.label9.TabIndex = 26;
            this.label9.Text = "Ingrese la Clase de Pedido:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(6, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(250, 19);
            this.label7.TabIndex = 22;
            this.label7.Text = "Seleccione la fecha de entrega:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(6, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(277, 19);
            this.label5.TabIndex = 15;
            this.label5.Text = "Seleccione la fecha de expedicion:";
            // 
            // txtvalor
            // 
            this.txtvalor.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtvalor.Location = new System.Drawing.Point(384, 48);
            this.txtvalor.Name = "txtvalor";
            this.txtvalor.Size = new System.Drawing.Size(160, 23);
            this.txtvalor.TabIndex = 10;
            this.txtvalor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(380, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(230, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ingrese el Valor del pedido:";
            // 
            // txtnombre
            // 
            this.txtnombre.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtnombre.Location = new System.Drawing.Point(10, 48);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(200, 23);
            this.txtnombre.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(6, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ingrese el Nombre:";
            // 
            // lbltitulo
            // 
            this.lbltitulo.AutoSize = true;
            this.lbltitulo.Font = new System.Drawing.Font("Cooper Black", 20.25F);
            this.lbltitulo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltitulo.Location = new System.Drawing.Point(244, 21);
            this.lbltitulo.Name = "lbltitulo";
            this.lbltitulo.Size = new System.Drawing.Size(202, 31);
            this.lbltitulo.TabIndex = 45;
            this.lbltitulo.Text = "Nuevo Pedido";
            // 
            // btnregistrar
            // 
            this.btnregistrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnregistrar.BackgroundImage")));
            this.btnregistrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnregistrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnregistrar.FlatAppearance.BorderSize = 2;
            this.btnregistrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnregistrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnregistrar.Font = new System.Drawing.Font("Constantia", 12F);
            this.btnregistrar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnregistrar.Location = new System.Drawing.Point(12, 417);
            this.btnregistrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnregistrar.Name = "btnregistrar";
            this.btnregistrar.Size = new System.Drawing.Size(158, 67);
            this.btnregistrar.TabIndex = 48;
            this.btnregistrar.Text = "Registrar";
            this.btnregistrar.UseVisualStyleBackColor = true;
            this.btnregistrar.Click += new System.EventHandler(this.btnregistrar_Click);
            // 
            // btnsalir
            // 
            this.btnsalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalir.BackgroundImage")));
            this.btnsalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnsalir.FlatAppearance.BorderSize = 2;
            this.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnsalir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsalir.Font = new System.Drawing.Font("Constantia", 12F);
            this.btnsalir.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnsalir.Location = new System.Drawing.Point(464, 417);
            this.btnsalir.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(158, 67);
            this.btnsalir.TabIndex = 47;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // btnborrar
            // 
            this.btnborrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnborrar.BackgroundImage")));
            this.btnborrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnborrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnborrar.FlatAppearance.BorderSize = 2;
            this.btnborrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnborrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnborrar.Font = new System.Drawing.Font("Constantia", 12F);
            this.btnborrar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnborrar.Location = new System.Drawing.Point(236, 417);
            this.btnborrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnborrar.Name = "btnborrar";
            this.btnborrar.Size = new System.Drawing.Size(158, 67);
            this.btnborrar.TabIndex = 46;
            this.btnborrar.Text = "Borrar";
            this.btnborrar.UseVisualStyleBackColor = true;
            this.btnborrar.Click += new System.EventHandler(this.btnborrar_Click);
            // 
            // Form28
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 496);
            this.ControlBox = false;
            this.Controls.Add(this.btnregistrar);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.btnborrar);
            this.Controls.Add(this.lbltitulo);
            this.Controls.Add(this.gbregistro);
            this.Name = "Form28";
            this.Text = "Nuevo";
            this.Load += new System.EventHandler(this.Form28_Load);
            this.gbregistro.ResumeLayout(false);
            this.gbregistro.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbregistro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcantidad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtclase;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtvalor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbltitulo;
        private System.Windows.Forms.Button btnregistrar;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.Button btnborrar;
        private System.Windows.Forms.ComboBox cbproveedor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbprioridad;
        private System.Windows.Forms.DateTimePicker dtpfechacreacion;
        private System.Windows.Forms.DateTimePicker dtpfechaentrega;
    }
}