﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form31 : Form
    {
        public Form31()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Form21 a = new Form21();
            a.Show();
            this.Close();
        }

        private void Form31_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Pedido.pedir();
        }

        private void txtbusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
                if (txtbusqueda.Text == "")
                {
                    dgvmostrar.DataSource = Pedido.pedir();
                    dgvmostrar.Refresh();
                }
                else
                {
                    string code = txtbusqueda.Text;
                    dgvmostrar.DataSource = Pedido.buscar(code);
                    dgvmostrar.Refresh();
                }
                string sas = txtbusqueda.Text;
                dgvmostrar.DataSource = Pedido.buscar(sas);
                dgvmostrar.Refresh();

            }
        }
    }
}
