﻿namespace ennova.Presentacion
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.gbregistro = new System.Windows.Forms.GroupBox();
            this.cbsucursal = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtalias = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtcontraseña = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtdire = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtcorreo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gbgenero = new System.Windows.Forms.GroupBox();
            this.rbmasculino = new System.Windows.Forms.RadioButton();
            this.rbfemenino = new System.Windows.Forms.RadioButton();
            this.txtape2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtnumero = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtape1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnborrar = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.btnregistrar = new System.Windows.Forms.Button();
            this.PE1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.gbregistro.SuspendLayout();
            this.gbgenero.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PE1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbregistro
            // 
            this.gbregistro.Controls.Add(this.cbsucursal);
            this.gbregistro.Controls.Add(this.label8);
            this.gbregistro.Controls.Add(this.txtalias);
            this.gbregistro.Controls.Add(this.label11);
            this.gbregistro.Controls.Add(this.txtcontraseña);
            this.gbregistro.Controls.Add(this.label10);
            this.gbregistro.Controls.Add(this.txtdire);
            this.gbregistro.Controls.Add(this.label9);
            this.gbregistro.Controls.Add(this.txtcorreo);
            this.gbregistro.Controls.Add(this.label7);
            this.gbregistro.Controls.Add(this.gbgenero);
            this.gbregistro.Controls.Add(this.txtape2);
            this.gbregistro.Controls.Add(this.label6);
            this.gbregistro.Controls.Add(this.txtnumero);
            this.gbregistro.Controls.Add(this.label5);
            this.gbregistro.Controls.Add(this.txtcc);
            this.gbregistro.Controls.Add(this.label4);
            this.gbregistro.Controls.Add(this.txtape1);
            this.gbregistro.Controls.Add(this.label2);
            this.gbregistro.Controls.Add(this.txtnombre);
            this.gbregistro.Controls.Add(this.label3);
            this.gbregistro.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gbregistro.Location = new System.Drawing.Point(12, 59);
            this.gbregistro.Name = "gbregistro";
            this.gbregistro.Size = new System.Drawing.Size(577, 449);
            this.gbregistro.TabIndex = 6;
            this.gbregistro.TabStop = false;
            this.gbregistro.Text = "Empleado";
            // 
            // cbsucursal
            // 
            this.cbsucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbsucursal.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cbsucursal.FormattingEnabled = true;
            this.cbsucursal.Location = new System.Drawing.Point(325, 393);
            this.cbsucursal.MaxDropDownItems = 90;
            this.cbsucursal.Name = "cbsucursal";
            this.cbsucursal.Size = new System.Drawing.Size(198, 25);
            this.cbsucursal.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(327, 371);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(192, 19);
            this.label8.TabIndex = 34;
            this.label8.Text = "Seleccione la Sucursal:\r\n";
            // 
            // txtalias
            // 
            this.txtalias.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtalias.Location = new System.Drawing.Point(328, 218);
            this.txtalias.Name = "txtalias";
            this.txtalias.Size = new System.Drawing.Size(188, 23);
            this.txtalias.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label11.Location = new System.Drawing.Point(326, 188);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(138, 19);
            this.label11.TabIndex = 30;
            this.label11.Text = "Ingrese el Alias:";
            // 
            // txtcontraseña
            // 
            this.txtcontraseña.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtcontraseña.Location = new System.Drawing.Point(28, 210);
            this.txtcontraseña.Name = "txtcontraseña";
            this.txtcontraseña.Size = new System.Drawing.Size(188, 23);
            this.txtcontraseña.TabIndex = 27;
            this.txtcontraseña.UseSystemPasswordChar = true;
            this.txtcontraseña.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sinespacios_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label10.Location = new System.Drawing.Point(23, 188);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(191, 19);
            this.label10.TabIndex = 28;
            this.label10.Text = "Ingrese la Contraseña:";
            // 
            // txtdire
            // 
            this.txtdire.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtdire.Location = new System.Drawing.Point(27, 278);
            this.txtdire.Name = "txtdire";
            this.txtdire.Size = new System.Drawing.Size(188, 23);
            this.txtdire.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label9.Location = new System.Drawing.Point(26, 256);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(176, 19);
            this.label9.TabIndex = 26;
            this.label9.Text = "Ingrese la Direccion:";
            // 
            // txtcorreo
            // 
            this.txtcorreo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtcorreo.Location = new System.Drawing.Point(27, 351);
            this.txtcorreo.Name = "txtcorreo";
            this.txtcorreo.Size = new System.Drawing.Size(255, 23);
            this.txtcorreo.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label7.Location = new System.Drawing.Point(31, 329);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 19);
            this.label7.TabIndex = 22;
            this.label7.Text = "Ingrese el Correo:";
            // 
            // gbgenero
            // 
            this.gbgenero.BackColor = System.Drawing.Color.Transparent;
            this.gbgenero.Controls.Add(this.rbmasculino);
            this.gbgenero.Controls.Add(this.rbfemenino);
            this.gbgenero.Location = new System.Drawing.Point(332, 256);
            this.gbgenero.Name = "gbgenero";
            this.gbgenero.Size = new System.Drawing.Size(174, 92);
            this.gbgenero.TabIndex = 20;
            this.gbgenero.TabStop = false;
            // 
            // rbmasculino
            // 
            this.rbmasculino.AutoSize = true;
            this.rbmasculino.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.rbmasculino.Location = new System.Drawing.Point(28, 19);
            this.rbmasculino.Name = "rbmasculino";
            this.rbmasculino.Size = new System.Drawing.Size(110, 23);
            this.rbmasculino.TabIndex = 18;
            this.rbmasculino.TabStop = true;
            this.rbmasculino.Text = "Masculino";
            this.rbmasculino.UseVisualStyleBackColor = true;
            // 
            // rbfemenino
            // 
            this.rbfemenino.AutoSize = true;
            this.rbfemenino.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.rbfemenino.Location = new System.Drawing.Point(31, 54);
            this.rbfemenino.Name = "rbfemenino";
            this.rbfemenino.Size = new System.Drawing.Size(107, 23);
            this.rbfemenino.TabIndex = 19;
            this.rbfemenino.TabStop = true;
            this.rbfemenino.Text = "Femenino";
            this.rbfemenino.UseVisualStyleBackColor = true;
            // 
            // txtape2
            // 
            this.txtape2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtape2.Location = new System.Drawing.Point(332, 142);
            this.txtape2.Name = "txtape2";
            this.txtape2.Size = new System.Drawing.Size(188, 23);
            this.txtape2.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label6.Location = new System.Drawing.Point(326, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(180, 19);
            this.label6.TabIndex = 16;
            this.label6.Text = "Ingrese el Apellido 2:";
            // 
            // txtnumero
            // 
            this.txtnumero.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtnumero.Location = new System.Drawing.Point(28, 412);
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(188, 23);
            this.txtnumero.TabIndex = 14;
            this.txtnumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label5.Location = new System.Drawing.Point(23, 390);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(253, 19);
            this.label5.TabIndex = 15;
            this.label5.Text = "Ingrese el numero de telefono:";
            // 
            // txtcc
            // 
            this.txtcc.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtcc.Location = new System.Drawing.Point(331, 75);
            this.txtcc.Name = "txtcc";
            this.txtcc.Size = new System.Drawing.Size(188, 23);
            this.txtcc.TabIndex = 12;
            this.txtcc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label4.Location = new System.Drawing.Point(326, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 19);
            this.label4.TabIndex = 13;
            this.label4.Text = "Ingrese la CC:";
            // 
            // txtape1
            // 
            this.txtape1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtape1.Location = new System.Drawing.Point(28, 134);
            this.txtape1.Name = "txtape1";
            this.txtape1.Size = new System.Drawing.Size(188, 23);
            this.txtape1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label2.Location = new System.Drawing.Point(24, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ingrese el Apellido 1:";
            // 
            // txtnombre
            // 
            this.txtnombre.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtnombre.Location = new System.Drawing.Point(28, 67);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(188, 23);
            this.txtnombre.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label3.Location = new System.Drawing.Point(24, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ingrese el Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(138, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(326, 33);
            this.label1.TabIndex = 32;
            this.label1.Text = "Registrar Vendedor";
            // 
            // btnborrar
            // 
            this.btnborrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnborrar.BackgroundImage")));
            this.btnborrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnborrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnborrar.FlatAppearance.BorderSize = 2;
            this.btnborrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnborrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnborrar.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnborrar.Location = new System.Drawing.Point(216, 525);
            this.btnborrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnborrar.Name = "btnborrar";
            this.btnborrar.Size = new System.Drawing.Size(158, 67);
            this.btnborrar.TabIndex = 13;
            this.btnborrar.Text = "Borrar";
            this.btnborrar.UseVisualStyleBackColor = true;
            this.btnborrar.Click += new System.EventHandler(this.btnborrar_Click);
            // 
            // btnsalir
            // 
            this.btnsalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalir.BackgroundImage")));
            this.btnsalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnsalir.FlatAppearance.BorderSize = 2;
            this.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnsalir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsalir.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalir.Location = new System.Drawing.Point(431, 525);
            this.btnsalir.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(158, 67);
            this.btnsalir.TabIndex = 12;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // btnregistrar
            // 
            this.btnregistrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnregistrar.BackgroundImage")));
            this.btnregistrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnregistrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnregistrar.FlatAppearance.BorderSize = 2;
            this.btnregistrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnregistrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnregistrar.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnregistrar.Location = new System.Drawing.Point(12, 525);
            this.btnregistrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnregistrar.Name = "btnregistrar";
            this.btnregistrar.Size = new System.Drawing.Size(158, 67);
            this.btnregistrar.TabIndex = 11;
            this.btnregistrar.Text = "Registrar";
            this.btnregistrar.UseVisualStyleBackColor = true;
            this.btnregistrar.Click += new System.EventHandler(this.btnregistrar_Click);
            // 
            // PE1
            // 
            this.PE1.ContainerControl = this;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DodgerBlue;
            this.ClientSize = new System.Drawing.Size(603, 604);
            this.ControlBox = false;
            this.Controls.Add(this.btnborrar);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbregistro);
            this.Controls.Add(this.btnregistrar);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registrar Vendedor";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.gbregistro.ResumeLayout(false);
            this.gbregistro.PerformLayout();
            this.gbgenero.ResumeLayout(false);
            this.gbgenero.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PE1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbregistro;
        private System.Windows.Forms.TextBox txtalias;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtcontraseña;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtdire;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtcorreo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbgenero;
        private System.Windows.Forms.RadioButton rbmasculino;
        private System.Windows.Forms.RadioButton rbfemenino;
        private System.Windows.Forms.TextBox txtape2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtnumero;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtcc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtape1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnborrar;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.Button btnregistrar;
        private System.Windows.Forms.ErrorProvider PE1;
        private System.Windows.Forms.ComboBox cbsucursal;
        private System.Windows.Forms.Label label8;
    }
}