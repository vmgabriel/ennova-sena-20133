﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ennova.Logica;

namespace ennova.Presentacion
{
    public partial class Form41 : Form
    {
        public Form41()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form20 a = new Form20();
            a.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtbusqueda.Text == "")
            {
                MessageBox.Show("Ingrese lo datos requeridos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string code = txtbusqueda.Text;
                dgvmostrar.DataSource = Pago.buscar(code);
                dgvmostrar.Refresh();
            }
        }

        private void Form41_Load(object sender, EventArgs e)
        {
            dgvmostrar.DataSource = Pago.pedir();
        }

        private void sinespacios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
