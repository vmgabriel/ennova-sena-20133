﻿namespace ennova.Presentacion
{
    partial class Form45
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form45));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbproveedor = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtespuma = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txttapizado = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtgarantia = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtlargo = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtclase = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtcategoria = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txttela = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtanchura = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtcosto = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtaltura = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txttipo = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.btnborrar = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.btnregistrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbproveedor);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.txtespuma);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.txttapizado);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtgarantia);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtlargo);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtclase);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtcategoria);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txttela);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtanchura);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtcosto);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txtaltura);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.txttipo);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.txtnombre);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.Location = new System.Drawing.Point(7, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(564, 510);
            this.groupBox1.TabIndex = 43;
            this.groupBox1.TabStop = false;
            // 
            // cbproveedor
            // 
            this.cbproveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbproveedor.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cbproveedor.FormattingEnabled = true;
            this.cbproveedor.Location = new System.Drawing.Point(22, 450);
            this.cbproveedor.MaxDropDownItems = 90;
            this.cbproveedor.Name = "cbproveedor";
            this.cbproveedor.Size = new System.Drawing.Size(198, 25);
            this.cbproveedor.TabIndex = 41;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label25.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label25.Location = new System.Drawing.Point(18, 428);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(199, 19);
            this.label25.TabIndex = 40;
            this.label25.Text = "Seleccione el Proveedor\r\n";
            // 
            // txtespuma
            // 
            this.txtespuma.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtespuma.Location = new System.Drawing.Point(329, 427);
            this.txtespuma.Name = "txtespuma";
            this.txtespuma.Size = new System.Drawing.Size(188, 23);
            this.txtespuma.TabIndex = 39;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label24.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label24.Location = new System.Drawing.Point(325, 397);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(162, 19);
            this.label24.TabIndex = 38;
            this.label24.Text = "Ingrese la Espuma:\r\n";
            // 
            // txttapizado
            // 
            this.txttapizado.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txttapizado.Location = new System.Drawing.Point(329, 355);
            this.txttapizado.Name = "txttapizado";
            this.txttapizado.Size = new System.Drawing.Size(188, 23);
            this.txttapizado.TabIndex = 37;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(325, 333);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(170, 19);
            this.label13.TabIndex = 36;
            this.label13.Text = "Ingrese el Tapizado:\r\n";
            // 
            // txtgarantia
            // 
            this.txtgarantia.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtgarantia.Location = new System.Drawing.Point(329, 291);
            this.txtgarantia.Name = "txtgarantia";
            this.txtgarantia.Size = new System.Drawing.Size(188, 23);
            this.txtgarantia.TabIndex = 35;
            this.txtgarantia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label14.Location = new System.Drawing.Point(302, 269);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(228, 19);
            this.label14.TabIndex = 34;
            this.label14.Text = "Ingrese la Garantia (años):\r\n";
            // 
            // txtlargo
            // 
            this.txtlargo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtlargo.Location = new System.Drawing.Point(329, 182);
            this.txtlargo.Name = "txtlargo";
            this.txtlargo.Size = new System.Drawing.Size(188, 23);
            this.txtlargo.TabIndex = 29;
            this.txtlargo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label15.Location = new System.Drawing.Point(325, 163);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(143, 19);
            this.label15.TabIndex = 30;
            this.label15.Text = "Ingrese el Largo:";
            // 
            // txtclase
            // 
            this.txtclase.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtclase.Location = new System.Drawing.Point(28, 180);
            this.txtclase.Name = "txtclase";
            this.txtclase.Size = new System.Drawing.Size(188, 23);
            this.txtclase.TabIndex = 27;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label16.Location = new System.Drawing.Point(24, 158);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(121, 19);
            this.label16.TabIndex = 28;
            this.label16.Text = "Ingrese Clase:";
            // 
            // txtcategoria
            // 
            this.txtcategoria.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtcategoria.Location = new System.Drawing.Point(28, 250);
            this.txtcategoria.Name = "txtcategoria";
            this.txtcategoria.Size = new System.Drawing.Size(188, 23);
            this.txtcategoria.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label17.Location = new System.Drawing.Point(23, 228);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(176, 19);
            this.label17.TabIndex = 26;
            this.label17.Text = "Ingrese la Categoria:";
            // 
            // txttela
            // 
            this.txttela.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txttela.Location = new System.Drawing.Point(28, 321);
            this.txttela.Name = "txttela";
            this.txttela.Size = new System.Drawing.Size(192, 23);
            this.txttela.TabIndex = 21;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label18.Location = new System.Drawing.Point(24, 296);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(131, 19);
            this.label18.TabIndex = 22;
            this.label18.Text = "Ingrese la Tela:";
            // 
            // txtanchura
            // 
            this.txtanchura.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtanchura.Location = new System.Drawing.Point(329, 119);
            this.txtanchura.Name = "txtanchura";
            this.txtanchura.Size = new System.Drawing.Size(188, 23);
            this.txtanchura.TabIndex = 17;
            this.txtanchura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label19.Location = new System.Drawing.Point(325, 97);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(168, 19);
            this.label19.TabIndex = 16;
            this.label19.Text = "Ingrese el Anchura:";
            // 
            // txtcosto
            // 
            this.txtcosto.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtcosto.Location = new System.Drawing.Point(27, 385);
            this.txtcosto.Name = "txtcosto";
            this.txtcosto.Size = new System.Drawing.Size(188, 23);
            this.txtcosto.TabIndex = 14;
            this.txtcosto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label20.Location = new System.Drawing.Point(23, 363);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(143, 19);
            this.label20.TabIndex = 15;
            this.label20.Text = "Ingrese el Costo:";
            // 
            // txtaltura
            // 
            this.txtaltura.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtaltura.Location = new System.Drawing.Point(329, 52);
            this.txtaltura.Name = "txtaltura";
            this.txtaltura.Size = new System.Drawing.Size(188, 23);
            this.txtaltura.TabIndex = 12;
            this.txtaltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.solonumeros_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label21.Location = new System.Drawing.Point(325, 30);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(152, 19);
            this.label21.TabIndex = 13;
            this.label21.Text = "Ingrese la Altura:";
            // 
            // txttipo
            // 
            this.txttipo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txttipo.Location = new System.Drawing.Point(27, 107);
            this.txttipo.Name = "txttipo";
            this.txttipo.Size = new System.Drawing.Size(188, 23);
            this.txttipo.TabIndex = 10;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label22.Location = new System.Drawing.Point(24, 85);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(116, 19);
            this.label22.TabIndex = 11;
            this.label22.Text = "Ingrese Tipo:";
            // 
            // txtnombre
            // 
            this.txtnombre.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtnombre.Location = new System.Drawing.Point(27, 40);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(188, 23);
            this.txtnombre.TabIndex = 8;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label23.Location = new System.Drawing.Point(24, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(143, 19);
            this.label23.TabIndex = 9;
            this.label23.Text = "Ingrese Nombre:";
            // 
            // btnborrar
            // 
            this.btnborrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnborrar.BackgroundImage")));
            this.btnborrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnborrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnborrar.FlatAppearance.BorderSize = 2;
            this.btnborrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnborrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnborrar.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnborrar.Location = new System.Drawing.Point(207, 583);
            this.btnborrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnborrar.Name = "btnborrar";
            this.btnborrar.Size = new System.Drawing.Size(158, 67);
            this.btnborrar.TabIndex = 42;
            this.btnborrar.Text = "Borrar";
            this.btnborrar.UseVisualStyleBackColor = true;
            this.btnborrar.Click += new System.EventHandler(this.btnborrar_Click);
            // 
            // btnsalir
            // 
            this.btnsalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalir.BackgroundImage")));
            this.btnsalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnsalir.FlatAppearance.BorderSize = 2;
            this.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnsalir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsalir.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalir.Location = new System.Drawing.Point(401, 583);
            this.btnsalir.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(158, 67);
            this.btnsalir.TabIndex = 41;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // btnregistrar
            // 
            this.btnregistrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnregistrar.BackgroundImage")));
            this.btnregistrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnregistrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnregistrar.FlatAppearance.BorderSize = 2;
            this.btnregistrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.btnregistrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnregistrar.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnregistrar.Location = new System.Drawing.Point(16, 583);
            this.btnregistrar.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnregistrar.Name = "btnregistrar";
            this.btnregistrar.Size = new System.Drawing.Size(158, 67);
            this.btnregistrar.TabIndex = 40;
            this.btnregistrar.Text = "Modificar";
            this.btnregistrar.UseVisualStyleBackColor = true;
            this.btnregistrar.Click += new System.EventHandler(this.btnregistrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(124, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(333, 33);
            this.label1.TabIndex = 39;
            this.label1.Text = "Modificar Producto";
            // 
            // Form45
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 666);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnborrar);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.btnregistrar);
            this.Controls.Add(this.label1);
            this.Name = "Form45";
            this.Text = "Modificar";
            this.Load += new System.EventHandler(this.Form45_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbproveedor;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtespuma;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txttapizado;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtgarantia;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtlargo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtclase;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtcategoria;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txttela;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtanchura;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtcosto;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtaltura;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txttipo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnborrar;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.Button btnregistrar;
        private System.Windows.Forms.Label label1;
    }
}