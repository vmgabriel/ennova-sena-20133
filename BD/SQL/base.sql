--reconfiguracion de fechas de sql server a visual y viserversa....
SP_CONFIGURE 'default language', 5
RECONFIGURE;
go
use master
select *from syslanguages
go
create database ennova

go

use ennova

go

create table cliente
(
id_cliente bigint identity (1,1) primary key not null,
nombre_cliente varchar (40) not null,
apellido1_cliente varchar (25) not null,
apellido2_cliente varchar (25) null,
correo_cliente varchar (40) not null,
genero_cliente varchar (20) not null,
cedula_cliente bigint not null,
direccion_cliente varchar (50) not null,
telefono_cliente varchar (20) not null,
)

go

create table proveedor
(
id_proveedor bigint identity (1,1) primary key not null,
nombre_proveedor varchar (30) not null,
direccion_proveedor varchar (40) not null,
telefono_proveedor varchar (20) not null,
correo_proveedor varchar (30) not null,
descripcion_proveedor varchar (150) not null,
)

go

create table sucursal
(
id_sucursal bigint identity (1,1) primary key not null,
nombre_sucursal varchar (40) not null,
direccion_sucursal varchar (40) not null,
ciudad_sucursal varchar (40) not null,
pais_sucursal varchar (40) not null,
telefono_sucursal varchar (30) not null,
correo_sucursal varchar (30) not null,
nit_estandar_sucursal varchar (30) not null,
)

go

create table vendedor
(
Id_vendedor bigint identity (1,1) primary key not null,
nombre_vendedor varchar (30) not null,
apellido1_vendedor varchar (30) not null,
apellido2_vendedor varchar (30) not null,
nick_vendedor varchar (30) not null,
contraseņa_vendedor varchar (20) not null,
direccion_vendedor varchar (50) not null,
genero_vendedor varchar (20) not null,
correo_vendedor varchar (50) not null,
telefono_vendedor varchar (20) not null,
cc_vendedor varchar (20) not null,
FK_sucursal_vendedor bigint not null,
foreign key(Fk_sucursal_vendedor)references sucursal(id_sucursal),
)

go

create table pedido
(
id_pedido bigint identity (1,1) primary key not null,
nombre_pedido varchar (30) not null,
clase_pedido varchar (30) not null,
fecha_entrega_pedido datetime not null,
fecha_expedida_pedido datetime not null,
valor_pedido bigint not null,
cantidad_productos_pedido bigint not null,
necesidad_producto_pedido varchar (30) not null,
FK_proveedor_pedido bigint not null,
foreign key(Fk_proveedor_pedido)references proveedor(id_proveedor),
)

go

create table factura_compra
(
id_fc bigint identity (1,1) primary key not null,
tipo_fc varchar (30) not null,
organizacion_fc varchar (30) not null,
fecha_creacion_fc datetime not null,
ciudad_fc varchar (30) not null,
iva_fc bigint not null,
total_fc bigint not null,
sub_total_fc bigint not null,
Fk_vendedor_fc bigint not null,
Fk_pedido_fc bigint not null,
foreign key(Fk_vendedor_fc)references vendedor(Id_vendedor),
foreign key(Fk_pedido_fc)references pedido(id_pedido),
)

go

create table pago
(
id_pago bigint identity (1,1) primary key not null,
tipo_pago varchar (30) not null,
iva_pago bigint not null,
total_pago bigint not null,
sub_total_pago bigint not null,
Fk_cliente_pago bigint not null,
Fk_vendedor_pago bigint not null,
Fk_sucursal_pago bigint not null,
foreign key(Fk_cliente_pago)references cliente(id_cliente),
foreign key(Fk_vendedor_pago)references vendedor(Id_vendedor),
foreign key(Fk_sucursal_pago)references sucursal(id_sucursal),
)

go

create table factura_venta
(
id_fv bigint identity (1,1) primary key not null,
tipo_fv varchar (40) not null,
total_fv bigint not null,
iva_fv bigint not null,
sub_total_fv bigint not null,
comentarios_fv varchar (125) not null,
Fk_cliente_fv bigint not null,
Fk_vendedor_fv bigint not null,
Fk_sucursal_fv bigint not null,
fecha_creacion_fv datetime not null,
feha_entrega_fv datetime not null,
foreign key(Fk_cliente_fv)references cliente(id_cliente),
foreign key(Fk_vendedor_fv)references vendedor(Id_vendedor),
foreign key(Fk_sucursal_fv)references sucursal(id_sucursal),
)

go

create table producto
(
id_producto bigint identity (1,1) primary key not null,
nombre_producto varchar (40) not null,
tipo_producto varchar (20) not null,
clase_producto varchar(40) not null,
categoria_producto varchar(40) not null,
tela_producto varchar (30) not null,
costo_producto bigint not null,
altura_producto bigint not null,
anchura_producto bigint not null,
largo_producto bigint not null,
garantia_producto float not null,
tapizado_producto varchar (30) not null,
espuma_producto varchar (30) not null,
Fk_proveedor_producto bigint not null,
foreign key(Fk_proveedor_producto)references proveedor(id_proveedor),
)

go

create table detallefactura_venta
(
id_detallefv bigint identity (1,1) primary key not null,
cantidad_productos_detalle bigint not null,
valor_total_productos_detalle bigint not null,
Fk_prod_detallefv bigint not null,
Fk_fv_detallefv bigint not null,
foreign key(Fk_fv_detallefv)references factura_venta(id_fv),
foreign key(Fk_prod_detallefv)references producto(id_producto),
)

go

/*
--------------------------

Procedimientos almacenados

--------------------------
*/

--codigo para modificar el datetime y que quede con el formato que se envia en visual

/*Cliente*/
go

create proc crear_cliente
@nombre_cliente varchar (40),
@apellido1_cliente varchar (25),
@apellido2_cliente varchar (25),
@correo_cliente varchar (40),
@genero_cliente varchar (20),
@cedula_cliente bigint,
@direccion_cliente varchar (50),
@telefono_cliente varchar (20)
as
begin
insert into cliente values (@nombre_cliente,@apellido1_cliente,@apellido2_cliente,@correo_cliente,@genero_cliente,@cedula_cliente,@direccion_cliente,@telefono_cliente)
end

go

create proc eliminar_cliente
@id bigint
as
begin
delete from cliente where cliente.id_cliente=@id
end

go

create proc modificar_cliente
@id_cliente bigint,
@nombre_cliente varchar (40),
@apellido1_cliente varchar (25),
@apellido2_cliente varchar (25),
@correo_cliente varchar (40),
@genero_cliente varchar (20),
@cedula_cliente bigint,
@direccion_cliente varchar (50),
@telefono_cliente varchar (20)
as
begin
update cliente
set
nombre_cliente=@nombre_cliente,
apellido1_cliente=@apellido1_cliente,
apellido2_cliente=@apellido2_cliente,
correo_cliente=@correo_cliente,
genero_cliente=@genero_cliente,
cedula_cliente=@cedula_cliente,
direccion_cliente=@direccion_cliente,
telefono_cliente=@telefono_cliente
where
id_cliente=@id_cliente
end

go

create proc todos_cliente
as
begin
select *from cliente
end

go

create proc buscar_cliente
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from cliente
where cliente.id_cliente like @code or cliente.apellido1_cliente like @dato or cliente.apellido2_cliente like @dato or cliente.cedula_cliente like @code or cliente.correo_cliente like @code or cliente.direccion_cliente like @dato or cliente.genero_cliente like @dato or cliente.nombre_cliente like @dato or cliente.telefono_cliente like @dato
end

go

create proc seleccionar_cliente
@id bigint
as
begin
select *from cliente where id_cliente=@id
end

go
/*proveedor*/

create proc crear_proveedor
@nombre_proveedor varchar (30),
@direccion_proveedor varchar (40),
@telefono_proveedor varchar (20),
@correo_proveedor varchar (30),
@descripcion_proveedor varchar (150)
as
begin
insert into proveedor values (@nombre_proveedor,@direccion_proveedor,@telefono_proveedor,@correo_proveedor,@descripcion_proveedor)
end
go

create proc eliminar_proveedor
@id bigint
as
begin
delete from proveedor where proveedor.id_proveedor=@id
end

go

create proc modificar_proveedor
@id_proveedor bigint,
@nombre_proveedor varchar (30),
@direccion_proveedor varchar (40),
@telefono_proveedor varchar (20),
@correo_proveedor varchar (30),
@descripcion_proveedor varchar (150)
as
begin
update proveedor
set
nombre_proveedor=@nombre_proveedor,
direccion_proveedor=@direccion_proveedor,
telefono_proveedor=@telefono_proveedor,
correo_proveedor=@correo_proveedor,
descripcion_proveedor=@descripcion_proveedor
where
id_proveedor=@id_proveedor
end

go

create proc todos_proveedor
as
begin
select *from proveedor
end

go

create proc buscar_proveedor
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from proveedor
where proveedor.id_proveedor like @code or proveedor.nombre_proveedor like @dato or proveedor.correo_proveedor like @dato or proveedor.descripcion_proveedor like @dato or proveedor.direccion_proveedor like @dato or proveedor.telefono_proveedor like @dato
end

go

create proc seleccionar_proveedor
@id bigint
as
begin
select *from proveedor where id_proveedor=@id
end

go

/* SUCURSAL */

create proc crear_sucursal
@nombre_sucursal varchar (40),
@direccion_sucursal varchar (40),
@ciudad_sucursal varchar (40),
@pais_sucursal varchar (40),
@telefono_sucursal varchar (30),
@correo_sucursal varchar (30),
@nit_estandar_sucursal varchar (30)
as
begin
insert into sucursal values (@nombre_sucursal,@direccion_sucursal,@ciudad_sucursal,@pais_sucursal,@telefono_sucursal,@correo_sucursal,@nit_estandar_sucursal)
end
go

create proc eliminar_sucursal
@id bigint
as
begin
delete from sucursal where sucursal.id_sucursal=@id
end

go

create proc modificar_sucursal
@id_sucursal bigint,
@nombre_sucursal varchar (40),
@direccion_sucursal varchar (40),
@ciudad_sucursal varchar (40),
@pais_sucursal varchar (40),
@telefono_sucursal varchar (30),
@correo_sucursal varchar (30),
@nit_estandar_sucursal varchar (30)
as
begin
update sucursal
set
nombre_sucursal=@nombre_sucursal,
direccion_sucursal=@direccion_sucursal,
ciudad_sucursal=@ciudad_sucursal,
pais_sucursal=@pais_sucursal,
telefono_sucursal=@telefono_sucursal,
correo_sucursal=@correo_sucursal,
nit_estandar_sucursal=@nit_estandar_sucursal
where
id_sucursal=@id_sucursal
end

go

create proc todos_sucursal
as
begin
select *from sucursal
end

go

create proc buscar_sucursal
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from sucursal
where id_sucursal like @code or nombre_sucursal like @dato or direccion_sucursal like @dato or ciudad_sucursal like @dato or pais_sucursal like @dato or telefono_sucursal like @dato or correo_sucursal like @dato or nit_estandar_sucursal like @dato
end

go

create proc seleccionar_sucursal
@id bigint
as
begin
select *from sucursal where id_sucursal=@id
end
go

/*Vendedor*/
go
create proc crear_vendedor
@nombre_vendedor varchar (30),
@apellido1_vendedor varchar (30),
@apellido2_vendedor varchar (30),
@nick_vendedor varchar (30),
@contraseņa_vendedor varchar (20),
@direccion_vendedor varchar (50),
@genero_vendedor varchar (20),
@correo_vendedor varchar (50),
@telefono_vendedor varchar (20),
@cc_vendedor varchar (20),
@Fk_sucursal_vendedor bigint
as
begin
insert into vendedor values (@nombre_vendedor,@apellido1_vendedor,@apellido2_vendedor,@nick_vendedor,@contraseņa_vendedor,@direccion_vendedor,@genero_vendedor,@correo_vendedor,@telefono_vendedor,@cc_vendedor,@Fk_sucursal_vendedor)
end
go
create proc eliminar_vendedor
@id bigint
as
begin
delete from vendedor where Id_vendedor=@id
end
go
create proc ingresar_vendedor
@nick varchar (30),
@contra varchar (20)
as
begin
select *from vendedor
where
nick_vendedor=@nick and contraseņa_vendedor=@contra
end
go
create proc modificar_vendedor
@id_vendedor bigint,
@nombre_vendedor varchar (30),
@apellido1_vendedor varchar (30),
@apellido2_vendedor varchar (30),
@nick_vendedor varchar (30),
@contraseņa_vendedor varchar (20),
@direccion_vendedor varchar (50),
@genero_vendedor varchar (20),
@correo_vendedor varchar (50),
@telefono_vendedor varchar (20),
@cc_vendedor varchar (20),
@Fk_sucursal_vendedor bigint
as
begin
update vendedor 
set
nombre_vendedor=@nombre_vendedor,
apellido1_vendedor=@apellido1_vendedor,
apellido2_vendedor=@apellido2_vendedor,
nick_vendedor=@nick_vendedor,
contraseņa_vendedor=@contraseņa_vendedor,
direccion_vendedor=@direccion_vendedor,
genero_vendedor=@genero_vendedor,
correo_vendedor=@correo_vendedor,
telefono_vendedor=@telefono_vendedor,
cc_vendedor=@cc_vendedor,
Fk_sucursal_vendedor=@Fk_sucursal_vendedor
where
Id_vendedor=@id_vendedor
end
go
create proc seleccionar_vendedor
@id bigint
as
begin
select *from vendedor where Id_vendedor=@id
end
go
create proc todos_vendedor
as
begin
select *from vendedor
end
go
create procedure Buscar_vendedor
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from vendedor
where vendedor.Id_vendedor like @code or vendedor.nombre_vendedor like @dato or vendedor.apellido1_vendedor like @dato or vendedor.apellido2_vendedor like
@dato or vendedor.cc_vendedor like @dato or vendedor.correo_vendedor like @dato or vendedor.direccion_vendedor like @dato or vendedor.genero_vendedor like
@dato or vendedor.nick_vendedor like @dato or vendedor.telefono_vendedor like @dato or vendedor.Fk_sucursal_vendedor like @code
end
go

create proc lista_sucursal_seleccion
as
begin
select id_sucursal,nombre_sucursal from sucursal
end


go

create proc con_nombre_id_sucursal
@nombre varchar (30)
as
begin
select id_sucursal from sucursal where nombre_sucursal=@nombre
end

go

create proc con_id_nombre_sucursal
@id bigint
as
begin
select nombre_sucursal from sucursal where id_sucursal=@id
end

go
exec crear_sucursal 'principal','calle 23 norte','Bogota','Colombia','1234567','abc@abc.com','147.147.85.2'
go
select *from sucursal
insert into vendedor(nombre_vendedor,apellido1_vendedor,apellido2_vendedor,nick_vendedor,contraseņa_vendedor,direccion_vendedor,genero_vendedor,correo_vendedor,telefono_vendedor,cc_vendedor,FK_sucursal_vendedor) values ('piloto','piloto','Piloto','piloto','piloto','piloto','cll 84d 56n53','masculino','1234567','147852369',1)
go

/* Pedido */
go
create proc crear_pedido
@nombre_pedido varchar (30),
@clase_pedido varchar (30),
@fecha_entrega_pedido datetime,
@fecha_expedida_pedido datetime,
@valor_pedido bigint,
@cantidad_productos_pedido bigint,
@necesidad_producto_pedido varchar (30),
@FK_proveedor_pedido bigint
as
begin
insert into pedido values (@nombre_pedido,@clase_pedido,@fecha_entrega_pedido,@fecha_expedida_pedido,@valor_pedido,@cantidad_productos_pedido,@necesidad_producto_pedido,@FK_proveedor_pedido)
end
go

create proc eliminar_pedido
@id bigint
as
begin
delete from pedido where pedido.id_pedido=@id
end

go

create proc modificar_pedido
@id_pedido bigint,
@nombre_pedido varchar (30),
@clase_pedido varchar (30),
@fecha_entrega_pedido datetime,
@fecha_expedida_pedido datetime,
@valor_pedido bigint,
@cantidad_productos_pedido bigint,
@necesidad_producto_pedido varchar (30),
@FK_proveedor_pedido bigint
as
begin
update pedido
set
nombre_pedido=@nombre_pedido,
clase_pedido=@clase_pedido,
fecha_entrega_pedido=@fecha_entrega_pedido,
fecha_expedida_pedido=@fecha_expedida_pedido,
valor_pedido=@valor_pedido,
cantidad_productos_pedido=@cantidad_productos_pedido,
necesidad_producto_pedido=@necesidad_producto_pedido,
FK_proveedor_pedido=@FK_proveedor_pedido
where
id_pedido=@id_pedido
end

go

create proc todos_pedido
as
begin
select *from pedido
end

go

create proc buscar_pedido
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from pedido
where id_pedido like @code or nombre_pedido like @dato or clase_pedido like @dato or valor_pedido like @code or cantidad_productos_pedido like @code or necesidad_producto_pedido like @dato or FK_proveedor_pedido like @code
end

go

create proc seleccionar_pedido
@id bigint
as
begin
select *from pedido where id_pedido=@id
end

go

create proc lista_proveedor_seleccion
as
begin
select id_proveedor,nombre_proveedor from proveedor
end


go

create proc nombre_id_proveedor
@nombre varchar (30)
as
begin
select id_proveedor from proveedor where nombre_proveedor=@nombre
end

go

create proc id_nombre_proveedor
@id bigint
as
begin
select nombre_proveedor from proveedor where id_proveedor=@id
end
go

/*Factura de Compra*/


create proc lista_vendedor_seleccion
as
begin
select Id_vendedor,nick_vendedor from vendedor
end


go

create proc con_alias_id_vendedor
@alias varchar (30)
as
begin
select Id_vendedor from vendedor where nick_vendedor=@alias
end

go

create proc con_id_alias_vendedor
@id bigint
as
begin
select nick_vendedor from vendedor where Id_vendedor=@id
end

go

create proc lista_pedido_seleccion
as
begin
select id_pedido,nombre_pedido from pedido
end


go

create proc con_nombre_id_pedido
@nombre varchar (30)
as
begin
select id_pedido from pedido where nombre_pedido=@nombre
end

go

create proc con_id_nombre_pedido
@id bigint
as
begin
select nombre_pedido from pedido where id_pedido=@id
end

go

create proc crear_fc
@tipo_fc varchar (30),
@organizacion_fc varchar (30),
@fecha_creacion_fc datetime,
@ciudad_fc varchar (30),
@iva_fc bigint,
@total_fc bigint,
@sub_total_fc bigint,
@Fk_vendedor_fc bigint,
@Fk_pedido_fc bigint
as
begin
insert into factura_compra values (@tipo_fc,@organizacion_fc,@fecha_creacion_fc,@ciudad_fc,@iva_fc,@total_fc,@sub_total_fc,@Fk_vendedor_fc,@Fk_pedido_fc)
end
go

create proc eliminar_fc
@id bigint
as
begin
delete from factura_compra where factura_compra.id_fc=@id
end

go

create proc modificar_fc
@id_fc bigint,
@tipo_fc varchar (30),
@organizacion_fc varchar (30),
@fecha_creacion_fc datetime,
@ciudad_fc varchar (30),
@iva_fc bigint,
@total_fc bigint,
@sub_total_fc bigint,
@Fk_vendedor_fc bigint,
@Fk_pedido_fc bigint
as
begin
update factura_compra
set
tipo_fc=@tipo_fc,
organizacion_fc=@organizacion_fc,
fecha_creacion_fc=@fecha_creacion_fc,
ciudad_fc=@ciudad_fc,
iva_fc=@iva_fc,
total_fc=@total_fc,
sub_total_fc=@sub_total_fc,
Fk_vendedor_fc=@Fk_vendedor_fc,
Fk_pedido_fc=@Fk_pedido_fc
where
id_fc=@id_fc
end

go

create proc todos_fc
as
begin
select *from factura_compra
end

go

create proc buscar_fc
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from factura_compra
where id_fc like @code or tipo_fc like @dato or organizacion_fc like @dato or ciudad_fc like @dato or iva_fc like @code or total_fc like @code or sub_total_fc like @code or Fk_pedido_fc like @code or Fk_vendedor_fc like @code
end

go

create proc seleccionar_fc
@id bigint
as
begin
select *from factura_compra where id_fc=@id
end

go

/* Pago */

create proc crear_pago
@tipo_pago varchar (30),
@iva_pago bigint,
@total_pago bigint,
@sub_total_pago bigint,
@Fk_cliente_pago bigint,
@Fk_vendedor_pago bigint,
@Fk_sucursal_pago bigint
as
begin
insert into pago values (@tipo_pago,@iva_pago,@total_pago,@sub_total_pago,@Fk_cliente_pago,@Fk_vendedor_pago,@Fk_sucursal_pago)
end
go

create proc eliminar_pago
@id bigint
as
begin
delete from pago where pago.id_pago=@id
end

go

create proc modificar_pago
@id_pago bigint,
@tipo_pago varchar (30),
@iva_pago bigint,
@total_pago bigint,
@sub_total_pago bigint,
@Fk_cliente_pago bigint,
@Fk_vendedor_pago bigint,
@Fk_sucursal_pago bigint
as
begin
update pago
set
tipo_pago=@tipo_pago,
iva_pago=@iva_pago,
total_pago=@total_pago,
sub_total_pago=@sub_total_pago,
Fk_cliente_pago=@Fk_cliente_pago,
Fk_vendedor_pago=@Fk_vendedor_pago,
Fk_sucursal_pago=@Fk_sucursal_pago
where
id_pago=@id_pago
end

go

create proc todos_pago
as
begin
select *from pago
end

go

create proc buscar_pago
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from pago
where id_pago like @code or pago.iva_pago like @code or pago.Fk_cliente_pago like @code or pago.Fk_sucursal_pago like @code or pago.Fk_vendedor_pago like @code or pago.sub_total_pago like @code or pago.tipo_pago like @dato or pago.total_pago like @dato
end

go

create proc seleccionar_pago
@id bigint
as
begin
select *from pago where id_pago=@id
end

go

create proc lista_cliente_seleccion
as
begin
select id_cliente,nombre_cliente from cliente
end


go

create proc nombre_id_cliente
@nombre varchar (30)
as
begin
select id_cliente from cliente where nombre_cliente=@nombre
end

go

create proc id_nombre_cliente
@id bigint
as
begin
select nombre_cliente from cliente where id_cliente=@id
end

go

go

create proc nombre_id_vendedor
@nombre varchar (30)
as
begin
select Id_vendedor from vendedor where nick_vendedor=@nombre
end

go

create proc id_nombre_vendedor
@id bigint
as
begin
select nick_vendedor from vendedor where Id_vendedor=@id
end
go

create proc nombre_id_sucursal
@nombre varchar (30)
as
begin
select id_sucursal from sucursal where nombre_sucursal=@nombre
end

go

create proc id_nombre_sucursal
@id bigint
as
begin
select nombre_sucursal from sucursal where id_sucursal=@id
end
go

/* Factura de venta */
go
create proc crear_fv
@tipo_fv varchar (40),
@total_fv bigint,
@iva_fv bigint,
@sub_total_fv bigint,
@comentarios_fv varchar (125),
@Fk_cliente_fv bigint,
@Fk_vendedor_fv bigint,
@Fk_sucursal_fv bigint,
@fecha_creacion_fv datetime,
@feha_entrega_fv datetime
as
begin
insert into factura_venta values (@tipo_fv,@total_fv,@iva_fv,@sub_total_fv,@comentarios_fv,@Fk_cliente_fv,@Fk_vendedor_fv,@Fk_sucursal_fv,@feha_entrega_fv,@fecha_creacion_fv)
end
go

create proc eliminar_fv
@id bigint
as
begin
delete from factura_venta where id_fv=@id
end

go

create proc modificar_fv
@id_fv bigint,
@tipo_fv varchar (40),
@total_fv bigint,
@iva_fv bigint,
@sub_total_fv bigint,
@comentarios_fv varchar (125),
@Fk_cliente_fv bigint,
@Fk_vendedor_fv bigint,
@Fk_sucursal_fv bigint,
@fecha_creacion_fv datetime,
@feha_entrega_fv datetime
as
begin
update factura_venta
set
tipo_fv=@tipo_fv,
total_fv=@total_fv,
iva_fv=@iva_fv,
sub_total_fv=@sub_total_fv,
comentarios_fv=@comentarios_fv,
Fk_cliente_fv=@Fk_cliente_fv,
Fk_vendedor_fv=@Fk_vendedor_fv,
Fk_sucursal_fv=@Fk_sucursal_fv,
fecha_creacion_fv=@fecha_creacion_fv,
feha_entrega_fv=@feha_entrega_fv
where
id_fv=@id_fv
end

go

create proc todos_fv
as
begin
select *from factura_venta
end

go

create proc buscar_fv
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from factura_venta
where
id_fv like @code or tipo_fv like @dato or total_fv like @code or
iva_fv like @code or sub_total_fv like @code or comentarios_fv like @dato or
Fk_cliente_fv like @code or Fk_vendedor_fv like @code or Fk_sucursal_fv like @code
end
go

/*Producto*/

go
create proc crear_producto
@nombre_producto varchar (40),
@tipo_producto varchar (20),
@clase_producto varchar(40),
@categoria_producto varchar(40),
@tela_producto varchar (30),
@costo_producto bigint,
@altura_producto bigint,
@anchura_producto bigint,
@largo_producto bigint,
@garantia_producto float,
@tapizado_producto varchar (30),
@espuma_producto varchar (30),
@Fk_proveedor_producto bigint
as
begin
insert into producto values (@nombre_producto,@tipo_producto,@clase_producto,@categoria_producto,@tela_producto,@costo_producto,@altura_producto,@anchura_producto,@largo_producto,@garantia_producto,@tapizado_producto,@espuma_producto,@Fk_proveedor_producto)
end
go

create proc eliminar_producto
@id bigint
as
begin
delete from producto where id_producto=@id
end

go

create proc modificar_producto
@id_producto bigint,
@nombre_producto varchar (40),
@tipo_producto varchar (20),
@clase_producto varchar(40),
@categoria_producto varchar(40),
@tela_producto varchar (30),
@costo_producto bigint,
@altura_producto bigint,
@anchura_producto bigint,
@largo_producto bigint,
@garantia_producto float,
@tapizado_producto varchar (30),
@espuma_producto varchar (30),
@Fk_proveedor_producto bigint
as
begin
update producto
set
nombre_producto=@nombre_producto,
tipo_producto=@tipo_producto,
clase_producto=@clase_producto,
categoria_producto=@categoria_producto,
tela_producto=@tela_producto,
costo_producto=@costo_producto,
altura_producto=@altura_producto,
anchura_producto=@anchura_producto,
largo_producto=@largo_producto,
garantia_producto=@garantia_producto,
tapizado_producto=@tapizado_producto,
espuma_producto=@espuma_producto,
Fk_proveedor_producto=@Fk_proveedor_producto
where
id_producto=@id_producto
end

go

create proc todos_producto
as
begin
select *from producto
end

go

create proc buscar_producto
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from producto
where
id_producto like @code or nombre_producto like @dato or
tipo_producto like @dato or clase_producto like @dato or
categoria_producto like @dato or tela_producto like @dato or
costo_producto like @code or altura_producto like @code or
anchura_producto like @code or largo_producto like @code or
tapizado_producto like @dato or espuma_producto like @dato or
Fk_proveedor_producto like @code
end

go

create proc seleccionar_producto
@id bigint
as
begin
select *from producto where id_producto=@id
end

go

/* Detalle_factura */

go
create proc crear_detfactura
@cantidad_productos_detalle bigint,
@valor_total_productos_detalle bigint,
@Fk_prod_detallefv bigint,
@Fk_fv_detallefv bigint
as
begin
insert into detallefactura_venta values (@cantidad_productos_detalle,@valor_total_productos_detalle,@Fk_prod_detallefv,@Fk_fv_detallefv)
end
go

create proc eliminar_detfactura
@id bigint
as
begin
delete from detallefactura_venta where id_detallefv=@id
end

go

create proc modificar_detfactura
@id_detallefv bigint,
@cantidad_productos_detalle bigint,
@valor_total_productos_detalle bigint,
@Fk_prod_detallefv bigint,
@Fk_fv_detallefv bigint
as
begin
update detallefactura_venta
set
cantidad_productos_detalle=@cantidad_productos_detalle,
valor_total_productos_detalle=@valor_total_productos_detalle,
Fk_prod_detallefv=@Fk_prod_detallefv,
Fk_fv_detallefv=@Fk_fv_detallefv
where
id_detallefv=@id_detallefv
end

go

create proc todos_detfactura
as
begin
select *from detallefactura_venta
end

go

create proc buscar_detfactura
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from detallefactura_venta
where
cantidad_productos_detalle like @code or valor_total_productos_detalle like @code or
Fk_fv_detallefv like @code or Fk_prod_detallefv like @code or id_detallefv like @code
end

go

create proc seleccionar_detfactura
@id bigint
as
begin
select *from detallefactura_venta where id_detallefv=@id
end

go

create proc lista_producto_seleccion
as
begin
select id_producto,nombre_producto from producto
end


go

create proc nombre_id_producto
@nombre varchar (30)
as
begin
select id_producto from producto where nombre_producto=@nombre
end

go

create proc id_nombre_producto
@id bigint
as
begin
select nombre_producto from producto where id_producto=@id
end

go

create proc ultima_factura_venta
as
begin
SELECT MAX(id_fv) FROM factura_venta
end


go

go
create proc busqueda_no_especifica_producto
@dato varchar(410)
as
begin
declare @code bigint
begin try
set @code = CONVERT(bigint,@dato)
end try
begin catch
set @code = 0
end catch
select *from producto
where
id_producto like @code or nombre_producto like '%'+@dato+'%' or
tipo_producto like '%'+@dato+'%' or clase_producto like '%'+@dato+'%' or
categoria_producto like '%'+@dato+'%' or tela_producto like '%'+@dato+'%' or
costo_producto like @code or altura_producto like @code or
anchura_producto like @code or largo_producto like @code or
tapizado_producto like '%'+@dato+'%' or espuma_producto like '%'+@dato+'%' or
Fk_proveedor_producto like @code
end
go
go

create proc todos_factura_detfactura
@id bigint
as
begin
select *from detallefactura_venta
where
Fk_fv_detallefv=id_detallefv
end
go
create proc Factura_venta_sacar
@id_fv bigint
as
begin
select id_detallefv,id_producto,nombre_producto,cantidad_productos_detalle,costo_producto,valor_total_productos_detalle,id_fv 
from detallefactura_venta,producto,factura_venta
where
Fk_fv_detallefv=@id_fv and  id_fv=@id_fv and id_producto=Fk_prod_detallefv
end
go
create proc seleccionar_fv
@id bigint
as
begin
select *from factura_venta where id_fv=@id
end
go
go
create proc eliminar_detalles_fv_todos
@id bigint
as
begin
delete from detallefactura_venta where Fk_fv_detallefv=@id
end
go